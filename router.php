<?php
error_reporting(1);
/**
 * Define routing for web permalink
 */

use Masterzain\Classes\Routing;

// CLEAR CACHE
Routing::group(['prefix' => '/clear'], function () {
	Routing::run("get", "/",										"LibController@clear"); // http://namadomain.com/clear
	Routing::run("get", "/cache",								"LibController@clear"); // http://namadomain.com/clear/cache
	Routing::run("get", "/views",								"LibController@clear"); // http://namadomain.com/clear/views
});

// SITEMAP ROUTING
Routing::run("get", "/sitemap-image.xml",			"LibController@sitemap_image");
Routing::run("get", "/sitemap.xml", 					"LibController@sitemap_index");
Routing::run("get", "/sitemap-{loop:n}.xml", 	"LibController@sitemap_main");
Routing::run("get", "/feed",									"LibController@feed_main");
Routing::run("get", "/rss",										"LibController@feed_main");
Routing::run("get", "/atom",									"LibController@feed_main");
Routing::run("get", "/feed/image",						"LibController@feed_image");
Routing::run("get", "/robots.txt",						"LibController@robots");

// MAIN ROUTING

Routing::run("get", "/",														"WallpaperController@index");
Routing::run("get", "/page/{page:uri}", 						"WallpaperController@page");
Routing::run("get", "/list/{abjad:a}", 							"WallpaperController@list_term");

Routing::run("get", "/{query:all}.html", 						"WallpaperController@search");

//---------------------------
// OTHER PERMALINK OPTION //
//---------------------------

//Routing::run("get", "/{random:x}/{query:all}.html", 					"WallpaperController@search");
// http://coding.dev/7yfq5/home-garden-interior-design.html

//Routing::run("get", "/{firstchar:x}/{query:all}.html", 					"WallpaperController@search");
// http://coding.dev/h/home-garden-interior-design.html

// Routing::run("get", "/{firstword:x}/{query:all}.html", 					"WallpaperController@search");
// http://coding.dev/home/home-garden-interior-design.html

Routing::run("get", "/{query:all}/{subquery:all}.html", 	"WallpaperController@attachment");
// Routing::run("get", "/{query:all}/{subquery:all}.html", 	"WallpaperController@attachment");

// Routing::run("get", "/uploads/{query:all}/{subquery:all}.jpg", 	"WallpaperController@image");

// POST ROUTING SEARCH PAGE
Routing::run("post", "/", "WallpaperController@indexPost");

// 404 NOT FOUND
Routing::Errors(); // redirect to homepage

// START ROUTING
Routing::start();
