<style>
	
body, html, div, blockquote, img, label, p, h1, h2, h3, h4, h5, h6, pre, ul, ol,
li, dl, dt, dd, form, a, fieldset, input, th, td
{
margin: 0; padding: 0; border: 0; outline: none;
}

body
{
font-family: 'Open Sans', Arial, Helvetica, sans-serif;
line-height: 25px;
font-size:13px;
}
p{
line-height: 18px !important;
font-size:13px !important;
}
h1, h2, h3, h4, h5, h6
{
font-size: 100%;
}
p {
margin: 0 0 1em;
}
ul, ol
{
list-style: none;
}

a
{
color: #0085d1;
text-decoration: none;
-webkit-transition: all ease-in-out 0.2s;
-moz-transition: all ease-in-out 0.2s;
-o-transition: all ease-in-out 0.2s;
transition: all ease-in-out 0.2s;
}

a:hover
{
color: #C62B07;
}

.clear{
clear: both;
}
ul, li {
list-style:none;
padding:0px;
margin:0px;
}
.clear{
clear:both;
}
button, input, textarea {
border: 1px solid #ccc;
border-radius: 3px;
font-family: inherit;
padding: 6px;
padding: 0.428571429rem;
}

.fa{
margin:0px 5px 0px 0px;
}
/*!
 *  Font Awesome 4.2.0 by @@davegandy - http://fontawesome.io - @@fontawesome
 *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
 */
/* FONT PATH
 * -------------------------- */
@@font-face {
  font-family: 'FontAwesome';
  src: url('fonts/fontawesome-webfont.eot');
  src: url('fonts/fontawesome-webfont.eot?#iefix') format('embedded-opentype'), url('fonts/fontawesome-webfont.woff') format('woff'), url('fonts/fontawesome-webfont.ttf') format('truetype'), url('fonts/fontawesome-webfont.svg#fontawesomeregular') format('svg');
  font-weight: normal;
  font-style: normal;
}
.fa {
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  font-size: inherit;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.fa-search:before {
  content: "\f002";
}
.fa-chevron-left:before {
  content: "\f053";
}
.fa-chevron-right:before {
  content: "\f054";
}

.main-menu>li.icon-star:before {
  content: "\f005";
}

.fa-search{
position: absolute;
top: 9px;
left: 7px;
color: #999;
font-size: 18px;
line-height: 22px;
width: 22px;
height: 22px;
-webkit-transition: all ease-in-out 0.2s;
-moz-transition: all ease-in-out 0.2s;
-o-transition: all ease-in-out 0.2s;
transition: all ease-in-out 0.2s;
}
.imenu:before{
position: relative;
float: left;
margin: 17px 0px 0px 10px;
padding: 3px 0px 0px 3px;
height: 22px;
width: 22px;
font-size: 14px;
font-family: "FontAwesome";
}
.circle:before{
border: 1px solid #fbfbfb;
border-radius: 10px;
}
.main-wrapper{
position: fixed;
top: 0px;
left: 0px;
width: 100%;
height: 100%;
z-index: -9999;
background:#111
}

.container{
max-width:1280px !important;
margin:0 auto;
}
#header {
width: 100%;
float: left;
font-family: Helvetica, Arial, sans-serif;
font-size: 15px;
position: relative;
padding: 2px 0px 0px 0px;
display: block;
z-index: 99999;
background: #111;
border-bottom: 3px solid green;
}
#header h1, #header h2{
font-size:16px;
font-weight: 400;
margin-bottom: 0px;
margin-top: 0px;
margin-right: 10px;
float: left;
}

.logo{
float: left;
padding: 7px;
}
.logo h1 a, .logo h2 a{
font-size:20px;
color:#2980b9;
font-weight: bold;
}
.site-logo img{
float:left;
}
.site-description{
font-size: 11px;
line-height: 12px;
padding: 8px;
}
.right-header{
width:720px;
float:right;
text-align:right;
}
.top-menu{
margin:0px;
}
.top-menu li {
display: inline-block;
font-size: 11px;
}
.top-menu li a{
padding:3px 10px;
}
/* Category menu*/
nav{
font-family: 'Open Sans', sans-serif;
font-size: 12px;
font-weight: normal;
font-style: normal;
}
#navigation .columns, #navigation.columns{
padding-left:0px !important
}
.nav-menu{
margin: 0px;
padding: 0px;
display: inline-block;
float: right;
text-align: right;
}
.search-wall{
margin: 10px 0px;
}
#searchform {
position: relative;
margin:0px;
}
#searchsubmit{
position:absolute;
top:0px;
right:0px;
opacity:0;
z-index:1;
}
#s{
margin:0px;
border-radius: 5px;
padding-left: 30px;
}
#searchform:hover .fa-search{
left:265px;
z-index:0
}
.page-title{
font-size: 24px;
line-height: 30px;
padding: 10px 0px;
}
.follow-list{
width: 80px;
float: right;
display: inline-table;
}
.follow-list i{
margin-right:10px;
}
.follow-list h3{
line-height: 55px;
text-align: center;
font-size: 12px;
margin-bottom: 0px;
margin-top: 0px;
}
.follow-list h3 a{
color:#2980b9;
}
.follow-list ul li{
border-bottom:1px solid #eee;
font-size:12px;
line-height:16px;
width:100%;
float:left;
}
.follow-list ul{
margin-left: 0px;
z-index: 999999;
position: absolute;
height:0px;
width: 150px;
background: #fbfbfb;
overflow:hidden;
-webkit-transition: all ease-in-out 0.2s;
-moz-transition: all ease-in-out 0.2s;
-o-transition: all ease-in-out 0.2s;
transition: all ease-in-out 0.2s;
box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.28);
}

.follow-list a{
line-height:20px;
color:#313131;
padding:10px;
text-transform: none;
text-shadow:none;
font-weight: bold;
}
.follow-list:hover ul{
height:164px;
margin:0px;
}
a.facebook {
color: #314d91;
}
a.facebook:hover {
background:#314d91;
color: #fbfbfb;
}
a.rss {
color: #ffaa31
}
a.rss:hover {
background:#ffaa31;
color: #fbfbfb
}
a.twitter {
color: #07beed;
}
a.twitter:hover {
background:#07beed;
color: #fbfbfb;
}
a.gplus {
color: #313131;
}
a.gplus:hover {
background:#313131;
color: #fbfbfb;
}
.main-menu{
float:left;
font-size: 14px;
margin-bottom: 0px;
margin-left: 0px;
}
.main-menu>li{
position:relative;
color:#333;
}
.main-menu>li:hover{
color: #fbfbfb;
}
.main-menu>li:hover>a{
background:#2980b9;
color: #fbfbfb;
}
.main-menu>li>a{
padding: 5px 15px;
font-weight: bold;
color: #ccc;
}
.main-menu>li>ul{
margin-left: 0px;
z-index: 999999;
position: absolute;
height: 0px;
width: 150px;
background: #fbfbfb;
overflow: hidden;
-webkit-transition: all ease-in-out 0.2s;
-moz-transition: all ease-in-out 0.2s;
-o-transition: all ease-in-out 0.2s;
transition: all ease-in-out 0.2s;
box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.28);
}
.main-menu>li:hover ul{
height: auto;
top: 55px;
margin: 0px;
}
.main-menu>li>ul li{
background:none;
width:100%;
border-bottom: 1px solid #eee;
}
.main-menu>li>ul li a{
line-height:40px;
font-size:12px;
text-transform:none;
color:#333;
text-shadow:none;
}
.main-menu>li>ul li a:hover{
box-shadow: inset 0px -1px 0px rgba(0,0,0, .1);
color:#fbfbfb;
}
.footer-menu>li{
position:relative;
}
.footer-menu>li>ul{
display:none;
position:absolute;
bottom: 20px;
left: 0px;
z-index: 999;
background: #41aafa;
width: 200px;
}
.footer-menu>li:hover ul{
display:block;
}
.footer-menu>li>ul li{
background:none;
width:100%;
}
nav li{
float:left;
display:inline;
text-align: left;
}
.follow-list h3:hover{
background:#2980b9;
}
.follow-list h3:hover>a{
color:#fbfbfb;
}
nav li a{
padding: 0px 15px;
line-height: 45px;
display: block;
color: #fff;
font-family: "Open sans", arial, helvetica;
}
.breadcrumbs {
font-family:georgia;
font-size: 14px;
width:100%;
padding:10px 0px 10px 0px;
}
.breadcrumbs a{
color:#00BDF6;
}

/*content*/
#primary{
float: left;
overflow: hidden;
}
#main{
overflow: hidden;
margin: 10px 0px;
padding-left: 0px;
padding-right: 0px;
}
.attachment #main{
background:#000;
}
#content{
float: right;
padding-top: 10px;
width:100%;
}
#main .single{
float: left;
padding: 0px 0px 0px 0px;
overflow: hidden;
background: #000;
}
/* .holder .post .entry-title{
height: 20px;
overflow: hidden;
text-align:center !important;
padding-top: 0px !important;
} */
h1.entry-title a{
color:#ccc
}
.left-sidebar #main .single{
float: right;
padding: 0px 0px 0px 0px;
}
.single-content{
float: left;
padding: 10px;
line-height: 18px;
}
.single-content p{
font-size:13px;
}
.ads-samping-gbr{
float:right;
max-width:49%;
}
.single img{
max-width:100%;
height:auto;
}
.ads-samping-gbr {
width:49%;
height:300px;
border: 1px solid #ddd !important;
padding:10px;
}
.ads-samping-gbr img{
max-width:100%;
height:auto;
}
h1.single-title{
width: 100%;
float: left;
text-align: justify;
font-size: 26px;
line-height: 32px;
padding: 0px 10px;
font-weight: bold;
font-family: helvetica, arial, sans-serif;
text-transform: capitalize;
}
h1.single-title a{
color:#ddd
}
.holder .post{
width: 24.3%;
height: 223px;
float: left;
margin-bottom: 10px;
margin-right:10px;
text-align: center;
padding-bottom: 1px;
box-shadow: 0px 1px 4px #000;
background: #000;
padding: 3px;
color: #333;
overflow: hidden;
position: relative;
z-index: 0;
}
.holder .post img{
-webkit-transition: all ease-in-out 0.2s;
-moz-transition: all ease-in-out 0.2s;
-o-transition: all ease-in-out 0.2s;
transition: all ease-in-out 0.2s;
}
.holder .post img:hover {
opacity: .7;
transform: scale(1.1, 1.1);
}
.holder-block{
float: left;
margin-bottom: 20px;
width: 100%;
}
.attachment .holder-block{
text-align: center;
}
.prev-next{
float:left;
width:100%;
margin: 20px 0;
}
.prev-next .full a{
padding:10px;
background:green
}
.prev-next .prev{
float:left;
margin-left: 20px;
}
.prev-next .next{
float:right;
margin-right: 20px;
}
.attachment .gallery-wrap{
padding: 0px 20px;
}
.attachment .gallery-item img{
width:100%;
padding: 3px;
height: auto;
}
.holder-block .post{
width: 23.9%;
float: left;
margin-right: 10px;
margin-bottom: 10px;
border: 1px solid #CCCCCC;
text-align: center;
padding-bottom: 1px;
box-shadow: 0 1px 5px #ddd;
padding: 3px;
height: 150px;
}
.attachment .holder-block .post{
height:210px;
width:24.1% !important;
display: inline-block;
float: none;
}
@@media (min-width:985px) {
.holder .post:nth-child(4n+0), .attachment .columns4 .post:nth-child(4n+0), .holder-block .post:nth-child(4n+0){
margin-right: 0px !important
}
}
.large-3.medium-3.small-4 .post{
height:160px !important;
} 
.large-3.medium-3.small-4 .entry-content{
height:100px;
}
.attachment .columns4 .post{
height:180px !important;
width: 219px !important;
}
.wp-caption {
max-width: 698px !important;
border: 1px solid #CCC;
padding: 10px;
box-shadow: 0 2px 5px #DDD;
height: auto;
background: #ffffff;
}
#content h2, #content h1{
float: center;
padding-left: 5px;
text-align: left;
padding-top: 5px;
font-weight: 400;
margin: 0px 0px 0px 0px;
font-size: 13px;
text-transform: capitalize;
height: 41px;
}
#content h2 a, #content h1 a{
text-decoration: none;
}
#content .entry-meta, .most-viewed .entry-meta, .related .entry-meta{
font-size: 10px;
}
#content .entry-meta ul{
margin-left:0px;
line-height:14px;
font-size:10px;
}
.columns4 .entry-meta{
line-height: 12px;
text-align: center;
}
.columns4 .entry-meta{
margin-top:7px;
}
.entry-meta ul{
margin:0px !important;
font-size: 10px !important;
line-height:14px !important;
}
.entry-meta ul li{
display:inline;
padding: 3px;
color: #6b6b6b;
opacity: 0.6;
font-weight: bold;
float: left;
}
.entry-content{
width: 100%;
height: 160px;
position: relative;
overflow:hidden;
z-index: 0;
}
span.most-loved {
position: absolute;
width: 43px;
height: 42px;
background: red;
border: 2px solid #eee;
color: #fff;
padding: 7px 5px;
border-radius: 22px;
line-height: 11px;
font-size: 11px;
top: 120px;
left: -9px;
z-index: 2;
}
.holder-block .entry-content{
height:95px;
}
.attachment .holder-block .entry-content{
height:160px;
}

.post-meta{
float: left;
margin: -10px 0px 10px 0px;
color: #000;
font-size: 13px;
font-style: italic;
font-family: georgia,serif !important;
}
.post-meta a{
color:#00BDF6;
}
.entry{
float: left;
width: 100%;
border-bottom: 1px solid #E0E0E0;
padding: 0 0 10px;
margin: 0 0 15px;
font-family: inherit;
font-size: 14px;
letter-spacing: 0px;
color: #444;
}
.gallery-wrapper{
float: left;
width: 100%;
}
.gallery-wrapper h3{
font-size: 18px;
color: inherit;
margin: 20px 0px;
font-weight: 600;
}
.related{
float: left;
margin: 0px 0px 20px 0px;
padding: 10px;
}
.related .holder{
text-align:center;
}
.related .post:last-child{
margin-right:0px
}
h2.entry-title{
height: 25px;
overflow: hidden;
text-align: center !important;
font-size: 12px;
}
.attachment h2.entry-title{
margin: 0px;
line-height: 27px;
}
.related .entry-title{
margin-bottom: 0px;
height: auto;
}
.related .entry-header a{
font-weight:400;
}
.most-viewed{
float:left;
width: 100%;
}
.inner-attachment{
float: left;
padding: 10px;
text-align: center;
width: 100%;
margin: 10px 0px;
}
#content .columns4 .post{
width: 164px !important;
height: 135px !important;
float: left;
margin-left: 0px;
margin-right: 10px;
margin-bottom: 13px;
border: 1px solid #CCCCCC;
text-align: left;
padding-bottom: 1px;
box-shadow: 0 1px 5px #ddd;
background: rgb(250, 250, 250);
padding: 3px;
color: #555;
overflow: hidden;
}
#content .columns4 .post .entry-header{
margin-top:5px;
}
#content .columns4 .entry-title{
height: 20px;
overflow: hidden;
float: center;
padding-left: 10px;
text-align: left;
padding-top: 5px;
font-weight: 400;
line-height: 16px;
margin: 0;
}
.home .columns4{
margin: 0px 0px 20px 0px;
}
.width-full{
width:98.5%;
display: inline-block;
}
.width-full.bottom{
margin: 20px 0px 0px 0px;
}
h3.header-title{
color: #fbfbfb;
font-size: 14px;
padding: 10px;
line-height: 12px;
background: #f7f7f7;
color: #ccc;
float: left;
width: 100%;
display: block;
margin-bottom:10px;
}
.related h3.header-title{
display:block;
}
.home h3.header-title{
text-transform: uppercase;
font-weight:400;
}

.view-more a{
color:#00A8FF;
}
#sidebar{
float: right;
height: 100%;
min-height: 100%;
padding: 20px 10px;
background: #000;
margin-left: 10px;
width: 24%;
}
.left-sidebar #sidebar{
margin-left:0px;
margin-right:10px;
float:left;
}
#sidebar h3{
padding:10px;
margin-bottom: 10px;
background: #1a99aa;
font-weight: bold;
color: #fff;
font-size: 14px;
white-space: nowrap;
}
.sidebar-widget ul{
margin-left:0px;
font-family: helvetica, arial, sans-serif;
}
.image-info{
border-radius: 4px;
}
.image-info img{
margin-bottom:10px;
margin-top: -10px;
}
.image-info li:before{
background:none !important;
}
#sidebar .image-info h3{
background: #dd3333;
padding: 10px;
color: #fff;
font-size: 13px;
line-height: 1.5em;
position: relative;
text-shadow: 1px 1px 1px rgba(0,0,0,0.1);
box-shadow: none;
}
.image-info h3:after{
content: "";
display: block;
position: absolute;
bottom: -7px;
left: 8px;
width: 0;
height: 0;
border-left: 8px solid transparent;
border-right: 8px solid transparent;
border-top: 8px solid #dd3333;
}
.image-info li span{
float:right;
}
.image-info li span.filename{
width: 100%;
overflow: hidden;
}
.image-info ul li:hover{
overflow:visible;
}
#sidebar .sidebar-widget{
margin:0px 0px 20px 0px;
float: left;
width: 100%;
}
#sidebar ul li{
position:relative;
color: #ddd;
font-size: 12px;
line-height: 25px;
display: block;
min-height: 25px;
padding: 0 10px 0px 15px;
}
.image-info li {
float: left;
width: 100%;
border-bottom: 1px dashed #333;
}
#sidebar ul li:before{
position: absolute;
top: 10px;
left: 5px;
vertical-align: middle;
display: inline-block;
width: 5px;
height: 5px;
background: #00A8FF;
content: "";
margin-right: 10px;
}
#sidebar ul li:after{
position: absolute;
top: -8px;
left: 5px;
margin: 8px 10px 0 0px;
vertical-align: middle;
display: inline-block;
margin-right: 10px;
color: #fff;
font-size: 18px;
}
#sidebar>li {
background: #fff;
margin: 5px 0px 0px 0px;
}
#sidebar #searchsubmit {
width: 28%;
padding: 10px 0px 9px 0px;
margin: 1px 0px 0px 1px;
background: #2980b9;
font-weight: 600;
font-size: 11px;
line-height: 16px;
color: #fbfbfb;
border-top-right-radius: 3px;
border-bottom-right-radius: 3px;
border: none;
}
#sidebar #s{
width: 70%;
padding: 5px 0px 5px 5px;
height: 35px;
margin-top: 1px;
border-color: #2980b9;
border-top-left-radius: 5px;
border-bottom-left-radius: 5px;
float: left;
}

#sidebar .screen-reader-text{
display:none;
}
.bflexslider{
margin:0px 0px 0px -20px;
}
.side-thumbnail{
width:82px;
height:82px;
float:left;
overflow:hidden;
}
#slider .list_items li {
width:300px;
float:left;
}
#slider li h3{
float:left;
width:200px;
margin: 0px 0px 0px 0px;
padding: 0px 10px 0px 7px;
}
.list_items ul{
margin:0px 0px 0px -30px;
}
.sidebar-direction-nav{
position:absolute;
top: -10px;
background: #fff;
width: 100%;
}
.sidebar-direction-nav li{
float:left;
font-weight: bold;
font-family: arial;
}
.sidebar-direction-nav li a{
padding: 0px 20px 0px 20px;
} 
.pagination{
width: 100%;
float: left;
text-align: center;
margin: 20px 0px;
}
.pagination span{
background: #41aafa;
    color: #FFFFFF;
    width: auto;
    height: auto;
    margin: 10px 5px;
    padding-top: 5px;
    padding-right: 10px;
    padding-bottom: 5px;
    padding-left: 10px;
    border-radius: 3px;
    box-shadow: inset 0px -2px 0px rgba(0,0,0, .2);
    display: inline-block;
}
.pagination span.current{display:inline;}
.pagination .inactive{
color: #313131;
text-decoration: none;
height: auto;
width: auto;
margin: 1px;
padding-top: 4px;
padding-right: 8px;
padding-bottom: 4px;
padding-left: 8px;
display: inline;
border: 1px solid #ccc;
box-shadow: inset 0px -2px 0px rgba(0,0,0, .2);
border-radius: 3px;
}
.pagination .inactive:hover{
background:#eee;
}
/*#sidebar li a{
list-style: none;
font-size: 15px;
text-decoration: none;
color: black;
}*/
#sidebar>ul>li a, .breadcrumbs{
color: #525252;
border: 1px solid #e2e2e2;
display: block;
font-size: 12px;
line-height: 16px;
margin-bottom: 8px;
padding: 5px;
text-decoration: none;
text-shadow: 0px 1px 0px #fff;
-moz-box-shadow: inset 0px 1px 0px #fbfbfb;
-webkit-box-shadow: inset 0px 1px 0px #fbfbfb;
box-shadow: inset 0px 1px 0px #fbfbfb;
}
img.size-full{
width:100%
}
.single-meta{
float: left;
width: 100%;
}
.metas li{
border-bottom:1px dashed #ddd;
line-height: 24px;
}
.single-ads{
float: left;
width: 100%;
text-align: center;
margin:10px 0px;
}
.attachment .single-ads{
margin-bottom: 20px;
}
.details{
width:100%;
float:left;
}
.detail{
font-size: 14px;
font-weight: 600;
padding: 10px;
border-bottom: 1px solid #ddd;
border-top: 1px solid #ddd;
background: #f7f7f7;
}
.detail-ads {
float: left;
width: 337px;
text-align: center;
}
#related .detail{
border:none;
}
.bottom-ads{
float: left;
width: 47%;
}
.thumblock{
display:none;
}
.ratingblock{
width:100%;
float:right;
}
ul.metas{
height: auto;
width: 46%;
float: left;
color: #555;
font-size: 12px;
padding-left: 20px;
}
ul.vd-links{
float: right;
width: 100%;
text-align: center;
margin: 20px 0px;
}
.vd-links li{
margin-left: 10px;
margin-top: 20px;
margin-bottom: 10px;
background: #2ea2cc;
border-color: #0074a2;
-webkit-box-shadow: inset 0 1px 0 rgba(120,200,230,.5),0 1px 0 rgba(0,0,0,.15);
box-shadow: inset 0 1px 0 rgba(120,200,230,.5),0 1px 0 rgba(0,0,0,.15);
font-weight: bold;
color: #fff;
display: inline-block;
text-decoration: none;
font-size: 13px;
line-height: 26px;
height: 28px;
margin: 0;
padding: 0 10px 1px;
cursor: pointer;
border-width: 1px;
border-style: solid;
-webkit-border-radius: 3px;
-webkit-appearance: none;
border-radius: 3px;
white-space: nowrap;
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
}
.vd-links a{
color:#fbfbfb;
}
#related{
font-weight: bold;
font-size: 12px;
width: 100%;
float: left;
margin: 20px 0px;
}
.disclaimer{
padding: 10px;
font-size: 11px;
line-height: 15px;
border-bottom: 1px dashed #333;
}
#footer{
clear: both;
font-family: Arial, Helvetica, sans-serif;
font-size: 15px;
position: relative;
z-index: 900;
background: #000;
padding: 10px;
color: #555;
}
#footer ul{
margin-left: 0px !important;
float: left;
margin-bottom: 0px;
}
.footer-menu ul{
width:100%;
}
#footer .copyright{
font-size:10px;
text-align: center;
}
.right{
width: 100%;
text-align: center;
}
.right, .right a{
color: #aaa;
font-size: 12px;
font-family: 'Open sans', arial;
}
.edit-link{
float:left;
margin:10px 0px 10px 0px
}
#breadcrumbs{
width: 100%;
float: left;
font-size: 11px;
padding: 10px;
}
.footer-menu li{
display: inline-block;
}
.footer-menu li a{
font-weight:bold;
color:#aaa;
padding-right:10px;
font-size:11px;
}
p img {
max-width: 100%;
border: 1px solid #CCC;
padding: 10px;
border: 1px solid #333;
height: auto;
}
.entry-description{
float: left;
padding: 0px 10px;
display: block;
margin: 10px 0px;}
.single-content p, .entry-description p{
font-size: 13px;
line-height: 22px;
margin-bottom: 10px;
}
.single-content ul, .entry-description ul{
font-size:13px;
}
.single-content ul span.ket .entry-description ul span.ket{
width: 120px;
display: inline-block;
}
.social-share-list li{
float:left;
margin:10px 30px 10px 0px;
display: block;
}
.attachment .columns4{
margin-left:10px;
}

.attachment .columns4 .post img{
height:127px
}
.attachment .width-full{
width:99%;
}
/* COMMENTS
----------------------------------------------- */
.comments-area {
padding: 10px 20px 10px 10px;
}
.comment{
position:relative;
margin-bottom:20px;
}
.comment-list {
	margin: 0 0 20px;
	padding: 0;
	list-style: none;
	}
.comment-list ul.children {
	margin: 0 0 0 50px;
	list-style: none;
	}
.comment-list li.comment,
.comment-list li.pingback {
	margin: 0;
	padding: 0;
	}
.comment-list li.pingback p {
	margin: 0;
	font-style: italic;
	}
.comment-list li.comment .comment-body {
	padding: 30px 0 20px;
	border-bottom: 1px solid #d6d6d6;
	}
.comment-list li.comment .comment-author .avatar {
	float: left;
	margin: 0 20px 0 0;
	display: block;
	}
.comment-list li.comment .comment-author .fn {
	color: #999;
font-size: 12px;
font-weight:bold;
text-transform: capitalize;
display: block;
	}
	.comment time{
	font-size:11px;
	}
.comment-list li.comment .comment-author .fn a {
	color: #333;
	text-decoration: none;
	}
.comment-list li.comment .comment-author .fn a:hover {
	color: #ff2828;
	}
.comment-list li.comment .comment-metadata {
	margin: 0 0 2px;
	font-size: .875em;
	}
.comment-list li.comment .comment-metadata a {
	color: #999;
	text-decoration: none;
	}
.comment-list li.comment .comment-metadata a:hover {
	color: #333;
	}
.comment-list li.comment .reply {
	font-size: 11px;
background: #E2E2E2;
margin-top: -29px;
padding: 1px 10px;
position: absolute;
border-top: 1px solid #D2D2D2;
border-left: 1px solid #D2D2D2;
right: 1px;
	}
.comment-list li.comment .reply a {
	color: #ff2828;
	font-size: .9em;
	font-weight: 300;
	text-transform: uppercase;
	text-decoration: none;
	letter-spacing: 1px;
	}
.comment-list li.comment .reply a:hover {
	color: #333;
	}
.comment-content {
	clear: both;
	display: block;
border-bottom: 1px solid #D2D2D2;
background: #FCFCFC;
margin: 0px 0 0 40px;
padding: 10px;
	}
.vcard {
display: block !important; 
 margin:0 !important; 
 border: none !important; 
 padding: 0; 
}
.comment-content a {
	word-wrap: break-word;
	}
.bypostauthor {
	}

#reply-title {
font-size: 17px;color: inherit;
	}
#cancel-comment-reply-link {
	margin: 0 0 0 10px;
	color: #333;
	font-size: .9em;
	text-decoration: none;
	}
#commentform {
	margin: 0;
	padding: 0;
	}
#commentform label {
float: left;
padding-right: 20px;
width: 70px;
font-size: 12px;
letter-spacing: 2px;
font-weight: bold;
	}
#commentform textarea {
	height: 100px;
	width: 99%;
	}
#commentform #submit {
line-height: 1;
background-color: #00A8FF;
padding: 10px;
color: #fff;
text-decoration: none;
font-size: 13px;
cursor: pointer;
display: inline-block;
border: none;
-webkit-transition: all 0.15s linear;
-moz-transition: all 0.15s linear;
transition: all 0.15s linear;
}
h3.gallery-title{
font-size: 14px;
font-weight: 600;
}
ul.single-gallery{
margin:0px 
}
li.gallery-items{
font-size: 11px;
line-height: 16px;
overflow: hidden;
/* width: 134px; */
min-height: 100px;
float: left;
}
li.gallery-items:last-child{
float:left !important;
}
.single-gallery li.gallery-items{
/* margin: 0px 6px 6px 2px; */
}
.gallery-items img{
border: 1px solid #777;
padding: 2px;
width: 95%;
height: 80px;
}
.gallery-items img:hover{
box-shadow: 0px 0px 3px #111;
}
.zilla-likes {
	
	border: 0 !important;
	display: inline-block;
	margin-bottom: 10px;
	min-height: 14px;
	padding-left: 18px;
	text-decoration: none;
	opacity: .5;
float: right;
margin-right: 10px;
font-size: 11px;
line-height: 14px;
}
.zilla-likes:hover,
.zilla-likes.active {
	
	border: 0 !important;
	margin: 0 10px 10px 0;
	padding-left: 18px !important;
	text-decoration: none;
}

.zilla-blog-widget .zilla-likes,
.tz_recentwork_widget .zilla-likes,
.tz_blog_widget .zilla-likes,
.home div#the_body #slider .excerpt .zilla-likes,
.archive div#the_body #slider .excerpt .zilla-likes { display: none; } /* Hide Zilla Share in custom post widget and sliders in themes */


.single-content .zilla-likes, .single-content .zilla-likes:hover,
.single-content .zilla-likes.active {
float: left;
margin: 13px 20px 10px 10px;
}

@@media(max-width: 1015px) {
#sidebar {
width:23%;
}
}
@@media(max-width: 640px) {
#sidebar {
width:100%;
}
}
@@media(max-width: 1288px) {
.holder .post, .attachment .post {
width: 24.2%;
height: auto;
}

.related .holder .post {
width: 23%;
}
}
@@media(max-width: 985px) {
.holder .post, .attachment .post {
width: 31.8%;
height: auto;
}
.entry-content {
width: 100%;
}
.holder-block .post{
width:23.5%;
height:100px;
overflow:hidden;
}
.holder-block .entry-content {
width: 100%;
height: 62px;
}
.attachment .holder-block .post{
width: 23.9% !important;
height: 160px;
overflow: hidden;
}
.attachment .holder-block .entry-content {
width: 100%;
height: 95px;
}
.attachment .entry-content img {
height: 95px;
}
}
@@media(max-width: 692px) {
.holder .post:nth-child(3n+0), .attachment .columns4 .post:nth-child(3n+0), .holder-block .post:nth-child(3n+0){
margin-right:0px;
}
}
@@media(max-width: 715px) {
.main-menu {
float: left;
}
.holder-block .post{
width:23.3%;
height:98px;
overflow:hidden;
}
.holder-block .entry-content {
width: 100%;
height: 62px;
}
.attachment .holder-block .post{
width:23.3% !important;
height:100px;
overflow:hidden;
}
.attachment .holder-block .entry-content {
width: 100%;
height: 62px;
}
.attachment .entry-content img {
height: auto;
}
h1.entry-title {
height: 23px !important;
font-size: 11px;
overflow: hidden;
}
}
@@media(max-width: 540px) {
.holder .post, .attachment .post {
width: 31.4%;
height:110px;
margin-right: 1%;
}
.entry-content {
width: 100%;
height: 65px !important;
}
.entry-content img{
max-width:100%;
height:auto;
}
.holder-block .post{
width:22.9%;
height:81px;
overflow:hidden;
}
.attachment .holder-block .entry-content{
height:69px;
}
.attachment .holder-block .post{
width: 23.2% !important;
height: 107px;
}
.holder-block .entry-content {
width: 100%;
height: 43px;
}
@@media(max-width: 540px) {
.holder-block .post{
margin-right:1%;
}
#content h2, #content h1 {
font-size: 11px;
}
.attachment .entry-content img {
height: 60px;
}
}

@@media only screen and (max-width: 479px) and (min-width: 340px){
.holder-block .post{
margin-right:1%;
}
}

</style>

<style>
/* @@import url("//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,300,700"); */
meta.foundation-mq-small {
  font-family: "/only screen and (max-width: 40em)/";
  width: 0em; }

meta.foundation-mq-medium {
  font-family: "/only screen and (min-width:40.063em) and (max-width:64em)/";
  width: 40.063em; }

meta.foundation-mq-large {
  font-family: "/only screen and (min-width:64.063em)/";
  width: 64.063em; }

meta.foundation-mq-xlarge {
  font-family: "/only screen and (min-width:90.063em)/";
  width: 90.063em; }

meta.foundation-mq-xxlarge {
  font-family: "/only screen and (min-width:120.063em)/";
  width: 120.063em; }

*,
*:before,
*:after {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box; }

html,
body {
  font-size: 100%; }

body {
  color: #222222;
  padding: 0;
  margin: 0;
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  font-weight: normal;
  font-style: normal;
  line-height: 1;
  position: relative;
  cursor: default; }

a:hover {
  cursor: pointer; }
/*
img,
object,
embed {
  max-width: 100%;
  height: auto; }*/

object,
embed {
  height: 100%; }

img {
  -ms-interpolation-mode: bicubic; }

#map_canvas img,
#map_canvas embed,
#map_canvas object,
.map_canvas img,
.map_canvas embed,
.map_canvas object {
  max-width: none !important; }

.left {
  float: left !important; }

.right {
  float: right !important; }

.clearfix {
  *zoom: 1; }
  .clearfix:before, .clearfix:after {
    content: " ";
    display: table; }
  .clearfix:after {
    clear: both; }

.text-left {
  text-align: left !important; }

.text-right {
  text-align: right !important; }

.text-center {
  text-align: center !important; }

.text-justify {
  text-align: justify !important; }

.hide {
  display: none; }

.antialiased {
  -webkit-font-smoothing: antialiased; }

img {
  display: inline-block;
  vertical-align: middle; }

textarea {
  height: auto;
  min-height: 50px; }

select {
  width: 100%; }

.row {
  width: 100%;
  margin-left: auto;
  margin-right: auto;
  margin-top: 0;
  margin-bottom: 0;
  max-width: 62.5em;
  *zoom: 1; }
  .row:before, .row:after {
    content: " ";
    display: table; }
  .row:after {
    clear: both; }
  .row.collapse > .column,
  .row.collapse > .columns {
    position: relative;
    padding-left: 0;
    padding-right: 0;
    float: left; }
  .row.collapse .row {
    margin-left: 0;
    margin-right: 0; }
  .row .row {
    width: auto;
    margin-left: -0.9375em;
    margin-right: -0.9375em;
    margin-top: 0;
    margin-bottom: 0;
    max-width: none;
    *zoom: 1; }
    .row .row:before, .row .row:after {
      content: " ";
      display: table; }
    .row .row:after {
      clear: both; }
    .row .row.collapse {
      width: auto;
      margin: 0;
      max-width: none;
      *zoom: 1; }
      .row .row.collapse:before, .row .row.collapse:after {
        content: " ";
        display: table; }
      .row .row.collapse:after {
        clear: both; }

.column,
.columns {
  position: relative;
  padding-left: 0.9375em;
  padding-right: 0.9375em;
  width: 100%;
  float: left; }

@@media only screen {
  .small-push-1 {
    position: relative;
    left: 8.33333%;
    right: auto; }

  .small-pull-1 {
    position: relative;
    right: 8.33333%;
    left: auto; }

  .small-push-2 {
    position: relative;
    left: 16.66667%;
    right: auto; }

  .small-pull-2 {
    position: relative;
    right: 16.66667%;
    left: auto; }

  .small-push-3 {
    position: relative;
    left: 25%;
    right: auto; }

  .small-pull-3 {
    position: relative;
    right: 25%;
    left: auto; }

  .small-push-4 {
    position: relative;
    left: 33.33333%;
    right: auto; }

  .small-pull-4 {
    position: relative;
    right: 33.33333%;
    left: auto; }

  .small-push-5 {
    position: relative;
    left: 41.66667%;
    right: auto; }

  .small-pull-5 {
    position: relative;
    right: 41.66667%;
    left: auto; }

  .small-push-6 {
    position: relative;
    left: 50%;
    right: auto; }

  .small-pull-6 {
    position: relative;
    right: 50%;
    left: auto; }

  .small-push-7 {
    position: relative;
    left: 58.33333%;
    right: auto; }

  .small-pull-7 {
    position: relative;
    right: 58.33333%;
    left: auto; }

  .small-push-8 {
    position: relative;
    left: 66.66667%;
    right: auto; }

  .small-pull-8 {
    position: relative;
    right: 66.66667%;
    left: auto; }

  .small-push-9 {
    position: relative;
    left: 75%;
    right: auto; }

  .small-pull-9 {
    position: relative;
    right: 75%;
    left: auto; }

  .small-push-10 {
    position: relative;
    left: 83.33333%;
    right: auto; }

  .small-pull-10 {
    position: relative;
    right: 83.33333%;
    left: auto; }

  .small-push-11 {
    position: relative;
    left: 91.66667%;
    right: auto; }

  .small-pull-11 {
    position: relative;
    right: 91.66667%;
    left: auto; }

  .column,
  .columns {
    position: relative;
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    float: left; }

  .small-1 {
    position: relative;
    width: 8.33333%; }

  .small-2 {
    position: relative;
    width: 16.66667%; }

  .small-3 {
    position: relative;
    width: 25%; }

  .small-4 {
    position: relative;
    width: 33.33333%; }

  .small-5 {
    position: relative;
    width: 41.66667%; }

  .small-6 {
    position: relative;
    width: 50%; }

  .small-7 {
    position: relative;
    width: 58.33333%; }

  .small-8 {
    position: relative;
    width: 66.66667%; }

  .small-9 {
    position: relative;
    width: 75%; }

  .small-10 {
    position: relative;
    width: 83.33333%; }

  .small-11 {
    position: relative;
    width: 91.66667%; }

  .small-12 {
    position: relative;
    width: 100%; }

  .small-offset-0 {
    position: relative;
    margin-left: 0%; }

  .small-offset-1 {
    position: relative;
    margin-left: 8.33333%; }

  .small-offset-2 {
    position: relative;
    margin-left: 16.66667%; }

  .small-offset-3 {
    position: relative;
    margin-left: 25%; }

  .small-offset-4 {
    position: relative;
    margin-left: 33.33333%; }

  .small-offset-5 {
    position: relative;
    margin-left: 41.66667%; }

  .small-offset-6 {
    position: relative;
    margin-left: 50%; }

  .small-offset-7 {
    position: relative;
    margin-left: 58.33333%; }

  .small-offset-8 {
    position: relative;
    margin-left: 66.66667%; }

  .small-offset-9 {
    position: relative;
    margin-left: 75%; }

  .small-offset-10 {
    position: relative;
    margin-left: 83.33333%; }

  [class*="column"] + [class*="column"]:last-child {
    float: right; }

  [class*="column"] + [class*="column"].end {
    float: left; }

  .column.small-centered,
  .columns.small-centered {
    position: relative;
    margin-left: auto;
    margin-right: auto;
    float: none !important; }

  .column.small-uncentered,
  .columns.small-uncentered {
    margin-left: 0;
    margin-right: 0;
    float: left !important; }

  .column.small-uncentered.opposite,
  .columns.small-uncentered.opposite {
    float: right !important; } }
@@media only screen and (min-width: 40.063em) {
  .medium-push-1 {
    position: relative;
    left: 8.33333%;
    right: auto; }

  .medium-pull-1 {
    position: relative;
    right: 8.33333%;
    left: auto; }

  .medium-push-2 {
    position: relative;
    left: 16.66667%;
    right: auto; }

  .medium-pull-2 {
    position: relative;
    right: 16.66667%;
    left: auto; }

  .medium-push-3 {
    position: relative;
    left: 25%;
    right: auto; }

  .medium-pull-3 {
    position: relative;
    right: 25%;
    left: auto; }

  .medium-push-4 {
    position: relative;
    left: 33.33333%;
    right: auto; }

  .medium-pull-4 {
    position: relative;
    right: 33.33333%;
    left: auto; }

  .medium-push-5 {
    position: relative;
    left: 41.66667%;
    right: auto; }

  .medium-pull-5 {
    position: relative;
    right: 41.66667%;
    left: auto; }

  .medium-push-6 {
    position: relative;
    left: 50%;
    right: auto; }

  .medium-pull-6 {
    position: relative;
    right: 50%;
    left: auto; }

  .medium-push-7 {
    position: relative;
    left: 58.33333%;
    right: auto; }

  .medium-pull-7 {
    position: relative;
    right: 58.33333%;
    left: auto; }

  .medium-push-8 {
    position: relative;
    left: 66.66667%;
    right: auto; }

  .medium-pull-8 {
    position: relative;
    right: 66.66667%;
    left: auto; }

  .medium-push-9 {
    position: relative;
    left: 75%;
    right: auto; }

  .medium-pull-9 {
    position: relative;
    right: 75%;
    left: auto; }

  .medium-push-10 {
    position: relative;
    left: 83.33333%;
    right: auto; }

  .medium-pull-10 {
    position: relative;
    right: 83.33333%;
    left: auto; }

  .medium-push-11 {
    position: relative;
    left: 91.66667%;
    right: auto; }

  .medium-pull-11 {
    position: relative;
    right: 91.66667%;
    left: auto; }

  .column,
  .columns {
    position: relative;
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    float: left; }

  .medium-1 {
    position: relative;
    width: 8.33333%; }

  .medium-2 {
    position: relative;
    width: 16.66667%; }

  .medium-3 {
    position: relative;
    width: 25%; }

  .medium-4 {
    position: relative;
    width: 33.33333%; }

  .medium-5 {
    position: relative;
    width: 41.66667%; }

  .medium-6 {
    position: relative;
    width: 50%; }

  .medium-7 {
    position: relative;
    width: 58.33333%; }

  .medium-8 {
    position: relative;
    width: 66.66667%; }

  .medium-9 {
    position: relative;
    width: 75%; }

  .medium-10 {
    position: relative;
    width: 83.33333%; }

  .medium-11 {
    position: relative;
    width: 91.66667%; }

  .medium-12 {
    position: relative;
    width: 100%; }

  .medium-offset-0 {
    position: relative;
    margin-left: 0%; }

  .medium-offset-1 {
    position: relative;
    margin-left: 8.33333%; }

  .medium-offset-2 {
    position: relative;
    margin-left: 16.66667%; }

  .medium-offset-3 {
    position: relative;
    margin-left: 25%; }

  .medium-offset-4 {
    position: relative;
    margin-left: 33.33333%; }

  .medium-offset-5 {
    position: relative;
    margin-left: 41.66667%; }

  .medium-offset-6 {
    position: relative;
    margin-left: 50%; }

  .medium-offset-7 {
    position: relative;
    margin-left: 58.33333%; }

  .medium-offset-8 {
    position: relative;
    margin-left: 66.66667%; }

  .medium-offset-9 {
    position: relative;
    margin-left: 75%; }

  .medium-offset-10 {
    position: relative;
    margin-left: 83.33333%; }

  [class*="column"] + [class*="column"]:last-child {
    float: right; }

  [class*="column"] + [class*="column"].end {
    float: left; }

  .column.medium-centered,
  .columns.medium-centered {
    position: relative;
    margin-left: auto;
    margin-right: auto;
    float: none !important; }

  .column.medium-uncentered,
  .columns.medium-uncentered {
    margin-left: 0;
    margin-right: 0;
    float: left !important; }

  .column.medium-uncentered.opposite,
  .columns.medium-uncentered.opposite {
    float: right !important; }

  .push-1 {
    position: relative;
    left: 8.33333%;
    right: auto; }

  .pull-1 {
    position: relative;
    right: 8.33333%;
    left: auto; }

  .push-2 {
    position: relative;
    left: 16.66667%;
    right: auto; }

  .pull-2 {
    position: relative;
    right: 16.66667%;
    left: auto; }

  .push-3 {
    position: relative;
    left: 25%;
    right: auto; }

  .pull-3 {
    position: relative;
    right: 25%;
    left: auto; }

  .push-4 {
    position: relative;
    left: 33.33333%;
    right: auto; }

  .pull-4 {
    position: relative;
    right: 33.33333%;
    left: auto; }

  .push-5 {
    position: relative;
    left: 41.66667%;
    right: auto; }

  .pull-5 {
    position: relative;
    right: 41.66667%;
    left: auto; }

  .push-6 {
    position: relative;
    left: 50%;
    right: auto; }

  .pull-6 {
    position: relative;
    right: 50%;
    left: auto; }

  .push-7 {
    position: relative;
    left: 58.33333%;
    right: auto; }

  .pull-7 {
    position: relative;
    right: 58.33333%;
    left: auto; }

  .push-8 {
    position: relative;
    left: 66.66667%;
    right: auto; }

  .pull-8 {
    position: relative;
    right: 66.66667%;
    left: auto; }

  .push-9 {
    position: relative;
    left: 75%;
    right: auto; }

  .pull-9 {
    position: relative;
    right: 75%;
    left: auto; }

  .push-10 {
    position: relative;
    left: 83.33333%;
    right: auto; }

  .pull-10 {
    position: relative;
    right: 83.33333%;
    left: auto; }

  .push-11 {
    position: relative;
    left: 91.66667%;
    right: auto; }

  .pull-11 {
    position: relative;
    right: 91.66667%;
    left: auto; } }
@@media only screen and (min-width: 64.063em) {
  .large-push-1 {
    position: relative;
    left: 8.33333%;
    right: auto; }

  .large-pull-1 {
    position: relative;
    right: 8.33333%;
    left: auto; }

  .large-push-2 {
    position: relative;
    left: 16.66667%;
    right: auto; }

  .large-pull-2 {
    position: relative;
    right: 16.66667%;
    left: auto; }

  .large-push-3 {
    position: relative;
    left: 25%;
    right: auto; }

  .large-pull-3 {
    position: relative;
    right: 25%;
    left: auto; }

  .large-push-4 {
    position: relative;
    left: 33.33333%;
    right: auto; }

  .large-pull-4 {
    position: relative;
    right: 33.33333%;
    left: auto; }

  .large-push-5 {
    position: relative;
    left: 41.66667%;
    right: auto; }

  .large-pull-5 {
    position: relative;
    right: 41.66667%;
    left: auto; }

  .large-push-6 {
    position: relative;
    left: 50%;
    right: auto; }

  .large-pull-6 {
    position: relative;
    right: 50%;
    left: auto; }

  .large-push-7 {
    position: relative;
    left: 58.33333%;
    right: auto; }

  .large-pull-7 {
    position: relative;
    right: 58.33333%;
    left: auto; }

  .large-push-8 {
    position: relative;
    left: 66.66667%;
    right: auto; }

  .large-pull-8 {
    position: relative;
    right: 66.66667%;
    left: auto; }

  .large-push-9 {
    position: relative;
    left: 75%;
    right: auto; }

  .large-pull-9 {
    position: relative;
    right: 75%;
    left: auto; }

  .large-push-10 {
    position: relative;
    left: 83.33333%;
    right: auto; }

  .large-pull-10 {
    position: relative;
    right: 83.33333%;
    left: auto; }

  .large-push-11 {
    position: relative;
    left: 91.66667%;
    right: auto; }

  .large-pull-11 {
    position: relative;
    right: 91.66667%;
    left: auto; }

  .column,
  .columns {
    position: relative;
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    float: left; }

  .large-1 {
    position: relative;
    width: 8.33333%; }

  .large-2 {
    position: relative;
    width: 16.66667%; }

  .large-3 {
    position: relative;
    width: 25%; }

  .large-4 {
    position: relative;
    width: 33.33333%; }

  .large-5 {
    position: relative;
    width: 41.66667%; }

  .large-6 {
    position: relative;
    width: 50%; }

  .large-7 {
    position: relative;
    width: 58.33333%; }

  .large-8 {
    position: relative;
    width: 66.66667%; }

  .large-9 {
    position: relative;
    width: 75%; }

  .large-10 {
    position: relative;
    width: 83.33333%; }

  .large-11 {
    position: relative;
    width: 91.66667%; }

  .large-12 {
    position: relative;
    width: 100%; }

  .large-offset-0 {
    position: relative;
    margin-left: 0%; }

  .large-offset-1 {
    position: relative;
    margin-left: 8.33333%; }

  .large-offset-2 {
    position: relative;
    margin-left: 16.66667%; }

  .large-offset-3 {
    position: relative;
    margin-left: 25%; }

  .large-offset-4 {
    position: relative;
    margin-left: 33.33333%; }

  .large-offset-5 {
    position: relative;
    margin-left: 41.66667%; }

  .large-offset-6 {
    position: relative;
    margin-left: 50%; }

  .large-offset-7 {
    position: relative;
    margin-left: 58.33333%; }

  .large-offset-8 {
    position: relative;
    margin-left: 66.66667%; }

  .large-offset-9 {
    position: relative;
    margin-left: 75%; }

  .large-offset-10 {
    position: relative;
    margin-left: 83.33333%; }

  [class*="column"] + [class*="column"]:last-child {
    float: right; }

  [class*="column"] + [class*="column"].end {
    float: left; }

  .column.large-centered,
  .columns.large-centered {
    position: relative;
    margin-left: auto;
    margin-right: auto;
    float: none !important; }

  .column.large-uncentered,
  .columns.large-uncentered {
    margin-left: 0;
    margin-right: 0;
    float: left !important; }

  .column.large-uncentered.opposite,
  .columns.large-uncentered.opposite {
    float: right !important; } }
@@media only screen and (min-width: 90.063em) {
  .xlarge-push-1 {
    position: relative;
    left: 8.33333%;
    right: auto; }

  .xlarge-pull-1 {
    position: relative;
    right: 8.33333%;
    left: auto; }

  .xlarge-push-2 {
    position: relative;
    left: 16.66667%;
    right: auto; }

  .xlarge-pull-2 {
    position: relative;
    right: 16.66667%;
    left: auto; }

  .xlarge-push-3 {
    position: relative;
    left: 25%;
    right: auto; }

  .xlarge-pull-3 {
    position: relative;
    right: 25%;
    left: auto; }

  .xlarge-push-4 {
    position: relative;
    left: 33.33333%;
    right: auto; }

  .xlarge-pull-4 {
    position: relative;
    right: 33.33333%;
    left: auto; }

  .xlarge-push-5 {
    position: relative;
    left: 41.66667%;
    right: auto; }

  .xlarge-pull-5 {
    position: relative;
    right: 41.66667%;
    left: auto; }

  .xlarge-push-6 {
    position: relative;
    left: 50%;
    right: auto; }

  .xlarge-pull-6 {
    position: relative;
    right: 50%;
    left: auto; }

  .xlarge-push-7 {
    position: relative;
    left: 58.33333%;
    right: auto; }

  .xlarge-pull-7 {
    position: relative;
    right: 58.33333%;
    left: auto; }

  .xlarge-push-8 {
    position: relative;
    left: 66.66667%;
    right: auto; }

  .xlarge-pull-8 {
    position: relative;
    right: 66.66667%;
    left: auto; }

  .xlarge-push-9 {
    position: relative;
    left: 75%;
    right: auto; }

  .xlarge-pull-9 {
    position: relative;
    right: 75%;
    left: auto; }

  .xlarge-push-10 {
    position: relative;
    left: 83.33333%;
    right: auto; }

  .xlarge-pull-10 {
    position: relative;
    right: 83.33333%;
    left: auto; }

  .xlarge-push-11 {
    position: relative;
    left: 91.66667%;
    right: auto; }

  .xlarge-pull-11 {
    position: relative;
    right: 91.66667%;
    left: auto; }

  .column,
  .columns {
    position: relative;
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    float: left; }

  .xlarge-1 {
    position: relative;
    width: 8.33333%; }

  .xlarge-2 {
    position: relative;
    width: 16.66667%; }

  .xlarge-3 {
    position: relative;
    width: 25%; }

  .xlarge-4 {
    position: relative;
    width: 33.33333%; }

  .xlarge-5 {
    position: relative;
    width: 41.66667%; }

  .xlarge-6 {
    position: relative;
    width: 50%; }

  .xlarge-7 {
    position: relative;
    width: 58.33333%; }

  .xlarge-8 {
    position: relative;
    width: 66.66667%; }

  .xlarge-9 {
    position: relative;
    width: 75%; }

  .xlarge-10 {
    position: relative;
    width: 83.33333%; }

  .xlarge-11 {
    position: relative;
    width: 91.66667%; }

  .xlarge-12 {
    position: relative;
    width: 100%; }

  .xlarge-offset-0 {
    position: relative;
    margin-left: 0%; }

  .xlarge-offset-1 {
    position: relative;
    margin-left: 8.33333%; }

  .xlarge-offset-2 {
    position: relative;
    margin-left: 16.66667%; }

  .xlarge-offset-3 {
    position: relative;
    margin-left: 25%; }

  .xlarge-offset-4 {
    position: relative;
    margin-left: 33.33333%; }

  .xlarge-offset-5 {
    position: relative;
    margin-left: 41.66667%; }

  .xlarge-offset-6 {
    position: relative;
    margin-left: 50%; }

  .xlarge-offset-7 {
    position: relative;
    margin-left: 58.33333%; }

  .xlarge-offset-8 {
    position: relative;
    margin-left: 66.66667%; }

  .xlarge-offset-9 {
    position: relative;
    margin-left: 75%; }

  .xlarge-offset-10 {
    position: relative;
    margin-left: 83.33333%; }

  [class*="column"] + [class*="column"]:last-child {
    float: right; }

  [class*="column"] + [class*="column"].end {
    float: left; }

  .column.xlarge-centered,
  .columns.xlarge-centered {
    position: relative;
    margin-left: auto;
    margin-right: auto;
    float: none !important; }

  .column.xlarge-uncentered,
  .columns.xlarge-uncentered {
    margin-left: 0;
    margin-right: 0;
    float: left !important; }

  .column.xlarge-uncentered.opposite,
  .columns.xlarge-uncentered.opposite {
    float: right !important; } }
@@media only screen and (min-width: 120.063em) {
  .xxlarge-push-1 {
    position: relative;
    left: 8.33333%;
    right: auto; }

  .xxlarge-pull-1 {
    position: relative;
    right: 8.33333%;
    left: auto; }

  .xxlarge-push-2 {
    position: relative;
    left: 16.66667%;
    right: auto; }

  .xxlarge-pull-2 {
    position: relative;
    right: 16.66667%;
    left: auto; }

  .xxlarge-push-3 {
    position: relative;
    left: 25%;
    right: auto; }

  .xxlarge-pull-3 {
    position: relative;
    right: 25%;
    left: auto; }

  .xxlarge-push-4 {
    position: relative;
    left: 33.33333%;
    right: auto; }

  .xxlarge-pull-4 {
    position: relative;
    right: 33.33333%;
    left: auto; }

  .xxlarge-push-5 {
    position: relative;
    left: 41.66667%;
    right: auto; }

  .xxlarge-pull-5 {
    position: relative;
    right: 41.66667%;
    left: auto; }

  .xxlarge-push-6 {
    position: relative;
    left: 50%;
    right: auto; }

  .xxlarge-pull-6 {
    position: relative;
    right: 50%;
    left: auto; }

  .xxlarge-push-7 {
    position: relative;
    left: 58.33333%;
    right: auto; }

  .xxlarge-pull-7 {
    position: relative;
    right: 58.33333%;
    left: auto; }

  .xxlarge-push-8 {
    position: relative;
    left: 66.66667%;
    right: auto; }

  .xxlarge-pull-8 {
    position: relative;
    right: 66.66667%;
    left: auto; }

  .xxlarge-push-9 {
    position: relative;
    left: 75%;
    right: auto; }

  .xxlarge-pull-9 {
    position: relative;
    right: 75%;
    left: auto; }

  .xxlarge-push-10 {
    position: relative;
    left: 83.33333%;
    right: auto; }

  .xxlarge-pull-10 {
    position: relative;
    right: 83.33333%;
    left: auto; }

  .xxlarge-push-11 {
    position: relative;
    left: 91.66667%;
    right: auto; }

  .xxlarge-pull-11 {
    position: relative;
    right: 91.66667%;
    left: auto; }

  .column,
  .columns {
    position: relative;
    padding-left: 0.9375em;
    padding-right: 0.9375em;
    float: left; }

  .xxlarge-1 {
    position: relative;
    width: 8.33333%; }

  .xxlarge-2 {
    position: relative;
    width: 16.66667%; }

  .xxlarge-3 {
    position: relative;
    width: 25%; }

  .xxlarge-4 {
    position: relative;
    width: 33.33333%; }

  .xxlarge-5 {
    position: relative;
    width: 41.66667%; }

  .xxlarge-6 {
    position: relative;
    width: 50%; }

  .xxlarge-7 {
    position: relative;
    width: 58.33333%; }

  .xxlarge-8 {
    position: relative;
    width: 66.66667%; }

  .xxlarge-9 {
    position: relative;
    width: 75%; }

  .xxlarge-10 {
    position: relative;
    width: 83.33333%; }

  .xxlarge-11 {
    position: relative;
    width: 91.66667%; }

  .xxlarge-12 {
    position: relative;
    width: 100%; }

  .xxlarge-offset-0 {
    position: relative;
    margin-left: 0%; }

  .xxlarge-offset-1 {
    position: relative;
    margin-left: 8.33333%; }

  .xxlarge-offset-2 {
    position: relative;
    margin-left: 16.66667%; }

  .xxlarge-offset-3 {
    position: relative;
    margin-left: 25%; }

  .xxlarge-offset-4 {
    position: relative;
    margin-left: 33.33333%; }

  .xxlarge-offset-5 {
    position: relative;
    margin-left: 41.66667%; }

  .xxlarge-offset-6 {
    position: relative;
    margin-left: 50%; }

  .xxlarge-offset-7 {
    position: relative;
    margin-left: 58.33333%; }

  .xxlarge-offset-8 {
    position: relative;
    margin-left: 66.66667%; }

  .xxlarge-offset-9 {
    position: relative;
    margin-left: 75%; }

  .xxlarge-offset-10 {
    position: relative;
    margin-left: 83.33333%; }

  [class*="column"] + [class*="column"]:last-child {
    float: right; }

  [class*="column"] + [class*="column"].end {
    float: left; }

  .column.xxlarge-centered,
  .columns.xxlarge-centered {
    position: relative;
    margin-left: auto;
    margin-right: auto;
    float: none !important; }

  .column.xxlarge-uncentered,
  .columns.xxlarge-uncentered {
    margin-left: 0;
    margin-right: 0;
    float: left !important; }

  .column.xxlarge-uncentered.opposite,
  .columns.xxlarge-uncentered.opposite {
    float: right !important; } }
meta.foundation-mq-topbar {
  font-family: "/only screen and (min-width:40.063em)/";
  width: 58.75em; }

/* Wrapped around .top-bar to contain to grid width */
.contain-to-grid {
  width: 100%;
  background: #333333; }
  .contain-to-grid .top-bar {
    margin-bottom: 0; }

.fixed {
  width: 100%;
  left: 0;
  position: fixed;
  top: 0;
  z-index: 99; }
  .fixed.expanded:not(.top-bar) {
    overflow-y: auto;
    height: auto;
    width: 100%;
    max-height: 100%; }
    .fixed.expanded:not(.top-bar) .title-area {
      position: fixed;
      width: 100%;
      z-index: 99; }
    .fixed.expanded:not(.top-bar) .top-bar-section {
      z-index: 98;
      margin-top: 45px; }

.top-bar {
  overflow: hidden;
  height: 45px;
  line-height: 45px;
  position: relative;
  background: #333333;
  margin-bottom: 0; }
  .top-bar ul {
    margin-bottom: 0;
    list-style: none; }
  .top-bar .row {
    max-width: none; }
  .top-bar form,
  .top-bar input {
    margin-bottom: 0; }
  .top-bar input {
    height: auto;
    padding-top: .35rem;
    padding-bottom: .35rem;
    font-size: 0.75rem; }
  .top-bar .button {
    padding-top: .45rem;
    padding-bottom: .35rem;
    margin-bottom: 0;
    font-size: 0.75rem; }
  .top-bar .title-area {
    position: relative;
    margin: 0; }
  .top-bar .name {
    height: 45px;
    margin: 0;
    font-size: 16px; }
    .top-bar .name h1 {
      line-height: 45px;
      font-size: 1.0625rem;
      margin: 0; }
      .top-bar .name h1 a {
        font-weight: normal;
        color: white;
        width: 50%;
        display: block;
        padding: 0 15px; }
  .top-bar .toggle-topbar {
    position: absolute;
    right: 0;
    top: 0; }
    .top-bar .toggle-topbar a {
      color: white;
      text-transform: uppercase;
      font-size: 0.8125rem;
      font-weight: bold;
      position: relative;
      display: block;
      padding: 0 15px;
      height: 45px;
      line-height: 45px; }
    .top-bar .toggle-topbar.menu-icon {
      right: 15px;
      top: 50%;
      margin-top: -16px;
      padding-left: 40px; }
      .top-bar .toggle-topbar.menu-icon a {
        text-indent: -48px;
        width: 34px;
        height: 34px;
        line-height: 33px;
        padding: 0;
        color: white; }
        .top-bar .toggle-topbar.menu-icon a span {
          position: absolute;
          right: 0;
          display: block;
          width: 16px;
          height: 0;
          -webkit-box-shadow: 0 10px 0 1px white, 0 16px 0 1px white, 0 22px 0 1px white;
          box-shadow: 0 10px 0 1px white, 0 16px 0 1px white, 0 22px 0 1px white; }
  .top-bar.expanded {
    height: auto;
    background: transparent; }
    .top-bar.expanded .title-area {
      background: #333333; }
    .top-bar.expanded .toggle-topbar a {
      color: #888888; }
      .top-bar.expanded .toggle-topbar a span {
        -webkit-box-shadow: 0 10px 0 1px #888888, 0 16px 0 1px #888888, 0 22px 0 1px #888888;
        box-shadow: 0 10px 0 1px #888888, 0 16px 0 1px #888888, 0 22px 0 1px #888888; }

.top-bar-section {
  left: 0;
  position: relative;
  width: auto;
  -webkit-transition: left 300ms ease-out;
  -moz-transition: left 300ms ease-out;
  transition: left 300ms ease-out; }
  .top-bar-section ul {
    width: 100%;
    height: auto;
    display: block;
    background: #333333;
    font-size: 16px;
    margin: 0; }
  .top-bar-section .divider,
  .top-bar-section [role="separator"] {
    border-top: solid 1px #1a1a1a;
    clear: both;
    height: 1px;
    width: 100%; }
  .top-bar-section ul li > a {
    display: block;
    width: 100%;
    color: white;
    padding: 12px 0 12px 0;
    padding-left: 15px;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    font-size: 0.8125rem;
    font-weight: normal;
    background: #333333; }
    .top-bar-section ul li > a.button {
      background: #2ba6cb;
      font-size: 0.8125rem;
      padding-right: 15px;
      padding-left: 15px; }
      .top-bar-section ul li > a.button:hover {
        background: #2284a1; }
    .top-bar-section ul li > a.button.secondary {
      background: #e9e9e9; }
      .top-bar-section ul li > a.button.secondary:hover {
        background: #d0d0d0; }
    .top-bar-section ul li > a.button.success {
      background: #5da423; }
      .top-bar-section ul li > a.button.success:hover {
        background: #457a1a; }
    .top-bar-section ul li > a.button.alert {
      background: #c60f13; }
      .top-bar-section ul li > a.button.alert:hover {
        background: #970b0e; }
  .top-bar-section ul li:hover > a {
    background: #272727;
    color: white; }
  .top-bar-section ul li.active > a {
    background: #2ba6cb;
    color: white; }
    .top-bar-section ul li.active > a:hover {
      background: #2795b6; }
  .top-bar-section .has-form {
    padding: 15px; }
  .top-bar-section .has-dropdown {
    position: relative; }
    .top-bar-section .has-dropdown > a:after {
      content: "";
      display: block;
      width: 0;
      height: 0;
      border: inset 5px;
      border-color: transparent transparent transparent rgba(255, 255, 255, 0.4);
      border-left-style: solid;
      margin-right: 15px;
      margin-top: -4.5px;
      position: absolute;
      top: 50%;
      right: 0; }
    .top-bar-section .has-dropdown.moved {
      position: static; }
      .top-bar-section .has-dropdown.moved > .dropdown {
        display: block; }
  .top-bar-section .dropdown {
    position: absolute;
    left: 100%;
    top: 0;
    display: none;
    z-index: 99; }
    .top-bar-section .dropdown li {
      width: 100%;
      height: auto; }
      .top-bar-section .dropdown li a {
        font-weight: normal;
        padding: 8px 15px; }
        .top-bar-section .dropdown li a.parent-link {
          font-weight: normal; }
      .top-bar-section .dropdown li.title h5 {
        margin-bottom: 0; }
        .top-bar-section .dropdown li.title h5 a {
          color: white;
          line-height: 22.5px;
          display: block; }
    .top-bar-section .dropdown label {
      padding: 8px 15px 2px;
      margin-bottom: 0;
      text-transform: uppercase;
      color: #777777;
      font-weight: bold;
      font-size: 0.625rem; }

.js-generated {
  display: block; }

@@media only screen and (min-width: 40.063em) {
  .top-bar {
    background: #333333;
    *zoom: 1;
    overflow: visible; }
    .top-bar:before, .top-bar:after {
      content: " ";
      display: table; }
    .top-bar:after {
      clear: both; }
    .top-bar .toggle-topbar {
      display: none; }
    .top-bar .title-area {
      float: left; }
    .top-bar .name h1 a {
      width: auto; }
    .top-bar input,
    .top-bar .button {
      font-size: 0.875rem;
      position: relative;
      top: 7px; }
    .top-bar.expanded {
      background: #333333; }

  .contain-to-grid .top-bar {
    max-width: 62.5em;
    margin: 0 auto;
    margin-bottom: 0; }

  .top-bar-section {
    -webkit-transition: none 0 0;
    -moz-transition: none 0 0;
    transition: none 0 0;
    left: 0 !important; }
    .top-bar-section ul {
      width: auto;
      height: auto !important;
      display: inline; }
      .top-bar-section ul li {
        float: left; }
        .top-bar-section ul li .js-generated {
          display: none; }
    .top-bar-section li.hover > a:not(.button) {
      background: #272727;
      color: white; }
    .top-bar-section li a:not(.button) {
      padding: 0 15px;
      line-height: 45px;
      background: #333333; }
      .top-bar-section li a:not(.button):hover {
        background: #272727; }
    .top-bar-section .has-dropdown > a {
      padding-right: 35px !important; }
      .top-bar-section .has-dropdown > a:after {
        content: "";
        display: block;
        width: 0;
        height: 0;
        border: inset 5px;
        border-color: rgba(255, 255, 255, 0.4) transparent transparent transparent;
        border-top-style: solid;
        margin-top: -2.5px;
        top: 22.5px; }
    .top-bar-section .has-dropdown.moved {
      position: relative; }
      .top-bar-section .has-dropdown.moved > .dropdown {
        display: none; }
    .top-bar-section .has-dropdown.hover > .dropdown, .top-bar-section .has-dropdown.not-click:hover > .dropdown {
      display: block; }
    .top-bar-section .has-dropdown .dropdown li.has-dropdown > a:after {
      border: none;
      content: "\00bb";
      top: 1rem;
      margin-top: -2px;
      right: 5px; }
    .top-bar-section .dropdown {
      left: 0;
      top: auto;
      background: transparent;
      min-width: 100%; }
      .top-bar-section .dropdown li a {
        color: white;
        line-height: 1;
        white-space: nowrap;
        padding: 12px 15px;
        background: #333333; }
      .top-bar-section .dropdown li label {
        white-space: nowrap;
        background: #333333; }
      .top-bar-section .dropdown li .dropdown {
        left: 100%;
        top: 0; }
    .top-bar-section > ul > .divider, .top-bar-section > ul > [role="separator"] {
      border-bottom: none;
      border-top: none;
      border-right: solid 1px #4d4d4d;
      clear: none;
      height: 45px;
      width: 0; }
    .top-bar-section .has-form {
      background: #333333;
      padding: 0 15px;
      height: 45px; }
    .top-bar-section ul.right li .dropdown {
      left: auto;
      right: 0; }
      .top-bar-section ul.right li .dropdown li .dropdown {
        right: 100%; }

  .no-js .top-bar-section ul li:hover > a {
    background: #272727;
    color: white; }
  .no-js .top-bar-section ul li:active > a {
    background: #2ba6cb;
    color: white; }
  .no-js .top-bar-section .has-dropdown:hover > .dropdown {
    display: block; } }
.breadcrumbs {
  display: block;
  padding: 0.5625rem 0.875rem 0.5625rem;
  overflow: hidden;
  margin-left: 0;
  list-style: none;
  border-style: solid;
  border-width: 1px;
  background-color: #f6f6f6;
  border-color: gainsboro;
  -webkit-border-radius: 3px;
  border-radius: 3px; }
  .breadcrumbs > * {
    margin: 0;
    float: left;
    font-size: 0.6875rem;
    text-transform: uppercase; }
    .breadcrumbs > *:hover a, .breadcrumbs > *:focus a {
      text-decoration: underline; }
    .breadcrumbs > * a,
    .breadcrumbs > * span {
      text-transform: uppercase;
      color: #2ba6cb; }
    .breadcrumbs > *.current {
      cursor: default;
      color: #333333; }
      .breadcrumbs > *.current a {
        cursor: default;
        color: #333333; }
      .breadcrumbs > *.current:hover, .breadcrumbs > *.current:hover a, .breadcrumbs > *.current:focus, .breadcrumbs > *.current:focus a {
        text-decoration: none; }
    .breadcrumbs > *.unavailable {
      color: #999999; }
      .breadcrumbs > *.unavailable a {
        color: #999999; }
      .breadcrumbs > *.unavailable:hover, .breadcrumbs > *.unavailable:hover a, .breadcrumbs > *.unavailable:focus,
      .breadcrumbs > *.unavailable a:focus {
        text-decoration: none;
        color: #999999;
        cursor: default; }
    .breadcrumbs > *:before {
      content: "/";
      color: #aaaaaa;
      margin: 0 0.75rem;
      position: relative;
      top: 1px; }
    .breadcrumbs > *:first-child:before {
      content: " ";
      margin: 0; }

.alert-box {
  border-style: solid;
  border-width: 1px;
  display: block;
  font-weight: normal;
  margin-bottom: 1.25rem;
  position: relative;
  padding: 0.875rem 1.5rem 0.875rem 0.875rem;
  font-size: 0.8125rem;
  background-color: #2ba6cb;
  border-color: #2795b6;
  color: white; }
  .alert-box .close {
    font-size: 1.375rem;
    padding: 9px 6px 4px;
    line-height: 0;
    position: absolute;
    top: 50%;
    margin-top: -0.6875rem;
    right: 0.25rem;
    color: #333333;
    opacity: 0.3; }
    .alert-box .close:hover, .alert-box .close:focus {
      opacity: 0.5; }
  .alert-box.radius {
    -webkit-border-radius: 3px;
    border-radius: 3px; }
  .alert-box.round {
    -webkit-border-radius: 1000px;
    border-radius: 1000px; }
  .alert-box.success {
    background-color: #5da423;
    border-color: #518f1f;
    color: white; }
  .alert-box.alert {
    background-color: #c60f13;
    border-color: #ae0d11;
    color: white; }
  .alert-box.secondary {
    background-color: #e9e9e9;
    border-color: gainsboro;
    color: #505050; }
  .alert-box.warning {
    background-color: #f08a24;
    border-color: #ea7d10;
    color: white; }
  .alert-box.info {
    background-color: #a0d3e8;
    border-color: #8bc9e3;
    color: #505050; }

.inline-list {
  margin: 0 auto 1.0625rem auto;
  margin-left: -1.375rem;
  margin-right: 0;
  padding: 0;
  list-style: none;
  overflow: hidden; }
  .inline-list > li {
    list-style: none;
    float: left;
    margin-left: 1.375rem;
    display: block; }
    .inline-list > li > * {
      display: block; }

button, .button {
  cursor: pointer;
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  font-weight: normal;
  line-height: normal;
  margin: 0 0 1.25rem;
  position: relative;
  text-decoration: none;
  text-align: center;
  display: inline-block;
  padding-top: 1rem;
  padding-right: 2rem;
  padding-bottom: 1.0625rem;
  padding-left: 2rem;
  font-size: 1rem;
  /*     @@else                            { font-size: $padding - rem-calc(2); } */
  background-color: #2ba6cb;
  border-color: #2795b6;
  color: white;
  -webkit-transition: background-color 300ms ease-out;
  -moz-transition: background-color 300ms ease-out;
  transition: background-color 300ms ease-out;
  padding-top: 1.0625rem;
  padding-bottom: 1rem;
  -webkit-appearance: none;
  border: none;
  font-weight: normal !important; }
  button:hover, button:focus, .button:hover, .button:focus {
    background-color: #2795b6; }
  button:hover, button:focus, .button:hover, .button:focus {
    color: white; }
  button.secondary, .button.secondary {
    background-color: #e9e9e9;
    border-color: gainsboro;
    color: #333333; }
    button.secondary:hover, button.secondary:focus, .button.secondary:hover, .button.secondary:focus {
      background-color: gainsboro; }
    button.secondary:hover, button.secondary:focus, .button.secondary:hover, .button.secondary:focus {
      color: #333333; }
  button.success, .button.success {
    background-color: #5da423;
    border-color: #518f1f;
    color: white; }
    button.success:hover, button.success:focus, .button.success:hover, .button.success:focus {
      background-color: #518f1f; }
    button.success:hover, button.success:focus, .button.success:hover, .button.success:focus {
      color: white; }
  button.alert, .button.alert {
    background-color: #c60f13;
    border-color: #ae0d11;
    color: white; }
    button.alert:hover, button.alert:focus, .button.alert:hover, .button.alert:focus {
      background-color: #ae0d11; }
    button.alert:hover, button.alert:focus, .button.alert:hover, .button.alert:focus {
      color: white; }
  button.large, .button.large {
    padding-top: 1.125rem;
    padding-right: 2.25rem;
    padding-bottom: 1.1875rem;
    padding-left: 2.25rem;
    font-size: 1.25rem;
    /*     @@else                            { font-size: $padding - rem-calc(2); } */ }
  button.small, .button.small {
    padding-top: 0.875rem;
    padding-right: 1.75rem;
    padding-bottom: 0.9375rem;
    padding-left: 1.75rem;
    font-size: 0.8125rem;
    /*     @@else                            { font-size: $padding - rem-calc(2); } */ }
  button.tiny, .button.tiny {
    padding-top: 0.625rem;
    padding-right: 1.25rem;
    padding-bottom: 0.6875rem;
    padding-left: 1.25rem;
    font-size: 0.6875rem;
    /*     @@else                            { font-size: $padding - rem-calc(2); } */ }
  button.expand, .button.expand {
    padding-right: 0;
    padding-left: 0;
    width: 100%; }
  button.left-align, .button.left-align {
    text-align: left;
    text-indent: 0.75rem; }
  button.right-align, .button.right-align {
    text-align: right;
    padding-right: 0.75rem; }
  button.radius, .button.radius {
    -webkit-border-radius: 3px;
    border-radius: 3px; }
  button.round, .button.round {
    -webkit-border-radius: 1000px;
    border-radius: 1000px; }
  button.disabled, button[disabled], .button.disabled, .button[disabled] {
    background-color: #2ba6cb;
    border-color: #2795b6;
    color: white;
    cursor: default;
    opacity: 0.7;
    -webkit-box-shadow: none;
    box-shadow: none; }
    button.disabled:hover, button.disabled:focus, button[disabled]:hover, button[disabled]:focus, .button.disabled:hover, .button.disabled:focus, .button[disabled]:hover, .button[disabled]:focus {
      background-color: #2795b6; }
    button.disabled:hover, button.disabled:focus, button[disabled]:hover, button[disabled]:focus, .button.disabled:hover, .button.disabled:focus, .button[disabled]:hover, .button[disabled]:focus {
      color: white; }
    button.disabled:hover, button.disabled:focus, button[disabled]:hover, button[disabled]:focus, .button.disabled:hover, .button.disabled:focus, .button[disabled]:hover, .button[disabled]:focus {
      background-color: #2ba6cb; }
    button.disabled.secondary, button[disabled].secondary, .button.disabled.secondary, .button[disabled].secondary {
      background-color: #e9e9e9;
      border-color: gainsboro;
      color: #333333;
      cursor: default;
      opacity: 0.7;
      -webkit-box-shadow: none;
      box-shadow: none; }
      button.disabled.secondary:hover, button.disabled.secondary:focus, button[disabled].secondary:hover, button[disabled].secondary:focus, .button.disabled.secondary:hover, .button.disabled.secondary:focus, .button[disabled].secondary:hover, .button[disabled].secondary:focus {
        background-color: gainsboro; }
      button.disabled.secondary:hover, button.disabled.secondary:focus, button[disabled].secondary:hover, button[disabled].secondary:focus, .button.disabled.secondary:hover, .button.disabled.secondary:focus, .button[disabled].secondary:hover, .button[disabled].secondary:focus {
        color: #333333; }
      button.disabled.secondary:hover, button.disabled.secondary:focus, button[disabled].secondary:hover, button[disabled].secondary:focus, .button.disabled.secondary:hover, .button.disabled.secondary:focus, .button[disabled].secondary:hover, .button[disabled].secondary:focus {
        background-color: #e9e9e9; }
    button.disabled.success, button[disabled].success, .button.disabled.success, .button[disabled].success {
      background-color: #5da423;
      border-color: #518f1f;
      color: white;
      cursor: default;
      opacity: 0.7;
      -webkit-box-shadow: none;
      box-shadow: none; }
      button.disabled.success:hover, button.disabled.success:focus, button[disabled].success:hover, button[disabled].success:focus, .button.disabled.success:hover, .button.disabled.success:focus, .button[disabled].success:hover, .button[disabled].success:focus {
        background-color: #518f1f; }
      button.disabled.success:hover, button.disabled.success:focus, button[disabled].success:hover, button[disabled].success:focus, .button.disabled.success:hover, .button.disabled.success:focus, .button[disabled].success:hover, .button[disabled].success:focus {
        color: white; }
      button.disabled.success:hover, button.disabled.success:focus, button[disabled].success:hover, button[disabled].success:focus, .button.disabled.success:hover, .button.disabled.success:focus, .button[disabled].success:hover, .button[disabled].success:focus {
        background-color: #5da423; }
    button.disabled.alert, button[disabled].alert, .button.disabled.alert, .button[disabled].alert {
      background-color: #c60f13;
      border-color: #ae0d11;
      color: white;
      cursor: default;
      opacity: 0.7;
      -webkit-box-shadow: none;
      box-shadow: none; }
      button.disabled.alert:hover, button.disabled.alert:focus, button[disabled].alert:hover, button[disabled].alert:focus, .button.disabled.alert:hover, .button.disabled.alert:focus, .button[disabled].alert:hover, .button[disabled].alert:focus {
        background-color: #ae0d11; }
      button.disabled.alert:hover, button.disabled.alert:focus, button[disabled].alert:hover, button[disabled].alert:focus, .button.disabled.alert:hover, .button.disabled.alert:focus, .button[disabled].alert:hover, .button[disabled].alert:focus {
        color: white; }
      button.disabled.alert:hover, button.disabled.alert:focus, button[disabled].alert:hover, button[disabled].alert:focus, .button.disabled.alert:hover, .button.disabled.alert:focus, .button[disabled].alert:hover, .button[disabled].alert:focus {
        background-color: #c60f13; }

@@media only screen and (min-width: 40.063em) {
  button, .button {
    display: inline-block; } }
.button-group {
  list-style: none;
  margin: 0;
  *zoom: 1; }
  .button-group:before, .button-group:after {
    content: " ";
    display: table; }
  .button-group:after {
    clear: both; }
  .button-group > * {
    margin: 0;
    float: left; }
    .button-group > * > button, .button-group > * .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group > *:first-child {
      margin-left: 0; }
  .button-group.radius > * > button, .button-group.radius > * .button {
    border-right: 1px solid;
    border-color: rgba(255, 255, 255, 0.5); }
  .button-group.radius > *:first-child, .button-group.radius > *:first-child > a, .button-group.radius > *:first-child > button, .button-group.radius > *:first-child > .button {
    -moz-border-radius-bottomleft: 3px;
    -moz-border-radius-topleft: 3px;
    -webkit-border-bottom-left-radius: 3px;
    -webkit-border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px; }
  .button-group.radius > *:last-child, .button-group.radius > *:last-child > a, .button-group.radius > *:last-child > button, .button-group.radius > *:last-child > .button {
    -moz-border-radius-topright: 3px;
    -moz-border-radius-bottomright: 3px;
    -webkit-border-top-right-radius: 3px;
    -webkit-border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px; }
  .button-group.round > * > button, .button-group.round > * .button {
    border-right: 1px solid;
    border-color: rgba(255, 255, 255, 0.5); }
  .button-group.round > *:first-child, .button-group.round > *:first-child > a, .button-group.round > *:first-child > button, .button-group.round > *:first-child > .button {
    -moz-border-radius-bottomleft: 1000px;
    -moz-border-radius-topleft: 1000px;
    -webkit-border-bottom-left-radius: 1000px;
    -webkit-border-top-left-radius: 1000px;
    border-bottom-left-radius: 1000px;
    border-top-left-radius: 1000px; }
  .button-group.round > *:last-child, .button-group.round > *:last-child > a, .button-group.round > *:last-child > button, .button-group.round > *:last-child > .button {
    -moz-border-radius-topright: 1000px;
    -moz-border-radius-bottomright: 1000px;
    -webkit-border-top-right-radius: 1000px;
    -webkit-border-bottom-right-radius: 1000px;
    border-top-right-radius: 1000px;
    border-bottom-right-radius: 1000px; }
  .button-group.even-2 li {
    width: 50%; }
    .button-group.even-2 li > button, .button-group.even-2 li .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group.even-2 li button, .button-group.even-2 li .button {
      width: 100%; }
  .button-group.even-3 li {
    width: 33.33333%; }
    .button-group.even-3 li > button, .button-group.even-3 li .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group.even-3 li button, .button-group.even-3 li .button {
      width: 100%; }
  .button-group.even-4 li {
    width: 25%; }
    .button-group.even-4 li > button, .button-group.even-4 li .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group.even-4 li button, .button-group.even-4 li .button {
      width: 100%; }
  .button-group.even-5 li {
    width: 20%; }
    .button-group.even-5 li > button, .button-group.even-5 li .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group.even-5 li button, .button-group.even-5 li .button {
      width: 100%; }
  .button-group.even-6 li {
    width: 16.66667%; }
    .button-group.even-6 li > button, .button-group.even-6 li .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group.even-6 li button, .button-group.even-6 li .button {
      width: 100%; }
  .button-group.even-7 li {
    width: 14.28571%; }
    .button-group.even-7 li > button, .button-group.even-7 li .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group.even-7 li button, .button-group.even-7 li .button {
      width: 100%; }
  .button-group.even-8 li {
    width: 12.5%; }
    .button-group.even-8 li > button, .button-group.even-8 li .button {
      border-right: 1px solid;
      border-color: rgba(255, 255, 255, 0.5); }
    .button-group.even-8 li button, .button-group.even-8 li .button {
      width: 100%; }

.button-bar {
  *zoom: 1; }
  .button-bar:before, .button-bar:after {
    content: " ";
    display: table; }
  .button-bar:after {
    clear: both; }
  .button-bar .button-group {
    float: left;
    margin-right: 0.625rem; }
    .button-bar .button-group div {
      overflow: hidden; }

/* Panels */
.panel {
  border-style: solid;
  border-width: 1px;
  border-color: #d9d9d9;
  margin-bottom: 1.25rem;
  padding: 1.25rem;
  background: #f2f2f2; }
  .panel > :first-child {
    margin-top: 0; }
  .panel > :last-child {
    margin-bottom: 0; }
  .panel h1, .panel h2, .panel h3, .panel h4, .panel h5, .panel h6, .panel p {
    color: #333333; }
  .panel h1, .panel h2, .panel h3, .panel h4, .panel h5, .panel h6 {
    line-height: 1;
    margin-bottom: 0.625rem; }
    .panel h1.subheader, .panel h2.subheader, .panel h3.subheader, .panel h4.subheader, .panel h5.subheader, .panel h6.subheader {
      line-height: 1.4; }
  .panel.callout {
    border-style: solid;
    border-width: 1px;
    border-color: #d5eef6;
    margin-bottom: 1.25rem;
    padding: 1.25rem;
    background: white; }
    .panel.callout > :first-child {
      margin-top: 0; }
    .panel.callout > :last-child {
      margin-bottom: 0; }
    .panel.callout h1, .panel.callout h2, .panel.callout h3, .panel.callout h4, .panel.callout h5, .panel.callout h6, .panel.callout p {
      color: #333333; }
    .panel.callout h1, .panel.callout h2, .panel.callout h3, .panel.callout h4, .panel.callout h5, .panel.callout h6 {
      line-height: 1;
      margin-bottom: 0.625rem; }
      .panel.callout h1.subheader, .panel.callout h2.subheader, .panel.callout h3.subheader, .panel.callout h4.subheader, .panel.callout h5.subheader, .panel.callout h6.subheader {
        line-height: 1.4; }
    .panel.callout a {
      color: #2ba6cb; }
  .panel.radius {
    -webkit-border-radius: 3px;
    border-radius: 3px; }

.dropdown.button {
  position: relative;
  padding-right: 3.5625rem; }
  .dropdown.button:before {
    position: absolute;
    content: "";
    width: 0;
    height: 0;
    display: block;
    border-style: solid;
    border-color: white transparent transparent transparent;
    top: 50%; }
  .dropdown.button:before {
    border-width: 0.375rem;
    right: 1.40625rem;
    margin-top: -0.15625rem; }
  .dropdown.button:before {
    border-color: white transparent transparent transparent; }
  .dropdown.button.tiny {
    padding-right: 2.625rem; }
    .dropdown.button.tiny:before {
      border-width: 0.375rem;
      right: 1.125rem;
      margin-top: -0.125rem; }
    .dropdown.button.tiny:before {
      border-color: white transparent transparent transparent; }
  .dropdown.button.small {
    padding-right: 3.0625rem; }
    .dropdown.button.small:before {
      border-width: 0.4375rem;
      right: 1.3125rem;
      margin-top: -0.15625rem; }
    .dropdown.button.small:before {
      border-color: white transparent transparent transparent; }
  .dropdown.button.large {
    padding-right: 3.625rem; }
    .dropdown.button.large:before {
      border-width: 0.3125rem;
      right: 1.71875rem;
      margin-top: -0.15625rem; }
    .dropdown.button.large:before {
      border-color: white transparent transparent transparent; }
  .dropdown.button.secondary:before {
    border-color: #333333 transparent transparent transparent; }

/* Image Thumbnails */
.th {
  line-height: 0;
  display: inline-block;
  border: solid 4px white;
  -webkit-box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.2);
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.2);
  -webkit-transition: all 200ms ease-out;
  -moz-transition: all 200ms ease-out;
  transition: all 200ms ease-out; }
  .th:hover, .th:focus {
    -webkit-box-shadow: 0 0 6px 1px rgba(43, 166, 203, 0.5);
    box-shadow: 0 0 6px 1px rgba(43, 166, 203, 0.5); }
  .th.radius {
    -webkit-border-radius: 3px;
    border-radius: 3px; }

a.th {
  display: inline-block;
  max-width: 100%; }

/* Pricing Tables */
.pricing-table {
  border: solid 1px #dddddd;
  margin-left: 0;
  margin-bottom: 1.25rem; }
  .pricing-table * {
    list-style: none;
    line-height: 1; }
  .pricing-table .title {
    background-color: #333333;
    padding: 0.9375rem 1.25rem;
    text-align: center;
    color: #eeeeee;
    font-weight: normal;
    font-size: 1rem;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }
  .pricing-table .price {
    background-color: #f6f6f6;
    padding: 0.9375rem 1.25rem;
    text-align: center;
    color: #333333;
    font-weight: normal;
    font-size: 2rem;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }
  .pricing-table .description {
    background-color: white;
    padding: 0.9375rem;
    text-align: center;
    color: #777777;
    font-size: 0.75rem;
    font-weight: normal;
    line-height: 1.4;
    border-bottom: dotted 1px #dddddd; }
  .pricing-table .bullet-item {
    background-color: white;
    padding: 0.9375rem;
    text-align: center;
    color: #333333;
    font-size: 0.875rem;
    font-weight: normal;
    border-bottom: dotted 1px #dddddd; }
  .pricing-table .cta-button {
    background-color: white;
    text-align: center;
    padding: 1.25rem 1.25rem 0; }

@@-webkit-keyframes rotate {
  from {
    -webkit-transform: rotate(0deg); }

  to {
    -webkit-transform: rotate(360deg); } }

@@-moz-keyframes rotate {
  from {
    -moz-transform: rotate(0deg); }

  to {
    -moz-transform: rotate(360deg); } }

@@-o-keyframes rotate {
  from {
    -o-transform: rotate(0deg); }

  to {
    -o-transform: rotate(360deg); } }

@@keyframes rotate {
  from {
    transform: rotate(0deg); }

  to {
    transform: rotate(360deg); } }

/* Orbit Graceful Loading */
.slideshow-wrapper {
  position: relative; }
  .slideshow-wrapper ul {
    list-style-type: none;
    margin: 0; }
    .slideshow-wrapper ul li,
    .slideshow-wrapper ul li .orbit-caption {
      display: none; }
    .slideshow-wrapper ul li:first-child {
      display: block; }
  .slideshow-wrapper .orbit-container {
    background-color: transparent; }
    .slideshow-wrapper .orbit-container li {
      display: block; }
      .slideshow-wrapper .orbit-container li .orbit-caption {
        display: block; }

.preloader {
  display: block;
  width: 40px;
  height: 40px;
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -20px;
  margin-left: -20px;
  border: solid 3px;
  border-color: #555555 white;
  -webkit-border-radius: 1000px;
  border-radius: 1000px;
  -webkit-animation-name: rotate;
  -webkit-animation-duration: 1.5s;
  -webkit-animation-iteration-count: infinite;
  -webkit-animation-timing-function: linear;
  -moz-animation-name: rotate;
  -moz-animation-duration: 1.5s;
  -moz-animation-iteration-count: infinite;
  -moz-animation-timing-function: linear;
  -o-animation-name: rotate;
  -o-animation-duration: 1.5s;
  -o-animation-iteration-count: infinite;
  -o-animation-timing-function: linear;
  animation-name: rotate;
  animation-duration: 1.5s;
  animation-iteration-count: infinite;
  animation-timing-function: linear; }

.orbit-container {
  overflow: hidden;
  width: 100%;
  position: relative;
  background: none; }
  .orbit-container .orbit-slides-container {
    list-style: none;
    margin: 0;
    padding: 0;
    position: relative; }
    .orbit-container .orbit-slides-container img {
      display: block;
      max-width: 100%; }
    .orbit-container .orbit-slides-container > * {
      position: absolute;
      top: 0;
      width: 100%;
      margin-left: 100%; }
      .orbit-container .orbit-slides-container > *:first-child {
        margin-left: 0%; }
      .orbit-container .orbit-slides-container > * .orbit-caption {
        position: absolute;
        bottom: 0;
        background-color: rgba(51, 51, 51, 0.8);
        color: white;
        width: 100%;
        padding: 10px 14px;
        font-size: 0.875rem; }
  .orbit-container .orbit-slide-number {
    position: absolute;
    top: 10px;
    left: 10px;
    font-size: 12px;
    color: white;
    background: rgba(0, 0, 0, 0);
    z-index: 10; }
    .orbit-container .orbit-slide-number span {
      font-weight: 700;
      padding: 0.3125rem; }
  .orbit-container .orbit-timer {
    position: absolute;
    top: 12px;
    right: 10px;
    height: 6px;
    width: 100px;
    z-index: 10; }
    .orbit-container .orbit-timer .orbit-progress {
      height: 3px;
      background-color: rgba(255, 255, 255, 0.3);
      display: block;
      width: 0%;
      position: relative;
      right: 20px;
      top: 5px; }
    .orbit-container .orbit-timer > span {
      display: none;
      position: absolute;
      top: 0px;
      right: 0;
      width: 11px;
      height: 14px;
      border: solid 4px white;
      border-top: none;
      border-bottom: none; }
    .orbit-container .orbit-timer.paused > span {
      right: -4px;
      top: 0px;
      width: 11px;
      height: 14px;
      border: inset 8px;
      border-right-style: solid;
      border-color: transparent transparent transparent white; }
      .orbit-container .orbit-timer.paused > span.dark {
        border-color: transparent transparent transparent #333333; }
  .orbit-container:hover .orbit-timer > span {
    display: block; }
  .orbit-container .orbit-prev,
  .orbit-container .orbit-next {
    position: absolute;
    top: 45%;
    margin-top: -25px;
    width: 36px;
    height: 60px;
    line-height: 50px;
    color: white;
    text-indent: -9999px !important;
    z-index: 10; }
    .orbit-container .orbit-prev:hover,
    .orbit-container .orbit-next:hover {
      background-color: rgba(0, 0, 0, 0.3); }
    .orbit-container .orbit-prev > span,
    .orbit-container .orbit-next > span {
      position: absolute;
      top: 50%;
      margin-top: -10px;
      display: block;
      width: 0;
      height: 0;
      border: inset 10px; }
  .orbit-container .orbit-prev {
    left: 0; }
    .orbit-container .orbit-prev > span {
      border-right-style: solid;
      border-color: transparent;
      border-right-color: white; }
    .orbit-container .orbit-prev:hover > span {
      border-right-color: white; }
  .orbit-container .orbit-next {
    right: 0; }
    .orbit-container .orbit-next > span {
      border-color: transparent;
      border-left-style: solid;
      border-left-color: white;
      left: 50%;
      margin-left: -4px; }
    .orbit-container .orbit-next:hover > span {
      border-left-color: white; }

.orbit-bullets-container {
  text-align: center; }

.orbit-bullets {
  margin: 0 auto 30px auto;
  overflow: hidden;
  position: relative;
  top: 10px;
  float: none;
  text-align: center;
  display: inline-block; }
  .orbit-bullets li {
    display: block;
    width: 0.5625rem;
    height: 0.5625rem;
    background: #cccccc;
    float: left;
    margin-right: 6px;
    -webkit-border-radius: 1000px;
    border-radius: 1000px; }
    .orbit-bullets li.active {
      background: #999999; }
    .orbit-bullets li:last-child {
      margin-right: 0; }

.touch .orbit-container .orbit-prev,
.touch .orbit-container .orbit-next {
  display: none; }
.touch .orbit-bullets {
  display: none; }

@@media only screen and (min-width: 40.063em) {
  .touch .orbit-container .orbit-prev,
  .touch .orbit-container .orbit-next {
    display: inherit; }
  .touch .orbit-bullets {
    display: block; } }
@@media only screen and (max-width: 40em) {
  .orbit-stack-on-small .orbit-slides-container {
    height: auto !important; }
  .orbit-stack-on-small .orbit-slides-container > * {
    position: relative;
    margin-left: 0% !important; }
  .orbit-stack-on-small .orbit-timer,
  .orbit-stack-on-small .orbit-next,
  .orbit-stack-on-small .orbit-prev,
  .orbit-stack-on-small .orbit-bullets {
    display: none; } }
[data-magellan-expedition] {
  background: white;
  z-index: 50;
  min-width: 100%;
  padding: 10px; }
  [data-magellan-expedition] .sub-nav {
    margin-bottom: 0; }
    [data-magellan-expedition] .sub-nav dd {
      margin-bottom: 0; }
    [data-magellan-expedition] .sub-nav .active {
      line-height: 1.8em; }

ul.pagination {
  display: block;
  height: 1.5rem;
  margin-left: -0.3125rem; }
  ul.pagination li {
    height: 1.5rem;
    color: #222222;
    font-size: 0.875rem;
    margin-left: 0.3125rem; }
    ul.pagination li a {
      display: block;
      padding: 0.0625rem 0.625rem 0.0625rem;
      color: #999999;
      -webkit-border-radius: 3px;
      border-radius: 3px; }
    ul.pagination li:hover a,
    ul.pagination li a:focus {
      background: #e6e6e6; }
    ul.pagination li.unavailable a {
      cursor: default;
      color: #999999; }
    ul.pagination li.unavailable:hover a, ul.pagination li.unavailable a:focus {
      background: transparent; }
    ul.pagination li.current a {
      background: #2ba6cb;
      color: white;
      font-weight: bold;
      cursor: default; }
      ul.pagination li.current a:hover, ul.pagination li.current a:focus {
        background: #2ba6cb; }
  ul.pagination li {
    float: left;
    display: block; }

/* Pagination centred wrapper */
.pagination-centered {
  text-align: center; }
  .pagination-centered ul.pagination li {
    float: none;
    display: inline-block; }

.side-nav {
  display: block;
  margin: 0;
  padding: 0.875rem 0;
  list-style-type: none;
  list-style-position: inside;
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }
  .side-nav li {
    margin: 0 0 0.4375rem 0;
    font-size: 0.875rem; }
    .side-nav li a {
      display: block;
      color: #2ba6cb; }
    .side-nav li.active > a:first-child {
      color: #4d4d4d;
      font-weight: normal;
      font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }
    .side-nav li.divider {
      border-top: 1px solid;
      height: 0;
      padding: 0;
      list-style: none;
      border-top-color: #e6e6e6; }

p.lead {
  font-size: 1.21875rem;
  line-height: 1.6; }

.subheader {
  line-height: 1.4;
  color: #6f6f6f;
  font-weight: 300;
  margin-top: 0.2rem;
  margin-bottom: 0.5rem; }

/* Typography resets */
div,
dl,
dt,
dd,
ul,
ol,
li,
h1,
h2,
h3,
h4,
h5,
h6,
pre,
form,
p,
blockquote,
th,
td {
  margin: 0;
  padding: 0;
  direction: ltr; }

/* Default Link Styles */
a {
  color: #2ba6cb;
  text-decoration: none;
  line-height: inherit; }
  a:hover, a:focus {
    color: #2795b6; }
  a img {
    border: none; }

/* Default paragraph styles */
p {
  font-family: inherit;
  font-weight: normal;
  font-size: 1rem;
  line-height: 1.6;
  margin-bottom: 1.25rem;
  text-rendering: optimizeLegibility; }
  p aside {
    font-size: 0.875rem;
    line-height: 1.35;
    font-style: italic; }

/* Default header styles */
h1, h2, h3, h4, h5, h6 {
  font-family: "Open Sans", "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  font-weight: 300;
  font-style: normal;
  color: #222222;
  text-rendering: optimizeLegibility;
  margin-top: 0.2rem;
  margin-bottom: 0.5rem;
  line-height: 1.4; }
  h1 small, h2 small, h3 small, h4 small, h5 small, h6 small {
    font-size: 60%;
    color: #6f6f6f;
    line-height: 0; }

h1 {
  font-size: 2.125rem; }

h2 {
  font-size: 1.6875rem; }

h3 {
  font-size: 1.375rem; }

h4 {
  font-size: 1.125rem; }

h5 {
  font-size: 1.125rem; }

h6 {
  font-size: 1rem; }

hr {
  border: solid #dddddd;
  border-width: 1px 0 0;
  clear: both;
  margin: 1.25rem 0 1.1875rem;
  height: 0; }

/* Helpful Typography Defaults */
em,
i {
  font-style: italic;
  line-height: inherit; }

strong,
b {
  font-weight: bold;
  line-height: inherit; }

small {
  font-size: 60%;
  line-height: inherit; }

code {
  font-family: Consolas, "Liberation Mono", Courier, monospace;
  font-weight: bold;
  color: #7f0a0c; }

/* Lists */
ul,
ol,
dl {
  font-size: 1rem;
  line-height: 1.6;
  margin-bottom: 1.25rem;
  list-style-position: outside;
  font-family: inherit; }

ul {
  margin-left: 1.1rem; }
  ul.no-bullet {
    margin-left: 0; }
    ul.no-bullet li ul,
    ul.no-bullet li ol {
      margin-left: 1.25rem;
      margin-bottom: 0;
      list-style: none; }

/* Unordered Lists */
ul li ul,
ul li ol {
  margin-left: 1.25rem;
  margin-bottom: 0;
  font-size: 1rem;
  /* Override nested font-size change */ }
ul.square li ul, ul.circle li ul, ul.disc li ul {
  list-style: inherit; }
ul.square {
  list-style-type: square;
  margin-left: 1.1rem; }
ul.circle {
  list-style-type: circle;
  margin-left: 1.1rem; }
ul.disc {
  list-style-type: disc;
  margin-left: 1.1rem; }
ul.no-bullet {
  list-style: none; }

/* Ordered Lists */
ol {
  margin-left: 1.4rem; }
  ol li ul,
  ol li ol {
    margin-left: 1.25rem;
    margin-bottom: 0; }

/* Definition Lists */
dl dt {
  margin-bottom: 0.3rem;
  font-weight: bold; }
dl dd {
  margin-bottom: 0.75rem; }

/* Abbreviations */
abbr,
acronym {
  text-transform: uppercase;
  font-size: 90%;
  color: #222222;
  border-bottom: 1px dotted #dddddd;
  cursor: help; }

abbr {
  text-transform: none; }

/* Blockquotes */
blockquote {
  margin: 0 0 1.25rem;
  padding: 0.5625rem 1.25rem 0 1.1875rem;
  border-left: 1px solid #dddddd; }
  blockquote cite {
    display: block;
    font-size: 0.8125rem;
    color: #555555; }
    blockquote cite:before {
      content: "\2014 \0020"; }
    blockquote cite a,
    blockquote cite a:visited {
      color: #555555; }

blockquote,
blockquote p {
  line-height: 1.6;
  color: #6f6f6f; }

/* Microformats */
.vcard {
  display: inline-block;
  margin: 0 0 1.25rem 0;
  border: 1px solid #dddddd;
  padding: 0.625rem 0.75rem; }
  .vcard li {
    margin: 0;
    display: block; }
  .vcard .fn {
    font-weight: bold;
    font-size: 0.9375rem; }

.vevent .summary {
  font-weight: bold; }
.vevent abbr {
  cursor: default;
  text-decoration: none;
  font-weight: bold;
  border: none;
  padding: 0 0.0625rem; }

@@media only screen and (min-width: 40.063em) {
  h1, h2, h3, h4, h5, h6 {
    line-height: 1.4; }

  h1 {
    font-size: 2.75rem; }

  h2 {
    font-size: 2.3125rem; }

  h3 {
    font-size: 1.6875rem; }

  h4 {
    font-size: 1.4375rem; } }
/*
 * Print styles.
 *
 * Inlined to avoid required HTTP connection: www.phpied.com/delay-loading-your-print-css/
 * Credit to Paul Irish and HTML5 Boilerplate (html5boilerplate.com)
*/
.print-only {
  display: none !important; }

@@media print {
  * {
    background: transparent !important;
    color: black !important;
    /* Black prints faster: h5bp.com/s */
    box-shadow: none !important;
    text-shadow: none !important; }

  a,
  a:visited {
    text-decoration: underline; }

  a[href]:after {
    content: " (" attr(href) ")"; }

  abbr[title]:after {
    content: " (" attr(title) ")"; }

  .ir a:after,
  a[href^="javascript:"]:after,
  a[href^="#"]:after {
    content: ""; }

  pre,
  blockquote {
    border: 1px solid #999999;
    page-break-inside: avoid; }

  thead {
    display: table-header-group;
    /* h5bp.com/t */ }

  tr,
  img {
    page-break-inside: avoid; }

  img {
    max-width: 100% !important; }

  @@page {
    margin: 0.5cm; }

  p,
  h2,
  h3 {
    orphans: 3;
    widows: 3; }

  h2,
  h3 {
    page-break-after: avoid; }

  .hide-on-print {
    display: none !important; }

  .print-only {
    display: block !important; }

  .hide-for-print {
    display: none !important; }

  .show-for-print {
    display: inherit !important; } }
.split.button {
  position: relative;
  padding-right: 5.0625rem; }
  .split.button span {
    display: block;
    height: 100%;
    position: absolute;
    right: 0;
    top: 0;
    border-left: solid 1px; }
    .split.button span:before {
      position: absolute;
      content: "";
      width: 0;
      height: 0;
      display: block;
      border-style: inset;
      top: 50%;
      left: 50%; }
    .split.button span:active {
      background-color: rgba(0, 0, 0, 0.1); }
  .split.button span {
    border-left-color: rgba(255, 255, 255, 0.5); }
  .split.button span {
    width: 3.09375rem; }
    .split.button span:before {
      border-top-style: solid;
      border-width: 0.375rem;
      top: 48%;
      margin-left: -0.375rem; }
  .split.button span:before {
    border-color: white transparent transparent transparent; }
  .split.button.secondary span {
    border-left-color: rgba(255, 255, 255, 0.5); }
  .split.button.secondary span:before {
    border-color: white transparent transparent transparent; }
  .split.button.alert span {
    border-left-color: rgba(255, 255, 255, 0.5); }
  .split.button.success span {
    border-left-color: rgba(255, 255, 255, 0.5); }
  .split.button.tiny {
    padding-right: 3.75rem; }
    .split.button.tiny span {
      width: 2.25rem; }
      .split.button.tiny span:before {
        border-top-style: solid;
        border-width: 0.375rem;
        top: 48%;
        margin-left: -0.375rem; }
  .split.button.small {
    padding-right: 4.375rem; }
    .split.button.small span {
      width: 2.625rem; }
      .split.button.small span:before {
        border-top-style: solid;
        border-width: 0.4375rem;
        top: 48%;
        margin-left: -0.375rem; }
  .split.button.large {
    padding-right: 5.5rem; }
    .split.button.large span {
      width: 3.4375rem; }
      .split.button.large span:before {
        border-top-style: solid;
        border-width: 0.3125rem;
        top: 48%;
        margin-left: -0.375rem; }
  .split.button.expand {
    padding-left: 2rem; }
  .split.button.secondary span:before {
    border-color: #333333 transparent transparent transparent; }
  .split.button.radius span {
    -moz-border-radius-topright: 3px;
    -moz-border-radius-bottomright: 3px;
    -webkit-border-top-right-radius: 3px;
    -webkit-border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px; }
  .split.button.round span {
    -moz-border-radius-topright: 1000px;
    -moz-border-radius-bottomright: 1000px;
    -webkit-border-top-right-radius: 1000px;
    -webkit-border-bottom-right-radius: 1000px;
    border-top-right-radius: 1000px;
    border-bottom-right-radius: 1000px; }

.reveal-modal-bg {
  position: fixed;
  height: 100%;
  width: 100%;
  background: black;
  background: rgba(0, 0, 0, 0.45);
  z-index: 98;
  display: none;
  top: 0;
  left: 0; }

.reveal-modal {
  visibility: hidden;
  display: none;
  position: absolute;
  left: 50%;
  z-index: 99;
  height: auto;
  margin-left: -40%;
  width: 80%;
  background-color: white;
  padding: 1.25rem;
  border: solid 1px #666666;
  -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.4);
  top: 50px; }
  .reveal-modal .column,
  .reveal-modal .columns {
    min-width: 0; }
  .reveal-modal > :first-child {
    margin-top: 0; }
  .reveal-modal > :last-child {
    margin-bottom: 0; }
  .reveal-modal .close-reveal-modal {
    font-size: 1.375rem;
    line-height: 1;
    position: absolute;
    top: 0.5rem;
    right: 0.6875rem;
    color: #aaaaaa;
    font-weight: bold;
    cursor: pointer; }

@@media only screen and (min-width: 40.063em) {
  .reveal-modal {
    padding: 1.875rem;
    top: 6.25rem; }
    .reveal-modal.tiny {
      margin-left: -15%;
      width: 30%; }
    .reveal-modal.small {
      margin-left: -20%;
      width: 40%; }
    .reveal-modal.medium {
      margin-left: -30%;
      width: 60%; }
    .reveal-modal.large {
      margin-left: -35%;
      width: 70%; }
    .reveal-modal.xlarge {
      margin-left: -47.5%;
      width: 95%; } }
@@media print {
  .reveal-modal {
    background: white !important; } }
/* Tooltips */
.has-tip {
  border-bottom: dotted 1px #cccccc;
  cursor: help;
  font-weight: bold;
  color: #333333; }
  .has-tip:hover, .has-tip:focus {
    border-bottom: dotted 1px #196177;
    color: #2ba6cb; }
  .has-tip.tip-left, .has-tip.tip-right {
    float: none !important; }

.tooltip {
  display: none;
  position: absolute;
  z-index: 999;
  font-weight: normal;
  font-size: 0.875rem;
  line-height: 1.3;
  padding: 0.75rem;
  max-width: 85%;
  left: 50%;
  width: 100%;
  color: white;
  background: #333333;
  -webkit-border-radius: 3px;
  border-radius: 3px; }
  .tooltip > .nub {
    display: block;
    left: 5px;
    position: absolute;
    width: 0;
    height: 0;
    border: solid 5px;
    border-color: transparent transparent #333333 transparent;
    top: -10px; }
  .tooltip.opened {
    color: #2ba6cb !important;
    border-bottom: dotted 1px #196177 !important; }

.tap-to-close {
  display: block;
  font-size: 0.625rem;
  color: #777777;
  font-weight: normal; }

@@media only screen and (min-width: 40.063em) {
  .tooltip > .nub {
    border-color: transparent transparent #333333 transparent;
    top: -10px; }
  .tooltip.tip-top > .nub {
    border-color: #333333 transparent transparent transparent;
    top: auto;
    bottom: -10px; }
  .tooltip.tip-left, .tooltip.tip-right {
    float: none !important; }
  .tooltip.tip-left > .nub {
    border-color: transparent transparent transparent #333333;
    right: -10px;
    left: auto;
    top: 50%;
    margin-top: -5px; }
  .tooltip.tip-right > .nub {
    border-color: transparent #333333 transparent transparent;
    right: auto;
    left: -10px;
    top: 50%;
    margin-top: -5px; } }
/* Clearing Styles */
[data-clearing] {
  *zoom: 1;
  margin-bottom: 0;
  margin-left: 0;
  list-style: none; }
  [data-clearing]:before, [data-clearing]:after {
    content: " ";
    display: table; }
  [data-clearing]:after {
    clear: both; }
  [data-clearing] li {
    float: left;
    margin-right: 10px; }

.clearing-blackout {
  background: #333333;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 998; }
  .clearing-blackout .clearing-close {
    display: block; }

.clearing-container {
  position: relative;
  z-index: 998;
  height: 100%;
  overflow: hidden;
  margin: 0; }

.visible-img {
  height: 95%;
  position: relative; }
  .visible-img img {
    position: absolute;
    left: 50%;
    top: 50%;
    margin-left: -50%;
    max-height: 100%;
    max-width: 100%; }

.clearing-caption {
  color: #cccccc;
  font-size: 0.875em;
  line-height: 1.3;
  margin-bottom: 0;
  text-align: center;
  bottom: 0;
  background: #333333;
  width: 100%;
  padding: 10px 30px 20px;
  position: absolute;
  left: 0; }

.clearing-close {
  z-index: 999;
  padding-left: 20px;
  padding-top: 10px;
  font-size: 30px;
  line-height: 1;
  color: #cccccc;
  display: none; }
  .clearing-close:hover, .clearing-close:focus {
    color: #ccc; }

.clearing-assembled .clearing-container {
  height: 100%; }
  .clearing-assembled .clearing-container .carousel > ul {
    display: none; }

.clearing-feature li {
  display: none; }
  .clearing-feature li.clearing-featured-img {
    display: block; }

@@media only screen and (min-width: 40.063em) {
  .clearing-main-prev,
  .clearing-main-next {
    position: absolute;
    height: 100%;
    width: 40px;
    top: 0; }
    .clearing-main-prev > span,
    .clearing-main-next > span {
      position: absolute;
      top: 50%;
      display: block;
      width: 0;
      height: 0;
      border: solid 12px; }
      .clearing-main-prev > span:hover,
      .clearing-main-next > span:hover {
        opacity: 0.8; }

  .clearing-main-prev {
    left: 0; }
    .clearing-main-prev > span {
      left: 5px;
      border-color: transparent;
      border-right-color: #cccccc; }

  .clearing-main-next {
    right: 0; }
    .clearing-main-next > span {
      border-color: transparent;
      border-left-color: #cccccc; }

  .clearing-main-prev.disabled,
  .clearing-main-next.disabled {
    opacity: 0.3; }

  .clearing-assembled .clearing-container .carousel {
    background: rgba(51, 51, 51, 0.8);
    height: 120px;
    margin-top: 10px;
    text-align: center; }
    .clearing-assembled .clearing-container .carousel > ul {
      display: inline-block;
      z-index: 999;
      height: 100%;
      position: relative;
      float: none; }
      .clearing-assembled .clearing-container .carousel > ul li {
        display: block;
        width: 120px;
        min-height: inherit;
        float: left;
        overflow: hidden;
        margin-right: 0;
        padding: 0;
        position: relative;
        cursor: pointer;
        opacity: 0.4; }
        .clearing-assembled .clearing-container .carousel > ul li.fix-height img {
          height: 100%;
          max-width: none; }
        .clearing-assembled .clearing-container .carousel > ul li a.th {
          border: none;
          -webkit-box-shadow: none;
          box-shadow: none;
          display: block; }
        .clearing-assembled .clearing-container .carousel > ul li img {
          cursor: pointer !important;
          width: 100% !important; }
        .clearing-assembled .clearing-container .carousel > ul li.visible {
          opacity: 1; }
        .clearing-assembled .clearing-container .carousel > ul li:hover {
          opacity: 0.8; }
  .clearing-assembled .clearing-container .visible-img {
    background: #333333;
    overflow: hidden;
    height: 85%; }

  .clearing-close {
    position: absolute;
    top: 10px;
    right: 20px;
    padding-left: 0;
    padding-top: 0; } }
/* Progress Bar */
.progress {
  background-color: #f6f6f6;
  height: 1.5625rem;
  border: 1px solid #cccccc;
  padding: 0.125rem;
  margin-bottom: 0.625rem; }
  .progress .meter {
    background: #2ba6cb;
    height: 100%;
    display: block; }
  .progress.secondary .meter {
    background: #e9e9e9;
    height: 100%;
    display: block; }
  .progress.success .meter {
    background: #5da423;
    height: 100%;
    display: block; }
  .progress.alert .meter {
    background: #c60f13;
    height: 100%;
    display: block; }
  .progress.radius {
    -webkit-border-radius: 3px;
    border-radius: 3px; }
    .progress.radius .meter {
      -webkit-border-radius: 2px;
      border-radius: 2px; }
  .progress.round {
    -webkit-border-radius: 1000px;
    border-radius: 1000px; }
    .progress.round .meter {
      -webkit-border-radius: 999px;
      border-radius: 999px; }

.sub-nav {
  display: block;
  width: auto;
  overflow: hidden;
  margin: -0.25rem 0 1.125rem;
  padding-top: 0.25rem;
  margin-right: 0;
  margin-left: -0.75rem; }
  .sub-nav dt {
    text-transform: uppercase; }
  .sub-nav dt,
  .sub-nav dd,
  .sub-nav li {
    float: left;
    display: inline;
    margin-left: 1rem;
    margin-bottom: 0.625rem;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    font-weight: normal;
    font-size: 0.875rem;
    color: #999999; }
    .sub-nav dt a,
    .sub-nav dd a,
    .sub-nav li a {
      text-decoration: none;
      color: #999999; }
      .sub-nav dt a:hover,
      .sub-nav dd a:hover,
      .sub-nav li a:hover {
        color: #2795b6; }
    .sub-nav dt.active a,
    .sub-nav dd.active a,
    .sub-nav li.active a {
      -webkit-border-radius: 3px;
      border-radius: 3px;
      font-weight: normal;
      background: #2ba6cb;
      padding: 0.1875rem 1rem;
      cursor: default;
      color: white; }
      .sub-nav dt.active a:hover,
      .sub-nav dd.active a:hover,
      .sub-nav li.active a:hover {
        background: #2795b6; }

/* Foundation Joyride */
.joyride-list {
  display: none; }

/* Default styles for the container */
.joyride-tip-guide {
  display: none;
  position: absolute;
  background: #333333;
  color: white;
  z-index: 101;
  top: 0;
  left: 2.5%;
  font-family: inherit;
  font-weight: normal;
  width: 95%; }

.lt-ie9 .joyride-tip-guide {
  max-width: 800px;
  left: 50%;
  margin-left: -400px; }

.joyride-content-wrapper {
  width: 100%;
  padding: 1.125rem 1.25rem 1.5rem; }
  .joyride-content-wrapper .button {
    margin-bottom: 0 !important; }

/* Add a little css triangle pip, older browser just miss out on the fanciness of it */
.joyride-tip-guide .joyride-nub {
  display: block;
  position: absolute;
  left: 22px;
  width: 0;
  height: 0;
  border: 10px solid #333333; }
  .joyride-tip-guide .joyride-nub.top {
    border-top-style: solid;
    border-color: #333333;
    border-top-color: transparent !important;
    border-left-color: transparent !important;
    border-right-color: transparent !important;
    top: -20px; }
  .joyride-tip-guide .joyride-nub.bottom {
    border-bottom-style: solid;
    border-color: #333333 !important;
    border-bottom-color: transparent !important;
    border-left-color: transparent !important;
    border-right-color: transparent !important;
    bottom: -20px; }
  .joyride-tip-guide .joyride-nub.right {
    right: -20px; }
  .joyride-tip-guide .joyride-nub.left {
    left: -20px; }

/* Typography */
.joyride-tip-guide h1,
.joyride-tip-guide h2,
.joyride-tip-guide h3,
.joyride-tip-guide h4,
.joyride-tip-guide h5,
.joyride-tip-guide h6 {
  line-height: 1.25;
  margin: 0;
  font-weight: bold;
  color: white; }

.joyride-tip-guide p {
  margin: 0 0 1.125rem 0;
  font-size: 0.875rem;
  line-height: 1.3; }

.joyride-timer-indicator-wrap {
  width: 50px;
  height: 3px;
  border: solid 1px #555555;
  position: absolute;
  right: 1.0625rem;
  bottom: 1rem; }

.joyride-timer-indicator {
  display: block;
  width: 0;
  height: inherit;
  background: #666666; }

.joyride-close-tip {
  position: absolute;
  right: 12px;
  top: 10px;
  color: #777777 !important;
  text-decoration: none;
  font-size: 24px;
  font-weight: normal;
  line-height: 0.5 !important; }
  .joyride-close-tip:hover, .joyride-close-tip:focus {
    color: #eeeeee !important; }

.joyride-modal-bg {
  position: fixed;
  height: 100%;
  width: 100%;
  background: transparent;
  background: rgba(0, 0, 0, 0.5);
  z-index: 100;
  display: none;
  top: 0;
  left: 0;
  cursor: pointer; }

.joyride-expose-wrapper {
  background-color: #ffffff;
  position: absolute;
  border-radius: 3px;
  z-index: 102;
  -moz-box-shadow: 0 0 30px white;
  -webkit-box-shadow: 0 0 15px white;
  box-shadow: 0 0 15px white; }

.joyride-expose-cover {
  background: transparent;
  border-radius: 3px;
  position: absolute;
  z-index: 9999;
  top: 0;
  left: 0; }

/* Styles for screens that are atleast 768px; */
@@media only screen and (min-width: 40.063em) {
  .joyride-tip-guide {
    width: 300px;
    left: inherit; }
    .joyride-tip-guide .joyride-nub.bottom {
      border-color: #333333 !important;
      border-bottom-color: transparent !important;
      border-left-color: transparent !important;
      border-right-color: transparent !important;
      bottom: -20px; }
    .joyride-tip-guide .joyride-nub.right {
      border-color: #333333 !important;
      border-top-color: transparent !important;
      border-right-color: transparent !important;
      border-bottom-color: transparent !important;
      top: 22px;
      left: auto;
      right: -20px; }
    .joyride-tip-guide .joyride-nub.left {
      border-color: #333333 !important;
      border-top-color: transparent !important;
      border-left-color: transparent !important;
      border-bottom-color: transparent !important;
      top: 22px;
      left: -20px;
      right: auto; } }
.label {
  font-weight: normal;
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  text-align: center;
  text-decoration: none;
  line-height: 1;
  white-space: nowrap;
  display: inline-block;
  position: relative;
  margin-bottom: inherit;
  padding: 0.25rem 0.5rem 0.375rem;
  font-size: 0.6875rem;
  background-color: #2ba6cb;
  color: white; }
  .label.radius {
    -webkit-border-radius: 3px;
    border-radius: 3px; }
  .label.round {
    -webkit-border-radius: 1000px;
    border-radius: 1000px; }
  .label.alert {
    background-color: #c60f13;
    color: white; }
  .label.success {
    background-color: #5da423;
    color: white; }
  .label.secondary {
    background-color: #e9e9e9;
    color: #333333; }

p.lead {
  font-size: 1.21875rem;
  line-height: 1.6; }

.subheader {
  line-height: 1.4;
  color: #6f6f6f;
  font-weight: 300;
  margin-top: 0.2rem;
  margin-bottom: 0.5rem; }

meta.foundation-mq-topbar {
  font-family: "/only screen and (min-width:40.063em)/";
  width: 58.75em; }

.off-canvas-wrap, .inner-wrap, nav.tab-bar, .left-off-canvas-menu, .left-off-canvas-menu *, .right-off-canvas-menu, .move-right a.exit-off-canvas, .move-left a.exit-off-canvas {
  -webkit-backface-visibility: hidden; }

.off-canvas-wrap, .inner-wrap {
  position: relative;
  width: 100%; }

.left-off-canvas-menu, .right-off-canvas-menu {
  width: 250px;
  top: 0;
  bottom: 0;
  height: 100%;
  position: absolute;
  overflow-y: auto;
  background: #333333;
  z-index: 1001;
  box-sizing: content-box; }

section.left-small, section.right-small {
  width: 2.8125rem;
  height: 2.8125rem;
  position: absolute;
  top: 0; }

.off-canvas-wrap {
  overflow: hidden; }

.inner-wrap {
  *zoom: 1;
  -webkit-transition: -webkit-transform 500ms ease;
  -moz-transition: -moz-transform 500ms ease;
  -ms-transition: -ms-transform 500ms ease;
  -o-transition: -o-transform 500ms ease;
  transition: transform 500ms ease; }
  .inner-wrap:before, .inner-wrap:after {
    content: " ";
    display: table; }
  .inner-wrap:after {
    clear: both; }

nav.tab-bar {
  background: #333333;
  color: white;
  height: 2.8125rem;
  line-height: 2.8125rem;
  position: relative; }
  nav.tab-bar h1, nav.tab-bar h2, nav.tab-bar h3, nav.tab-bar h4, nav.tab-bar h5, nav.tab-bar h6 {
    color: white;
    font-weight: bold;
    line-height: 2.8125rem;
    margin: 0; }
  nav.tab-bar h1, nav.tab-bar h2, nav.tab-bar h3, nav.tab-bar h4 {
    font-size: 1.125rem; }

section.left-small {
  border-right: solid 1px #1a1a1a;
  box-shadow: 1px 0 0 #4d4d4d;
  left: 0; }

section.right-small {
  border-left: solid 1px #4d4d4d;
  box-shadow: -1px 0 0 #1a1a1a;
  right: 0; }

section.tab-bar-section {
  padding: 0 0.625rem;
  position: absolute;
  text-align: center;
  height: 2.8125rem;
  top: 0; }
  @@media only screen and (min-width: 40.063em) {
    section.tab-bar-section {
      text-align: left; } }
  section.tab-bar-section.left {
    left: 0;
    right: 2.8125rem; }
  section.tab-bar-section.right {
    left: 2.8125rem;
    right: 0; }
  section.tab-bar-section.middle {
    left: 2.8125rem;
    right: 2.8125rem; }

a.menu-icon {
  text-indent: 2.1875rem;
  width: 2.8125rem;
  height: 2.8125rem;
  display: block;
  line-height: 2.0625rem;
  padding: 0;
  color: white;
  position: relative; }
  a.menu-icon span {
    position: absolute;
    display: block;
    width: 1rem;
    height: 0;
    left: 0.8125rem;
    top: 0.3125rem;
    -webkit-box-shadow: 0 10px 0 1px white, 0 16px 0 1px white, 0 22px 0 1px white;
    box-shadow: 0 10px 0 1px white, 0 16px 0 1px white, 0 22px 0 1px white; }
  a.menu-icon:hover span {
    -webkit-box-shadow: 0 10px 0 1px #b3b3b3, 0 16px 0 1px #b3b3b3, 0 22px 0 1px #b3b3b3;
    box-shadow: 0 10px 0 1px #b3b3b3, 0 16px 0 1px #b3b3b3, 0 22px 0 1px #b3b3b3; }

.left-off-canvas-menu {
  -webkit-transform: translate3d(-100%, 0, 0);
  -moz-transform: translate3d(-100%, 0, 0);
  -ms-transform: translate3d(-100%, 0, 0);
  -o-transform: translate3d(-100%, 0, 0);
  transform: translate3d(-100%, 0, 0); }

.right-off-canvas-menu {
  -webkit-transform: translate3d(100%, 0, 0);
  -moz-transform: translate3d(100%, 0, 0);
  -ms-transform: translate3d(100%, 0, 0);
  -o-transform: translate3d(100%, 0, 0);
  transform: translate3d(100%, 0, 0);
  right: 0; }

ul.off-canvas-list {
  list-style-type: none;
  padding: 0;
  margin: 0; }
  ul.off-canvas-list li label {
    padding: 0.3rem 0.9375rem;
    color: #999999;
    text-transform: uppercase;
    font-weight: bold;
    background: #444444;
    border-top: 1px solid #5e5e5e;
    border-bottom: none;
    margin: 0; }
  ul.off-canvas-list li a {
    display: block;
    padding: 0.66667rem;
    color: rgba(255, 255, 255, 0.7);
    border-bottom: 1px solid #262626; }

.move-right > .inner-wrap {
  -webkit-transform: translate3d(250px, 0, 0);
  -moz-transform: translate3d(250px, 0, 0);
  -ms-transform: translate3d(250px, 0, 0);
  -o-transform: translate3d(250px, 0, 0);
  transform: translate3d(250px, 0, 0); }
.move-right a.exit-off-canvas {
  transition: background 300ms ease;
  cursor: pointer;
  box-shadow: -4px 0 4px rgba(0, 0, 0, 0.5), 4px 0 4px rgba(0, 0, 0, 0.5);
  display: block;
  position: absolute;
  background: rgba(255, 255, 255, 0.2);
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1002; }
  @@media only screen and (min-width: 40.063em) {
    .move-right a.exit-off-canvas:hover {
      background: rgba(255, 255, 255, 0.05); } }

.move-left > .inner-wrap {
  -webkit-transform: translate3d(-250px, 0, 0);
  -moz-transform: translate3d(-250px, 0, 0);
  -ms-transform: translate3d(-250px, 0, 0);
  -o-transform: translate3d(-250px, 0, 0);
  transform: translate3d(-250px, 0, 0); }
.move-left a.exit-off-canvas {
  transition: background 300ms ease;
  cursor: pointer;
  box-shadow: -4px 0 4px rgba(0, 0, 0, 0.5), 4px 0 4px rgba(0, 0, 0, 0.5);
  display: block;
  position: absolute;
  background: rgba(255, 255, 255, 0.2);
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1002; }
  @@media only screen and (min-width: 40.063em) {
    .move-left a.exit-off-canvas:hover {
      background: rgba(255, 255, 255, 0.05); } }

.lt-ie10 .left-off-canvas-menu {
  left: -250px; }
.lt-ie10 .right-off-canvas-menu {
  right: -250px; }
.lt-ie10 .move-left > .inner-wrap {
  right: 250px; }
.lt-ie10 .move-right > .inner-wrap {
  left: 250px; }

@@media only screen and (max-width: 40em) {
  .f-dropdown {
    max-width: 100%;
    left: 0; } }
/* Foundation Dropdowns */
.f-dropdown {
  position: absolute;
  top: -9999px;
  list-style: none;
  margin-left: 0;
  width: 100%;
  max-height: none;
  height: auto;
  background: white;
  border: solid 1px #cccccc;
  font-size: 16px;
  z-index: 99;
  margin-top: 2px;
  max-width: 200px; }
  .f-dropdown > *:first-child {
    margin-top: 0; }
  .f-dropdown > *:last-child {
    margin-bottom: 0; }
  .f-dropdown:before {
    content: "";
    display: block;
    width: 0;
    height: 0;
    border: inset 6px;
    border-color: transparent transparent white transparent;
    border-bottom-style: solid;
    position: absolute;
    top: -12px;
    left: 10px;
    z-index: 99; }
  .f-dropdown:after {
    content: "";
    display: block;
    width: 0;
    height: 0;
    border: inset 7px;
    border-color: transparent transparent #cccccc transparent;
    border-bottom-style: solid;
    position: absolute;
    top: -14px;
    left: 9px;
    z-index: 98; }
  .f-dropdown.right:before {
    left: auto;
    right: 10px; }
  .f-dropdown.right:after {
    left: auto;
    right: 9px; }
  .f-dropdown li {
    font-size: 0.875rem;
    cursor: pointer;
    line-height: 1.125rem;
    margin: 0; }
    .f-dropdown li:hover, .f-dropdown li:focus {
      background: #eeeeee; }
    .f-dropdown li a {
      display: block;
      padding: 0.5rem;
      color: #555555; }
  .f-dropdown.content {
    position: absolute;
    top: -9999px;
    list-style: none;
    margin-left: 0;
    padding: 1.25rem;
    width: 100%;
    height: auto;
    max-height: none;
    background: white;
    border: solid 1px #cccccc;
    font-size: 16px;
    z-index: 99;
    max-width: 200px; }
    .f-dropdown.content > *:first-child {
      margin-top: 0; }
    .f-dropdown.content > *:last-child {
      margin-bottom: 0; }
  .f-dropdown.tiny {
    max-width: 200px; }
  .f-dropdown.small {
    max-width: 300px; }
  .f-dropdown.medium {
    max-width: 500px; }
  .f-dropdown.large {
    max-width: 800px; }

table {
  background: white;
  margin-bottom: 1.25rem;
  border: solid 1px #dddddd; }
  table thead,
  table tfoot {
    background: whitesmoke;
    font-weight: bold; }
    table thead tr th,
    table thead tr td,
    table tfoot tr th,
    table tfoot tr td {
      padding: 0.5rem 0.625rem 0.625rem;
      font-size: 0.875rem;
      color: #222222;
      text-align: left; }
  table tr th,
  table tr td {
    padding: 0.5625rem 0.625rem;
    font-size: 0.875rem;
    color: #222222; }
  table tr.even, table tr.alt, table tr:nth-of-type(even) {
    background: #f9f9f9; }
  table thead tr th,
  table tfoot tr th,
  table tbody tr td,
  table tr td,
  table tfoot tr td {
    display: table-cell;
    line-height: 1.125rem; }

/* Standard Forms */
form {
  margin: 0 0 1rem; }

/* Using forms within rows, we need to set some defaults */
form .row .row {
  margin: 0 -0.5rem; }
  form .row .row .column,
  form .row .row .columns {
    padding: 0 0.5rem; }
  form .row .row.collapse {
    margin: 0; }
    form .row .row.collapse .column,
    form .row .row.collapse .columns {
      padding: 0; }
    form .row .row.collapse input {
      -moz-border-radius-bottomright: 0;
      -moz-border-radius-topright: 0;
      -webkit-border-bottom-right-radius: 0;
      -webkit-border-top-right-radius: 0; }
form .row input.column,
form .row input.columns,
form .row textarea.column,
form .row textarea.columns {
  padding-left: 0.5rem; }

/* Label Styles */
label {
  font-size: 0.875rem;
  color: #4d4d4d;
  cursor: pointer;
  display: block;
  font-weight: normal;
  margin-bottom: 0.5rem;
  /* Styles for required inputs */ }
  label.right {
    float: none;
    text-align: right; }
  label.inline {
    margin: 0 0 1rem 0;
    padding: 0.625rem 0; }
  label small {
    text-transform: capitalize;
    color: #666666; }

select {
  -webkit-appearance: none !important;
  background: #fafafa url("data:image/svg+xml;base64, PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeD0iMHB4IiB5PSIwcHgiIHdpZHRoPSI2cHgiIGhlaWdodD0iM3B4IiB2aWV3Qm94PSIwIDAgNiAzIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA2IDMiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxwb2x5Z29uIHBvaW50cz0iNS45OTIsMCAyLjk5MiwzIC0wLjAwOCwwICIvPjwvc3ZnPg==") no-repeat;
  background-position-x: 97%;
  background-position-y: center;
  border: 1px solid #cccccc;
  padding: 0.5rem;
  font-size: 0.875rem;
  -webkit-border-radius: 0;
  border-radius: 0; }
  select.radius {
    -webkit-border-radius: 3px;
    border-radius: 3px; }
  select:hover {
    background: #f2f2f2 url("data:image/svg+xml;base64, PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeD0iMHB4IiB5PSIwcHgiIHdpZHRoPSI2cHgiIGhlaWdodD0iM3B4IiB2aWV3Qm94PSIwIDAgNiAzIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA2IDMiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxwb2x5Z29uIHBvaW50cz0iNS45OTIsMCAyLjk5MiwzIC0wLjAwOCwwICIvPjwvc3ZnPg==") no-repeat;
    background-position-x: 97%;
    background-position-y: center;
    border-color: #999999; }

@@-moz-document url-prefix() {
  select {
    background: #fafafa; }

  select:hover {
    background: #f2f2f2; } }

/* Attach elements to the beginning or end of an input */
.prefix,
.postfix {
  display: block;
  position: relative;
  z-index: 2;
  text-align: center;
  width: 100%;
  padding-top: 0;
  padding-bottom: 0;
  border-style: solid;
  border-width: 1px;
  overflow: hidden;
  font-size: 0.875rem;
  height: 2.3125rem;
  line-height: 2.3125rem; }

/* Adjust padding, alignment and radius if pre/post element is a button */
.postfix.button {
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  padding-bottom: 0;
  text-align: center;
  line-height: 2.125rem;
  border: none; }

.prefix.button {
  padding-left: 0;
  padding-right: 0;
  padding-top: 0;
  padding-bottom: 0;
  text-align: center;
  line-height: 2.125rem;
  border: none; }

.prefix.button.radius {
  -webkit-border-radius: 0;
  border-radius: 0;
  -moz-border-radius-bottomleft: 3px;
  -moz-border-radius-topleft: 3px;
  -webkit-border-bottom-left-radius: 3px;
  -webkit-border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
  border-top-left-radius: 3px; }

.postfix.button.radius {
  -webkit-border-radius: 0;
  border-radius: 0;
  -moz-border-radius-topright: 3px;
  -moz-border-radius-bottomright: 3px;
  -webkit-border-top-right-radius: 3px;
  -webkit-border-bottom-right-radius: 3px;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px; }

.prefix.button.round {
  -webkit-border-radius: 0;
  border-radius: 0;
  -moz-border-radius-bottomleft: 1000px;
  -moz-border-radius-topleft: 1000px;
  -webkit-border-bottom-left-radius: 1000px;
  -webkit-border-top-left-radius: 1000px;
  border-bottom-left-radius: 1000px;
  border-top-left-radius: 1000px; }

.postfix.button.round {
  -webkit-border-radius: 0;
  border-radius: 0;
  -moz-border-radius-topright: 1000px;
  -moz-border-radius-bottomright: 1000px;
  -webkit-border-top-right-radius: 1000px;
  -webkit-border-bottom-right-radius: 1000px;
  border-top-right-radius: 1000px;
  border-bottom-right-radius: 1000px; }

/* Separate prefix and postfix styles when on span or label so buttons keep their own */
span.prefix, label.prefix {
  background: #f2f2f2;
  border-color: #d9d9d9;
  border-right: none;
  color: #333333; }
  span.prefix.radius, label.prefix.radius {
    -webkit-border-radius: 0;
    border-radius: 0;
    -moz-border-radius-bottomleft: 3px;
    -moz-border-radius-topleft: 3px;
    -webkit-border-bottom-left-radius: 3px;
    -webkit-border-top-left-radius: 3px;
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px; }

span.postfix, label.postfix {
  background: #f2f2f2;
  border-color: #cccccc;
  border-left: none;
  color: #333333; }
  span.postfix.radius, label.postfix.radius {
    -webkit-border-radius: 0;
    border-radius: 0;
    -moz-border-radius-topright: 3px;
    -moz-border-radius-bottomright: 3px;
    -webkit-border-top-right-radius: 3px;
    -webkit-border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
    border-bottom-right-radius: 3px; }

/* Input groups will automatically style first and last elements of the group */
.input-group.radius > *:first-child, .input-group.radius > *:first-child * {
  -moz-border-radius-bottomleft: 3px;
  -moz-border-radius-topleft: 3px;
  -webkit-border-bottom-left-radius: 3px;
  -webkit-border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
  border-top-left-radius: 3px; }
.input-group.radius > *:last-child, .input-group.radius > *:last-child * {
  -moz-border-radius-topright: 3px;
  -moz-border-radius-bottomright: 3px;
  -webkit-border-top-right-radius: 3px;
  -webkit-border-bottom-right-radius: 3px;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px; }
.input-group.round > *:first-child, .input-group.round > *:first-child * {
  -moz-border-radius-bottomleft: 1000px;
  -moz-border-radius-topleft: 1000px;
  -webkit-border-bottom-left-radius: 1000px;
  -webkit-border-top-left-radius: 1000px;
  border-bottom-left-radius: 1000px;
  border-top-left-radius: 1000px; }
.input-group.round > *:last-child, .input-group.round > *:last-child * {
  -moz-border-radius-topright: 1000px;
  -moz-border-radius-bottomright: 1000px;
  -webkit-border-top-right-radius: 1000px;
  -webkit-border-bottom-right-radius: 1000px;
  border-top-right-radius: 1000px;
  border-bottom-right-radius: 1000px; }

/* We use this to get basic styling on all basic form elements */
input[type="text"],
input[type="password"],
input[type="date"],
input[type="datetime"],
input[type="datetime-local"],
input[type="month"],
input[type="week"],
input[type="email"],
input[type="number"],
input[type="search"],
input[type="tel"],
input[type="time"],
input[type="url"],
textarea {
  -webkit-appearance: none;
  -webkit-border-radius: 0;
  border-radius: 0;
  background-color: white;
  font-family: inherit;
  border: 1px solid #cccccc;
  -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
  box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.1);
  color: rgba(0, 0, 0, 0.75);
  display: block;
  font-size: 0.875rem;
  margin: 0 0 1rem 0;
  padding: 0.5rem;
  height: 2.3125rem;
  width: 100%;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-transition: -webkit-box-shadow 0.45s, border-color 0.45s ease-in-out;
  -moz-transition: -moz-box-shadow 0.45s, border-color 0.45s ease-in-out;
  transition: box-shadow 0.45s, border-color 0.45s ease-in-out; }
  input[type="text"]:focus,
  input[type="password"]:focus,
  input[type="date"]:focus,
  input[type="datetime"]:focus,
  input[type="datetime-local"]:focus,
  input[type="month"]:focus,
  input[type="week"]:focus,
  input[type="email"]:focus,
  input[type="number"]:focus,
  input[type="search"]:focus,
  input[type="tel"]:focus,
  input[type="time"]:focus,
  input[type="url"]:focus,
  textarea:focus {
    -webkit-box-shadow: 0 0 5px #999999;
    -moz-box-shadow: 0 0 5px #999999;
    box-shadow: 0 0 5px #999999;
    border-color: #999999; }
  input[type="text"]:focus,
  input[type="password"]:focus,
  input[type="date"]:focus,
  input[type="datetime"]:focus,
  input[type="datetime-local"]:focus,
  input[type="month"]:focus,
  input[type="week"]:focus,
  input[type="email"]:focus,
  input[type="number"]:focus,
  input[type="search"]:focus,
  input[type="tel"]:focus,
  input[type="time"]:focus,
  input[type="url"]:focus,
  textarea:focus {
    background: #fafafa;
    border-color: #999999;
    outline: none; }
  input[type="text"][disabled],
  input[type="password"][disabled],
  input[type="date"][disabled],
  input[type="datetime"][disabled],
  input[type="datetime-local"][disabled],
  input[type="month"][disabled],
  input[type="week"][disabled],
  input[type="email"][disabled],
  input[type="number"][disabled],
  input[type="search"][disabled],
  input[type="tel"][disabled],
  input[type="time"][disabled],
  input[type="url"][disabled],
  textarea[disabled] {
    background-color: #dddddd; }

/* Adjust margin for form elements below */
input[type="file"],
input[type="checkbox"],
input[type="radio"],
select {
  margin: 0 0 1rem 0; }

input[type="checkbox"] + label,
input[type="radio"] + label {
  display: inline-block;
  margin-left: 0.5rem;
  margin-right: 1rem;
  margin-bottom: 0;
  vertical-align: baseline; }

/* Normalize file input width */
input[type="file"] {
  width: 100%; }

/* We add basic fieldset styling */
fieldset {
  border: solid 1px #dddddd;
  padding: 1.25rem;
  margin: 1.125rem 0; }
  fieldset legend {
    font-weight: bold;
    background: white;
    padding: 0 0.1875rem;
    margin: 0;
    margin-left: -0.1875rem; }

/* Error Handling */
[data-abide] .error small.error, [data-abide] span.error, [data-abide] small.error {
  display: block;
  padding: 0.375rem 0.5625rem 0.5625rem;
  margin-top: -1px;
  margin-bottom: 1rem;
  font-size: 0.75rem;
  font-weight: normal;
  font-style: italic;
  background: #c60f13;
  color: white; }
[data-abide] span.error, [data-abide] small.error {
  display: none; }

span.error, small.error {
  display: block;
  padding: 0.375rem 0.5625rem 0.5625rem;
  margin-top: -1px;
  margin-bottom: 1rem;
  font-size: 0.75rem;
  font-weight: normal;
  font-style: italic;
  background: #c60f13;
  color: white; }

.error input,
.error textarea,
.error select {
  margin-bottom: 0; }
.error label,
.error label.error {
  color: #c60f13; }
.error > small,
.error small.error {
  display: block;
  padding: 0.375rem 0.5625rem 0.5625rem;
  margin-top: -1px;
  margin-bottom: 1rem;
  font-size: 0.75rem;
  font-weight: normal;
  font-style: italic;
  background: #c60f13;
  color: white; }
.error span.error-message {
  display: block; }

input.error,
textarea.error {
  margin-bottom: 0; }

label.error {
  color: #c60f13; }

[class*="block-grid-"] {
  display: block;
  padding: 0;
  margin: 0 -0.625rem;
  *zoom: 1; }
  [class*="block-grid-"]:before, [class*="block-grid-"]:after {
    content: " ";
    display: table; }
  [class*="block-grid-"]:after {
    clear: both; }
  [class*="block-grid-"] > li {
    display: inline;
    height: auto;
    float: left;
    padding: 0 0.625rem 1.25rem; }

@@media only screen {
  .small-block-grid-1 > li {
    width: 100%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-1 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-1 > li:nth-of-type(1n+1) {
      clear: both; }

  .small-block-grid-2 > li {
    width: 50%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-2 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-2 > li:nth-of-type(2n+1) {
      clear: both; }

  .small-block-grid-3 > li {
    width: 33.33333%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-3 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-3 > li:nth-of-type(3n+1) {
      clear: both; }

  .small-block-grid-4 > li {
    width: 25%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-4 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-4 > li:nth-of-type(4n+1) {
      clear: both; }

  .small-block-grid-5 > li {
    width: 20%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-5 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-5 > li:nth-of-type(5n+1) {
      clear: both; }

  .small-block-grid-6 > li {
    width: 16.66667%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-6 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-6 > li:nth-of-type(6n+1) {
      clear: both; }

  .small-block-grid-7 > li {
    width: 14.28571%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-7 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-7 > li:nth-of-type(7n+1) {
      clear: both; }

  .small-block-grid-8 > li {
    width: 12.5%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-8 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-8 > li:nth-of-type(8n+1) {
      clear: both; }

  .small-block-grid-9 > li {
    width: 11.11111%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-9 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-9 > li:nth-of-type(9n+1) {
      clear: both; }

  .small-block-grid-10 > li {
    width: 10%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-10 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-10 > li:nth-of-type(10n+1) {
      clear: both; }

  .small-block-grid-11 > li {
    width: 9.09091%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-11 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-11 > li:nth-of-type(11n+1) {
      clear: both; }

  .small-block-grid-12 > li {
    width: 8.33333%;
    padding: 0 0.625rem 1.25rem; }
    .small-block-grid-12 > li:nth-of-type(n) {
      clear: none; }
    .small-block-grid-12 > li:nth-of-type(12n+1) {
      clear: both; } }
@@media only screen and (min-width: 40.063em) {
  .medium-block-grid-1 > li {
    width: 100%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-1 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-1 > li:nth-of-type(1n+1) {
      clear: both; }

  .medium-block-grid-2 > li {
    width: 50%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-2 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-2 > li:nth-of-type(2n+1) {
      clear: both; }

  .medium-block-grid-3 > li {
    width: 33.33333%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-3 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-3 > li:nth-of-type(3n+1) {
      clear: both; }

  .medium-block-grid-4 > li {
    width: 25%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-4 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-4 > li:nth-of-type(4n+1) {
      clear: both; }

  .medium-block-grid-5 > li {
    width: 20%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-5 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-5 > li:nth-of-type(5n+1) {
      clear: both; }

  .medium-block-grid-6 > li {
    width: 16.66667%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-6 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-6 > li:nth-of-type(6n+1) {
      clear: both; }

  .medium-block-grid-7 > li {
    width: 14.28571%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-7 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-7 > li:nth-of-type(7n+1) {
      clear: both; }

  .medium-block-grid-8 > li {
    width: 12.5%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-8 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-8 > li:nth-of-type(8n+1) {
      clear: both; }

  .medium-block-grid-9 > li {
    width: 11.11111%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-9 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-9 > li:nth-of-type(9n+1) {
      clear: both; }

  .medium-block-grid-10 > li {
    width: 10%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-10 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-10 > li:nth-of-type(10n+1) {
      clear: both; }

  .medium-block-grid-11 > li {
    width: 9.09091%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-11 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-11 > li:nth-of-type(11n+1) {
      clear: both; }

  .medium-block-grid-12 > li {
    width: 8.33333%;
    padding: 0 0.625rem 1.25rem; }
    .medium-block-grid-12 > li:nth-of-type(n) {
      clear: none; }
    .medium-block-grid-12 > li:nth-of-type(12n+1) {
      clear: both; } }
@@media only screen and (min-width: 64.063em) {
  .large-block-grid-1 > li {
    width: 100%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-1 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-1 > li:nth-of-type(1n+1) {
      clear: both; }

  .large-block-grid-2 > li {
    width: 50%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-2 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-2 > li:nth-of-type(2n+1) {
      clear: both; }

  .large-block-grid-3 > li {
    width: 33.33333%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-3 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-3 > li:nth-of-type(3n+1) {
      clear: both; }

  .large-block-grid-4 > li {
    width: 25%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-4 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-4 > li:nth-of-type(4n+1) {
      clear: both; }

  .large-block-grid-5 > li {
    width: 20%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-5 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-5 > li:nth-of-type(5n+1) {
      clear: both; }

  .large-block-grid-6 > li {
    width: 16.66667%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-6 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-6 > li:nth-of-type(6n+1) {
      clear: both; }

  .large-block-grid-7 > li {
    width: 14.28571%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-7 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-7 > li:nth-of-type(7n+1) {
      clear: both; }

  .large-block-grid-8 > li {
    width: 12.5%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-8 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-8 > li:nth-of-type(8n+1) {
      clear: both; }

  .large-block-grid-9 > li {
    width: 11.11111%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-9 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-9 > li:nth-of-type(9n+1) {
      clear: both; }

  .large-block-grid-10 > li {
    width: 10%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-10 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-10 > li:nth-of-type(10n+1) {
      clear: both; }

  .large-block-grid-11 > li {
    width: 9.09091%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-11 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-11 > li:nth-of-type(11n+1) {
      clear: both; }

  .large-block-grid-12 > li {
    width: 8.33333%;
    padding: 0 0.625rem 1.25rem; }
    .large-block-grid-12 > li:nth-of-type(n) {
      clear: none; }
    .large-block-grid-12 > li:nth-of-type(12n+1) {
      clear: both; } }
.flex-video {
  position: relative;
  padding-top: 1.5625rem;
  padding-bottom: 67.5%;
  height: 0;
  margin-bottom: 1rem;
  overflow: hidden; }
  .flex-video.widescreen {
    padding-bottom: 57.25%; }
  .flex-video.vimeo {
    padding-top: 0; }
  .flex-video iframe,
  .flex-video object,
  .flex-video embed,
  .flex-video video {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%; }

.keystroke,
kbd {
  background-color: #ededed;
  border-color: #dbdbdb;
  color: #222222;
  border-style: solid;
  border-width: 1px;
  margin: 0;
  font-family: "Consolas", "Menlo", "Courier", monospace;
  font-size: 0.875rem;
  padding: 0.125rem 0.25rem 0;
  -webkit-border-radius: 3px;
  border-radius: 3px; }

/* Foundation Visibility HTML Classes */
.show-for-small,
.show-for-small-only,
.show-for-medium-down,
.show-for-large-down,
.hide-for-medium,
.hide-for-medium-up,
.hide-for-medium-only,
.hide-for-large,
.hide-for-large-up,
.hide-for-large-only,
.hide-for-xlarge,
.hide-for-xlarge-up,
.hide-for-xlarge-only,
.hide-for-xxlarge-up,
.hide-for-xxlarge-only {
  display: inherit !important; }

.hide-for-small,
.hide-for-small-only,
.hide-for-medium-down,
.show-for-medium,
.show-for-medium-up,
.show-for-medium-only,
.hide-for-large-down,
.show-for-large,
.show-for-large-up,
.show-for-large-only,
.show-for-xlarge,
.show-for-xlarge-up,
.show-for-xlarge-only,
.show-for-xxlarge-up,
.show-for-xxlarge-only {
  display: none !important; }

/* Specific visibility for tables */
table.show-for-small, table.show-for-small-only, table.show-for-medium-down, table.show-for-large-down, table.hide-for-medium, table.hide-for-medium-up, table.hide-for-medium-only, table.hide-for-large, table.hide-for-large-up, table.hide-for-large-only, table.hide-for-xlarge, table.hide-for-xlarge-up, table.hide-for-xlarge-only, table.hide-for-xxlarge-up, table.hide-for-xxlarge-only {
  display: table; }

thead.show-for-small, thead.show-for-small-only, thead.show-for-medium-down, thead.show-for-large-down, thead.hide-for-medium, thead.hide-for-medium-up, thead.hide-for-medium-only, thead.hide-for-large, thead.hide-for-large-up, thead.hide-for-large-only, thead.hide-for-xlarge, thead.hide-for-xlarge-up, thead.hide-for-xlarge-only, thead.hide-for-xxlarge-up, thead.hide-for-xxlarge-only {
  display: table-header-group !important; }

tbody.show-for-small, tbody.show-for-small-only, tbody.show-for-medium-down, tbody.show-for-large-down, tbody.hide-for-medium, tbody.hide-for-medium-up, tbody.hide-for-medium-only, tbody.hide-for-large, tbody.hide-for-large-up, tbody.hide-for-large-only, tbody.hide-for-xlarge, tbody.hide-for-xlarge-up, tbody.hide-for-xlarge-only, tbody.hide-for-xxlarge-up, tbody.hide-for-xxlarge-only {
  display: table-row-group !important; }

tr.show-for-small, tr.show-for-small-only, tr.show-for-medium-down, tr.show-for-large-down, tr.hide-for-medium, tr.hide-for-medium-up, tr.hide-for-medium-only, tr.hide-for-large, tr.hide-for-large-up, tr.hide-for-large-only, tr.hide-for-xlarge, tr.hide-for-xlarge-up, tr.hide-for-xlarge-only, tr.hide-for-xxlarge-up, tr.hide-for-xxlarge-only {
  display: table-row !important; }

td.show-for-small, td.show-for-small-only, td.show-for-medium-down
td.show-for-large-down, td.hide-for-medium, td.hide-for-medium-up, td.hide-for-large, td.hide-for-large-up, td.hide-for-xlarge
td.hide-for-xlarge-up, td.hide-for-xxlarge-up,
th.show-for-small,
th.show-for-small-only,
th.show-for-medium-down
th.show-for-large-down,
th.hide-for-medium,
th.hide-for-medium-up,
th.hide-for-large,
th.hide-for-large-up,
th.hide-for-xlarge
th.hide-for-xlarge-up,
th.hide-for-xxlarge-up {
  display: table-cell !important; }

/* Medium Displays: 641px and up */
@@media only screen and (min-width: 40.063em) {
  .hide-for-small,
  .hide-for-small-only,
  .show-for-medium,
  .show-for-medium-down,
  .show-for-medium-up,
  .show-for-medium-only,
  .hide-for-large,
  .hide-for-large-up,
  .hide-for-large-only,
  .hide-for-xlarge,
  .hide-for-xlarge-up,
  .hide-for-xlarge-only,
  .hide-for-xxlarge-up,
  .hide-for-xxlarge-only {
    display: inherit !important; }

  .show-for-small,
  .show-for-small-only,
  .hide-for-medium,
  .hide-for-medium-down,
  .hide-for-medium-up,
  .hide-for-medium-only,
  .hide-for-large-down,
  .show-for-large,
  .show-for-large-up,
  .show-for-large-only,
  .show-for-xlarge,
  .show-for-xlarge-up,
  .show-for-xlarge-only,
  .show-for-xxlarge-up,
  .show-for-xxlarge-only {
    display: none !important; }

  /* Specific visibility for tables */
  table.hide-for-small, table.hide-for-small-only, table.show-for-medium, table.show-for-medium-down, table.show-for-medium-up, table.show-for-medium-only, table.hide-for-large, table.hide-for-large-up, table.hide-for-large-only, table.hide-for-xlarge, table.hide-for-xlarge-up, table.hide-for-xlarge-only, table.hide-for-xxlarge-up, table.hide-for-xxlarge-only {
    display: table; }

  thead.hide-for-small, thead.hide-for-small-only, thead.show-for-medium, thead.show-for-medium-down, thead.show-for-medium-up, thead.show-for-medium-only, thead.hide-for-large, thead.hide-for-large-up, thead.hide-for-large-only, thead.hide-for-xlarge, thead.hide-for-xlarge-up, thead.hide-for-xlarge-only, thead.hide-for-xxlarge-up, thead.hide-for-xxlarge-only {
    display: table-header-group !important; }

  tbody.hide-for-small, tbody.hide-for-small-only, tbody.show-for-medium, tbody.show-for-medium-down, tbody.show-for-medium-up, tbody.show-for-medium-only, tbody.hide-for-large, tbody.hide-for-large-up, tbody.hide-for-large-only, tbody.hide-for-xlarge, tbody.hide-for-xlarge-up, tbody.hide-for-xlarge-only, tbody.hide-for-xxlarge-up, tbody.hide-for-xxlarge-only {
    display: table-row-group !important; }

  tr.hide-for-small, tr.hide-for-small-only, tr.show-for-medium, tr.show-for-medium-down, tr.show-for-medium-up, tr.show-for-medium-only, tr.hide-for-large, tr.hide-for-large-up, tr.hide-for-large-only, tr.hide-for-xlarge, tr.hide-for-xlarge-up, tr.hide-for-xlarge-only, tr.hide-for-xxlarge-up, tr.hide-for-xxlarge-only {
    display: table-row !important; }

  td.hide-for-small, td.hide-for-small-only, td.show-for-medium, td.show-for-medium-down, td.show-for-medium-up, td.show-for-medium-only, td.hide-for-large, td.hide-for-large-up, td.hide-for-large-only, td.hide-for-xlarge, td.hide-for-xlarge-up, td.hide-for-xlarge-only, td.hide-for-xxlarge-up, td.hide-for-xxlarge-only,
  th.hide-for-small,
  th.hide-for-small-only,
  th.show-for-medium,
  th.show-for-medium-down,
  th.show-for-medium-up,
  th.show-for-medium-only,
  th.hide-for-large,
  th.hide-for-large-up,
  th.hide-for-large-only,
  th.hide-for-xlarge,
  th.hide-for-xlarge-up,
  th.hide-for-xlarge-only,
  th.hide-for-xxlarge-up,
  th.hide-for-xxlarge-only {
    display: table-cell !important; } }
/* Large Displays: 1024px and up */
@@media only screen and (min-width: 64.063em) {
  .hide-for-small,
  .hide-for-small-only,
  .hide-for-medium,
  .hide-for-medium-down,
  .hide-for-medium-only,
  .show-for-medium-up,
  .show-for-large,
  .show-for-large-up,
  .show-for-large-only,
  .hide-for-xlarge,
  .hide-for-xlarge-up,
  .hide-for-xlarge-only,
  .hide-for-xxlarge-up,
  .hide-for-xxlarge-only {
    display: inherit !important; }

  .show-for-small-only,
  .show-for-medium,
  .show-for-medium-down,
  .show-for-medium-only,
  .hide-for-large,
  .hide-for-large-up,
  .hide-for-large-only,
  .show-for-xlarge,
  .show-for-xlarge-up,
  .show-for-xlarge-only,
  .show-for-xxlarge-up,
  .show-for-xxlarge-only {
    display: none !important; }

  /* Specific visilbity for tables */
  table.hide-for-small, table.hide-for-small-only, table.hide-for-medium, table.hide-for-medium-down, table.hide-for-medium-only, table.show-for-medium-up, table.show-for-large, table.show-for-large-up, table.show-for-large-only, table.hide-for-xlarge, table.hide-for-xlarge-up, table.hide-for-xlarge-only, table.hide-for-xxlarge-up, table.hide-for-xxlarge-only {
    display: table; }

  thead.hide-for-small, thead.hide-for-small-only, thead.hide-for-medium, thead.hide-for-medium-down, thead.hide-for-medium-only, thead.show-for-medium-up, thead.show-for-large, thead.show-for-large-up, thead.show-for-large-only, thead.hide-for-xlarge, thead.hide-for-xlarge-up, thead.hide-for-xlarge-only, thead.hide-for-xxlarge-up, thead.hide-for-xxlarge-only {
    display: table-header-group !important; }

  tbody.hide-for-small, tbody.hide-for-small-only, tbody.hide-for-medium, tbody.hide-for-medium-down, tbody.hide-for-medium-only, tbody.show-for-medium-up, tbody.show-for-large, tbody.show-for-large-up, tbody.show-for-large-only, tbody.hide-for-xlarge, tbody.hide-for-xlarge-up, tbody.hide-for-xlarge-only, tbody.hide-for-xxlarge-up, tbody.hide-for-xxlarge-only {
    display: table-row-group !important; }

  tr.hide-for-small, tr.hide-for-small-only, tr.hide-for-medium, tr.hide-for-medium-down, tr.hide-for-medium-only, tr.show-for-medium-up, tr.show-for-large, tr.show-for-large-up, tr.show-for-large-only, tr.hide-for-xlarge, tr.hide-for-xlarge-up, tr.hide-for-xlarge-only, tr.hide-for-xxlarge-up, tr.hide-for-xxlarge-only {
    display: table-row !important; }

  td.hide-for-small, td.hide-for-small-only, td.hide-for-medium, td.hide-for-medium-down, td.hide-for-medium-only, td.show-for-medium-up, td.show-for-large, td.show-for-large-up, td.show-for-large-only, td.hide-for-xlarge, td.hide-for-xlarge-up, td.hide-for-xlarge-only, td.hide-for-xxlarge-up, td.hide-for-xxlarge-only,
  th.hide-for-small,
  th.hide-for-small-only,
  th.hide-for-medium,
  th.hide-for-medium-down,
  th.hide-for-medium-only,
  th.show-for-medium-up,
  th.show-for-large,
  th.show-for-large-up,
  th.show-for-large-only,
  th.hide-for-xlarge,
  th.hide-for-xlarge-up,
  th.hide-for-xlarge-only,
  th.hide-for-xxlarge-up,
  th.hide-for-xxlarge-only {
    display: table-cell !important; } }
/* X-Large Displays: 1441 and up */
@@media only screen and (min-width: 90.063em) {
  .hide-for-small,
  .hide-for-small-only,
  .hide-for-medium,
  .hide-for-medium-down,
  .hide-for-medium-only,
  .show-for-medium-up,
  .show-for-large-up,
  .show-for-xlarge,
  .show-for-xlarge-up,
  .show-for-xlarge-only,
  .hide-for-xxlarge-up,
  .hide-for-xxlarge-only {
    display: inherit !important; }

  .show-for-small-only,
  .show-for-medium,
  .show-for-medium-down,
  .show-for-medium-only,
  .show-for-large,
  .show-for-large-only,
  .show-for-large-down,
  .hide-for-xlarge,
  .hide-for-xlarge-up,
  .hide-for-xlarge-only,
  .show-for-xxlarge-up,
  .show-for-xxlarge-only {
    display: none !important; }

  /* Specific visilbity for tables */
  table.hide-for-small, table.hide-for-small-only, table.hide-for-medium, table.hide-for-medium-down, table.hide-for-medium-only, table.show-for-medium-up, table.show-for-large-up, table.show-for-xlarge, table.show-for-xlarge-up, table.show-for-xlarge-only, table.hide-for-xxlarge-up, table.hide-for-xxlarge-only {
    display: table; }

  thead.hide-for-small, thead.hide-for-small-only, thead.hide-for-medium, thead.hide-for-medium-down, thead.hide-for-medium-only, thead.show-for-medium-up, thead.show-for-large-up, thead.show-for-xlarge, thead.show-for-xlarge-up, thead.show-for-xlarge-only, thead.hide-for-xxlarge-up, thead.hide-for-xxlarge-only {
    display: table-header-group !important; }

  tbody.hide-for-small, tbody.hide-for-small-only, tbody.hide-for-medium, tbody.hide-for-medium-down, tbody.hide-for-medium-only, tbody.show-for-medium-up, tbody.show-for-large-up, tbody.show-for-xlarge, tbody.show-for-xlarge-up, tbody.show-for-xlarge-only, tbody.hide-for-xxlarge-up, tbody.hide-for-xxlarge-only {
    display: table-row-group !important; }

  tr.hide-for-small, tr.hide-for-small-only, tr.hide-for-medium, tr.hide-for-medium-down, tr.hide-for-medium-only, tr.show-for-medium-up, tr.show-for-large-up, tr.show-for-xlarge, tr.show-for-xlarge-up, tr.show-for-xlarge-only, tr.hide-for-xxlarge-up, tr.hide-for-xxlarge-only {
    display: table-row !important; }

  td.hide-for-small, td.hide-for-small-only, td.hide-for-medium, td.hide-for-medium-down, td.hide-for-medium-only, td.show-for-medium-up, td.show-for-large-up, td.show-for-xlarge, td.show-for-xlarge-up, td.show-for-xlarge-only, td.hide-for-xxlarge-up, td.hide-for-xxlarge-only,
  th.hide-for-small,
  th.hide-for-small-only,
  th.hide-for-medium,
  th.hide-for-medium-down,
  th.hide-for-medium-only,
  th.show-for-medium-up,
  th.show-for-large-up,
  th.show-for-xlarge,
  th.show-for-xlarge-up,
  th.show-for-xlarge-only,
  th.hide-for-xxlarge-up,
  th.hide-for-xxlarge-only {
    display: table-cell !important; } }
/* XX-Large Displays: 1920 and up */
@@media only screen and (min-width: 120.063em) {
  .hide-for-small,
  .hide-for-small-only,
  .hide-for-medium,
  .hide-for-medium-down,
  .hide-for-medium-only,
  .show-for-medium-up,
  .show-for-large-up,
  .show-for-xlarge-up,
  .show-for-xxlarge-up,
  .show-for-xxlarge-only {
    display: inherit !important; }

  .show-for-small-only,
  .show-for-medium,
  .show-for-medium-down,
  .show-for-medium-only,
  .show-for-large,
  .show-for-large-only,
  .show-for-large-down,
  .hide-for-xlarge,
  .show-for-xlarge-only,
  .hide-for-xxlarge-up,
  .hide-for-xxlarge-only {
    display: none !important; }

  /* Specific visilbity for tables */
  table.hide-for-small, table.hide-for-small-only, table.hide-for-medium, table.hide-for-medium-down, table.hide-for-medium-only, table.show-for-medium-up, table.show-for-large-up, table.show-for-xlarge-up, table.show-for-xxlarge-up, table.show-for-xxlarge-only {
    display: table; }

  thead.hide-for-small, thead.hide-for-small-only, thead.hide-for-medium, thead.hide-for-medium-down, thead.hide-for-medium-only, thead.show-for-medium-up, thead.show-for-large-up, thead.show-for-xlarge-up, thead.show-for-xxlarge-up, thead.show-for-xxlarge-only {
    display: table-header-group !important; }

  tbody.hide-for-small, tbody.hide-for-small-only, tbody.hide-for-medium, tbody.hide-for-medium-down, tbody.hide-for-medium-only, tbody.show-for-medium-up, tbody.show-for-large-up, tbody.show-for-xlarge-up, tbody.show-for-xxlarge-up, tbody.show-for-xxlarge-only {
    display: table-row-group !important; }

  tr.hide-for-small, tr.hide-for-small-only, tr.hide-for-medium, tr.hide-for-medium-down, tr.hide-for-medium-only, tr.show-for-medium-up, tr.show-for-large-up, tr.show-for-xlarge-up, tr.show-for-xxlarge-up, tr.show-for-xxlarge-only {
    display: table-row !important; }

  td.hide-for-small, td.hide-for-small-only, td.hide-for-medium, td.hide-for-medium-down, td.hide-for-medium-only, td.show-for-medium-up, td.show-for-large-up, td.show-for-xlarge-up, td.show-for-xxlarge-up, td.show-for-xxlarge-only,
  th.hide-for-small,
  th.hide-for-small-only,
  th.hide-for-medium,
  th.hide-for-medium-down,
  th.hide-for-medium-only,
  th.show-for-medium-up,
  th.show-for-large-up,
  th.show-for-xlarge-up,
  th.show-for-xxlarge-up,
  th.show-for-xxlarge-only {
    display: table-cell !important; } }
/* Orientation targeting */
.show-for-landscape,
.hide-for-portrait {
  display: inherit !important; }

.hide-for-landscape,
.show-for-portrait {
  display: none !important; }

/* Specific visilbity for tables */
table.hide-for-landscape, table.show-for-portrait {
  display: table; }

thead.hide-for-landscape, thead.show-for-portrait {
  display: table-header-group !important; }

tbody.hide-for-landscape, tbody.show-for-portrait {
  display: table-row-group !important; }

tr.hide-for-landscape, tr.show-for-portrait {
  display: table-row !important; }

td.hide-for-landscape, td.show-for-portrait,
th.hide-for-landscape,
th.show-for-portrait {
  display: table-cell !important; }

@@media only screen and (orientation: landscape) {
  .show-for-landscape,
  .hide-for-portrait {
    display: inherit !important; }

  .hide-for-landscape,
  .show-for-portrait {
    display: none !important; }

  /* Specific visilbity for tables */
  table.show-for-landscape, table.hide-for-portrait {
    display: table; }

  thead.show-for-landscape, thead.hide-for-portrait {
    display: table-header-group !important; }

  tbody.show-for-landscape, tbody.hide-for-portrait {
    display: table-row-group !important; }

  tr.show-for-landscape, tr.hide-for-portrait {
    display: table-row !important; }

  td.show-for-landscape, td.hide-for-portrait,
  th.show-for-landscape,
  th.hide-for-portrait {
    display: table-cell !important; } }
@@media only screen and (orientation: portrait) {
  .show-for-portrait,
  .hide-for-landscape {
    display: inherit !important; }

  .hide-for-portrait,
  .show-for-landscape {
    display: none !important; }

  /* Specific visilbity for tables */
  table.show-for-portrait, table.hide-for-landscape {
    display: table; }

  thead.show-for-portrait, thead.hide-for-landscape {
    display: table-header-group !important; }

  tbody.show-for-portrait, tbody.hide-for-landscape {
    display: table-row-group !important; }

  tr.show-for-portrait, tr.hide-for-landscape {
    display: table-row !important; }

  td.show-for-portrait, td.hide-for-landscape,
  th.show-for-portrait,
  th.hide-for-landscape {
    display: table-cell !important; } }
/* Touch-enabled device targeting */
.show-for-touch {
  display: none !important; }

.hide-for-touch {
  display: inherit !important; }

.touch .show-for-touch {
  display: inherit !important; }

.touch .hide-for-touch {
  display: none !important; }

/* Specific visilbity for tables */
table.hide-for-touch {
  display: table; }

.touch table.show-for-touch {
  display: table; }

thead.hide-for-touch {
  display: table-header-group !important; }

.touch thead.show-for-touch {
  display: table-header-group !important; }

tbody.hide-for-touch {
  display: table-row-group !important; }

.touch tbody.show-for-touch {
  display: table-row-group !important; }

tr.hide-for-touch {
  display: table-row !important; }

.touch tr.show-for-touch {
  display: table-row !important; }

td.hide-for-touch {
  display: table-cell !important; }

.touch td.show-for-touch {
  display: table-cell !important; }

th.hide-for-touch {
  display: table-cell !important; }

.touch th.show-for-touch {
  display: table-cell !important; }
 </style>
 
 <style type="text/css">
	
	body{
	font-size: 13px;
	font-family : Arial, Helvetica, sans-serif;
	color: #dd9933;
	}
	.main-wrapper{
	background: #111111			}
	.detail, h3.header-title, #sidebar .image-info h3, .main-menu>li:hover>a,.follow-list h3:hover, #sidebar h3, #sidebar #searchsubmit, .pagination span,  nav li a:hover, span.most-loved{background: green;
	}
	.entry-meta ul li, .copyright a, .logo h1 a, .logo h2 a{color:green}
	#header {
	border-color: green!important;
	}
	.image-info h3:after{
	border-top: 8px solid green!important;
	}
	.holder .entry-title a:hover{color:#aaa			}
	a {
	color:#aaa;
	}
	#sidebar ul li:before{
	background: #aaa;
	}
	a:hover {
	color:#bbb;
	}
	a:active {
	color:#ccc;
	}
	#header{
	margin: 0 auto;
	}
	.termpost {
		width: 24.3%;
		float: left;
		margin-bottom: 10px;
		margin-right: 10px;
		text-align: center;
		padding-bottom: 1px;
		box-shadow: 0px 1px 4px #000;
		background: #000;
		padding: 3px;
		color: #333;
		overflow: hidden;
		position: relative;
		z-index: 0;
	}
	@@media (min-width:985px) {
	.termpost:nth-child(4n+0) {
	margin-right: 0px !important
	}
	}	
</style>