@extends('layout')

@section('content')

		<div id="main" class="large-12 medium-12 small-12 columns">
	<div id="primary" class="site-content large-12 medium-12 small-12 columns">
		<div id="content" class="large-12" role="main">
			
				<div class="single-ads">
					{!! ads('responsive') !!}			
				</div>
				
		
		
		<span class="most-loved">Most Loved</span>
		<div class="holder" style="position:relative">

@foreach( $results as $key => $item )
<article id="post-{{ $key }}" class="post-{{ $key }} post type-post status-publish format-standard has-post-thumbnail hentry">
		<div class="entry-content" style="position:relative">
			<a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}"> <img width="300" height="169" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" class="attachment-medium size-medium wp-post-image" alt="{{ $item['title'] }}" sizes="(max-width: 300px) 100vw, 300px" title="{{ $item['title'] }}"/></a>
		</div><!-- .entry-content -->
		<header class="entry-header">
			<h2 class="entry-title">
<a href="#" title="Permalink to {{ $item['title'] }}" rel="bookmark">{{ $item['title'] }}</a>
			</h2>
			
		</header><!-- .entry-header -->
		<footer class="entry-meta">
			<ul><li>Size : {{ $item['size'] }}</li></ul>
			
		</footer><!-- .entry-meta -->
	</article>
@endforeach	
<!-- #post -->

@foreach( $random_terms as $term )
	<div class="termpost">
		<a href="{{ permalink( $term ) }}" title="{{ $term }}">{{ ucwords($term) }}</a>
	</div>
@endforeach

						@php
						  $pagination = range(1,20)
						@endphp
		 
						<!--Start Pagination-->
						<div class="navigation">
							<div class='pagination'>
							
							 @foreach($pagination as $n => $page)
								@if( input()->get('page') == $n+1 )
									<strong class='current'>{{ $page }}</strong>
								@elseif( $page == end($pagination) )
									<a href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a>
								@else
									<a href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a>
								@endif
							 @endforeach

							</div>          
						</div>
						<!--End Pagination-->
						</div>
<div class="single-ads">
{!! ads('responsive') !!}		
</div>
				</div>
	</div><!-- end #primary-->
</div><!-- end #main-->
	




@endsection