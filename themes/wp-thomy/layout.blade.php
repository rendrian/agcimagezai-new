<!DOCTYPE html>
<html lang="en-US">
   <head>
	  @include('header')  
	  <link href="{{ theme_url('style.css') }}" rel="stylesheet">
   </head>
   <body>
      <div id="top">
         <div id="top-mid">
            <header class="logo">
               <a href="{{ home_url() }}" title="{{ sitename() }} Homepage">{{ sitename() }}</a>
            </header>
            <nav class="menubox">
               <ul class="menu">
                  <li><a href="{{ home_url('feed') }}">Feed</a></li>
                  <li><a href="{{ home_url('sitemap.xml') }}">Crawler</a></li>
               </ul>
            </nav>
            <div class="cl"></div>
         </div>
      </div>
      <div id="main">

	@if( $path != 'attachment' && $path != 'page' )	
		<div id="contentbox"> 
	@else
		<div id="contentfull"> 	
	@endif
		
			@yield('content')
         </div>

		@if( $path != 'attachment' && $path != 'page' )	 
			@include('sidebar')
		@endif
	
	


      </div>

      <div id="bottom">
         <footer class="bottom-mid">
            <div class="cl"></div>
            <div class="copy left">Copyright &copy; {{ date('Y') }} {{ sitename() }}. All Rights Reserved.</div>
            <div class="menubox">
               <ul class="menu">
                  <li><a rel="follow" href="{{ home_url() }}">Home</a></li>
					@foreach( config('themes.page') as $page )
						<li><a rel="nofollow"href="{{ permalink($page, 'page') }}">{{ ucwords($page) }}</a></li>
					@endforeach
                  <li><a href="{{ home_url('sitemap-image.xml') }}">Sitemap</a></li>
               </ul>
            </div>
            <div class="cl"></div>
         </footer>
      </div>
@include('footer')	  
   </body>
</html>