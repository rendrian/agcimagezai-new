@extends('layout')

@section('content')

   <div class="content">
      <h1 class="post_title">{{ $subquery }}</h1>

		{!! ads('responsive') !!}

      <div class="post_img">

			<img width="1920" height="1200" src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';"  class="attachment-full size-full" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}"/>

		 <h2>{{ $subquery }}</h2>
     
			{!! ads('responsive') !!}

		 <div class="rads">
            <div class="nextat">
               <a href="{{ attachment_url( $query, $gallery['next']['title'] ) }}">Next</a>
            </div>
            <div class="prevat">
               <a href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}">Prev</a>
            </div>
         </div>
      </div>
      <div class="cl"></div>
      <div class="lbox">
         <div class="sharebtn">
            <a class="facebook" href="https://www.facebook.com/sharer.php?u={{ urlencode( get_permalink() ) }}" title="Share on Facebook!" target="_blank" rel="nofollow">Facebook</a>
            <a class="pinterest" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" title="Pin It!" rel="nofollow">Pinterest</a>
            <a class="twitter" href="http://twitter.com/share?url={{ urlencode( get_permalink() ) }}&amp;text={{ $subquery }}&amp;hashtags=home,architecture,interior,design,house,decor" title="Tweet this!" target="_blank" rel="nofollow">Twitter</a>
            <a class="googleplus" href="https://plus.google.com/share?url={{ urlencode( get_permalink() ) }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false; " title="Share on Google+!" target="_blank" rel="nofollow">Google+</a>
         </div>
         <div class="downloadbtn">
            <a rel="nofollow" href="{{ $gallery['current']['url'] }}" download="{{ $subquery }} - {{ sitename() }}" title="Download {{ $subquery }}">Download</a>
         </div>
      </div>
      <p>

		  <strong>{{ $subquery }} - {{ $query }}</strong> part of <a href="{{ get_permalink() }}" rev="attachment">{{ $query }}</a>. {{ $query }}  is one {{ spin('{of our|in our|of our own|of the|of your}') }} collection.<br>
		We choose the image option for display
		We {{ spin('{paid attention to|taken notice of}') }} you to {{ spin('{provide a|give a}') }} good picture and with {{ spin('{high definition|hi-def}') }} (HD). {{ spin('{If you want to|If you wish to}') }} keep this image right click {{ spin('{directly on|on}') }} the picture. {{ spin('{Select|Choose}') }} "Save As.." and than choose {{ spin('{the location|the positioning}') }} or the folder where {{ spin('{will you|do you want to}') }} keep this picture, .Jpg is a default format picture, you can also change the format picture, {{ spin('{the way|just how}') }} is at {{ spin('{will save|helps you to save}') }} in {{ spin('{storage|storage space|safe-keeping|storage area}') }} the image {{ spin('{can add|can truly add}') }} {{ spin('{extension|expansion}') }} or add other {{ spin('{extension|expansion}') }} as .png .jpeg, then {{ spin('{you will get|you can get}') }} image {{ $subquery }} with the {{ spin('{resolution|quality|image resolution}') }} are {{ spin('{equal to|add up to}') }} in {{ spin('{your computer|your personal computer}') }}.<br><br>
		{{ spin('{You can see|You can view}') }} the picture in a gallery other similar with {{ spin('{previous|earlier|prior|past}') }} navigate using the image and {{ spin('{the next|another}') }} image that will {{ spin('{facilitate|help|assist in|aid|help in|accomplish}') }} you in venturing website. The picture {{ spin('{you see|you observe|the thing is|the truth is|the thing is that|the simple truth is}') }} in {{ spin('{the public|the general public}') }} domain and {{ spin('{in our|inside our}') }} website {{ spin('{will be the|would be the}') }} same, {{ spin('{so you|and that means you|which means you}') }} will {{ spin('{have no|have not any}') }} trouble finding pictures is.<br>
		This image is one of {{ spin('{the existing|the prevailing}') }} images in the <strong>{{ $subquery }}</strong>, {{ spin('{we provide|we offer}') }} other similar images {{ spin('{in one|in a single}') }} post, {{ spin('{so this|which means this}') }} {{ spin('{will allow|allows}') }} a user {{ spin('{in search of|searching for}') }} a {{ spin('{wanted|desired|needed|wished|required|sought}') }} picture.

	  </p>
      <div class="cl"></div>
      <div class="gallery">
         <h3>Gallery of {{ $subquery }} - {{ $query }}</h3>
			@foreach($results as $i => $item)
			<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" rel="bookmark" title="{{ $results[$i]['title'] }}">
			 <img width="730" height="960" src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-full size-full" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
			</a>
			@endforeach
      </div>
      <div class="cl"></div>
   </div>

@endsection
