@extends('layout')

@section('ads')
  {!! ads('responsive') !!}
@endsection

@section('content')

<h1 class="page_title">Welcome to {{ sitename() }}</h1>

   @yield('ads')

   <div class="contentlist">

      @php
			$cols   = array_group( $results, 4 );
      @endphp

      @foreach ($cols as $item)

      <div class="imagebox">

			  @php
				$lastitem = end($item);
			  @endphp

			<a href="{{ attachment_url( $query, $lastitem['title'] ) }}" rel="bookmark" title="{{ ucwords( $lastitem['title'] ) }}">
				<img src="{{ $lastitem['small'] }}" data-src="{{ $lastitem['url'] }}" onerror="this.onerror=null;this.src='{{ $lastitem['small'] }}';" class="attachment-full size-full wp-post-image" alt="{{ ucwords( $lastitem['title'] ) }}" title="{{ ucwords( $lastitem['title'] ) }}" />
			</a>

         <h2><a href="{{ permalink($query) }}" title="{{ ucwords( $lastitem['title'] ) }}" rel="bookmark">{{ ucwords( limit_the_words( $lastitem['title'], 5 ) ) }}</a></h2>



         <div class="imgkecil">

			  @for( $i=0; $i<=count($item)-2; $i++ )

				  <a href="{{ attachment_url( $query, $item[$i]['title'] ) }}" rel="bookmark" title="{{ ucwords( $item[$i]['title'] ) }}">
					<img width="730" height="960"  src="{{ $item[$i]['small'] }}" data-src="{{ $item[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $item[$i]['small'] }}';"  class="attachment-full size-full" alt="{{ ucwords( $item[$i]['title'] ) }}" title="{{ ucwords( $item[$i]['title'] ) }}"/>
				  </a>

			  @endfor

         </div>
      </div>

	@endforeach

@php
  $pagination = range(1,20)
@endphp

      <div id="post-navigator">
         <div class="wp-pagenavi">
			 <span class="pages">Page 1 of {{ end($pagination) }}:</span>

         @foreach($pagination as $n => $page)
            @if( input()->get('page') == $n+1 )
                <strong class='current'>{{ $page }}</strong>
            @elseif( $page == end($pagination) )
                <a href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a>
            @else
                <a href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a>
            @endif
         @endforeach

		</div>
       <div class="cl"></div>
      </div>
   </div>

@endsection
