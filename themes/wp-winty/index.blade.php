@extends('layout')

@section('content')

<div class="ha mt m18 tc">
	{!! ads('responsive') !!}
</div>
<div class="c"></div>



<div id="jl">
  @for( $i=0; $i<=count($results)-1; $i++ )
<div class="j">
    <a class="ij" href="{{ attachment_url( $query, $results[$i]['title'] ) }}" rel="bookmark" title="{{ ucwords($query) }} {{ ucwords( $results[$i]['title'] ) }}">
    <img width="70" height="55"  src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-featured-thumb" alt="{{ ucwords($query) }} {{ ucwords( $results[$i]['title'] ) }}" title="{{ ucwords($query) }} {{ ucwords( $results[$i]['title'] ) }}"/>
    </a>
		<div class="c p">
			<h2 class="jh">
				<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ ucwords( $results[$i]['title'] ) }}"></a>{{ ucwords( $results[$i]['title'] ) }}</h2>
			</div>
</div>
  @endfor


@php
  $pagination = range(1,100)
@endphp

<div class="u tc m18">
	<div class="tc">Page 1 of {{ end($pagination) }}</div>
	<div class="c"></div>
</div>

<div class="u m18 tc">

       @foreach($pagination as $n => $page)
			 			<a href="{{ home_url() }}?page={{ $page }}">✶</a>
       @endforeach

  </div>

<style>
.jlist{
	border: 1px solid #ddd;
	width: 33%;
	float:left;
}
</style>

	@foreach( $random_terms as $term )

	<div class="jlist">
						<a href="{{ permalink($term) }}" rel="bookmark">{{ $term }}</a>
	</div>

	@endforeach

</div>

<div class="c"></div>




@endsection
