@extends('layout')

@section('content')

<div id="j">
  <div id="si">
    <h2>{{ $page_title }}</h2>

    <div class="bc">
      <a href="{{ home_url() }}" rel="nofollow">Home</a><i>〉</i>
      <a rel="nofollow">{{ $page_title }}</a>
    </div>

        @include('page/' . strtolower($page_title) )
  </div>
</div>

<div id="t">
  <p class="ts">Newest Articles</p>
  <ul>
    @foreach( $random_terms as $term )
      <li><a rel="nofollow" href="{{ permalink($term) }}">{{ $term }}</a></li>
    @endforeach
  </ul>
  </div>
<div class="c"></div>
@endsection
