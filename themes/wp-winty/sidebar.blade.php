<div id="sidebar">

{!! ads('responsive') !!}

@if( !is_page() )
<div class="magc-sidebarbox">
<h3 class="magc-sidebarname">Random Post</h3>
<ul>

  @foreach( $random_terms as $term )
  <li>
    <h3><a href="{{ permalink($term) }}">{{ $term }}</a></h3>
  </li>
  @endforeach
</ul>
      </div><!-- sidebarbox -->
@endif
</div><!-- sidebar -->
<div class="clear"></div>
