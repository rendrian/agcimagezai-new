@extends('layout')

@section('content')

<div id="master_content">


  @foreach( $results as $key => $item)
     <div class="master_posting">
        <div itemscope itemtype="schema.org/ImageObject" >
           <meta itemprop="name" content="{{ strtolower($query) }} - {{ $item['title'] }}"/>
           <meta itemprop="representativeOfPage" content="true"/>
           <a href="{{ attachment_url( $query, $item['title'] ) }}" rel="bookmark" title="{{ strtolower($query) }} {{ $item['title'] }}">
             <img width="218" height="134" class="attachment-featured-home size-featured-home" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}"/>
           </a>
           <div class="ttlmaster_posting">
              <h2>
                <a href="{{ permalink($query) }}" rel="bookmark" title="{{ $item['title'] }}">
                  <strong>{{ $item['title'] }}</strong>
                </a>
              </h2>
           </div>
           <div class="master_postinginfo" style="display:none;" itemprop="description">
             <strong>{{ $item['title'] }}</strong>
           </div>
           <meta
           itemprop="image thumbnail" content="{{ $item['url'] }}"/>
           <div style="display:none;">
              <div itemprop="aggregateRating" itemscope itemtype="schema.org/AggregateRating">
                <span itemprop="ratingValue">{{ rand(3,5) }}</span>
                <span itemprop="ratingCount">({{ rand(1000,9999) }} votes)</span>
                <meta itemprop="author" content="{{ sitename() }}"/>
                <meta itemprop="datePublished" content="{{ date('d/m/Y') }}">
              </div>
           </div>
        </div>
     </div>
   @endforeach

   <br/>
   <div class="clear"></div>


   <div class ="page_navigation">
      <div class="wp-pagenavi">

        @php
      	  $pagination = range(1,20)
      	@endphp

        @foreach($pagination as $n => $page)
           @if( input()->get('page') == $n+1 )
               <strong class='current'>{{ $page }}</strong>
           @elseif( $page == end($pagination) )
               <a class="page" href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a>
           @else
               <a class="page" href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a>
           @endif
        @endforeach

      </div>
   </div>


</div>

@endsection
