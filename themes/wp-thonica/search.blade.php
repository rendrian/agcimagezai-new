@extends('layout')

@section('content')

<div id="master_content">

  <!-- breadcrumb -->
  @include('breadcrumb')

   <!-- image single -->
   <div itemscope="" itemtype="schema.org/ImageObject">
      <div class="clear"></div>
      <div class="master_single">
         <h1 itemprop="name">{{ $query }}</h1>

         

         <meta itemprop="representativeOfPage" content="true">

         <div style="display:none;">
           <img src="{{ $results[0]['url'] }}" itemprop="image thumbnail">
         </div>

         <center>
           <a href="#">
             <img style="width:100%" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" data-src="{{ $results[0]['url'] }}" src="{{ $results[0]['small'] }}" class="attachment-large size-large" alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}">
           </a>
         </center>

         <center>
            <h2>{{ $results[0]['title'] }}</h2>
         </center>

         {!! ads('responsive') !!}


         <div itemprop="description">
            <p>{{ random_strings($results, 10, ', ', 'ucwords', true ) }}</p>
         </div>

         <div class="master_sosmed">
            <center>
              <a class="twitter" href="http://twitter.com/home?status=Reading: {{ get_permalink() }}" title="Share this post on Twitter!" target="_blank">Twitter</a>
              <a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u={{ get_permalink() }}&amp;t={{ $query }}" title="Share this post on Facebook!">Facebook</a>
              <a class="pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url={{ get_permalink() }}&amp;media=&amp;description={{ $query }}" title="Share this post on Pinterest!">Pinterest</a>
              <a class="google" target="_blank" href="https://plus.google.com/share?url={{ get_permalink() }}" title="Share this post on Google!">Google+</a></center>
         </div>

         <div style="display:none;">
            <div itemprop="aggregateRating" itemscope itemtype="schema.org/AggregateRating">
              <span itemprop="ratingValue">{{ rand(3,5) }}</span>
              <span itemprop="ratingCount">({{ rand(1000,9999) }} votes)</span>
              <meta itemprop="author" content="{{ sitename() }}"/>
              <meta itemprop="datePublished" content="{{ date('d/m/Y') }}">
            </div>
         </div>
         <br>
         <div style="clear"></div>
      </div>
   </div>
   <div style="clear"></div>
   <br>
   <div class="master_title">
      <h3>Gallery of {{ $query }}</h3>
   </div>
   <center>
     <br>
     @foreach($results as $key => $item)

     <a itemprop="significantLinks" href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}" rel="bookmark">
       <img width="110" height="110" class="attachment-thumbnail size-thumbnail" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
     </a>

     @endforeach
   </center>

   <div style="clear:both;margin-bottom:20px;"></div>

   <div class="clear"></div>

   <nav>
     <span class="nav-previous1">
       <a href="{{ permalink( $random_terms[0] ) }}" rel="prev"><span class="meta-nav"><span class=" icon-double-angle-left"></span></span>«« {{ $random_terms[0] }}</a>
     </span>
     <span class="nav-next1">
       <a href="{{ permalink( end($random_terms) ) }}" rel="next">
         {{ end($random_terms) }} »» <span class="meta-nav"><span class=" icon-double-angle-right"></span></span>
       </a>
     </span>
   </nav>

   <br>

   <div class="clear"></div>

</div>

@endsection
