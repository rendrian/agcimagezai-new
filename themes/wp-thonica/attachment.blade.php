@extends('layout')

@section('content')

<div id="master_content">

  <!-- breadcrumb -->
  @include('breadcrumb')

   <!-- image single -->
   <div itemscope="" itemtype="schema.org/ImageObject">
      <div class="clear"></div>
      <div class="master_single">
         <h1 itemprop="name">{{ $subquery }}</h1>

         

         <meta itemprop="representativeOfPage" content="true">

         <div style="display:none;">
           <img src="{{ $gallery['current']['url'] }}" itemprop="image thumbnail">
         </div>

         <center>
           <a href="#">
             <img style="width:100%" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" data-src="{{ $gallery['current']['url'] }}" src="{{ $gallery['current']['small'] }}" class="attachment-large size-large" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}">
           </a>
         </center>

				 <div class="clear"></div>

				 <nav class="nav-single-top">
					 <span class="nav-previous">
						 <a href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}"> « Previous Image</a></span>
					 <span class="nav-next">
						 <a href="{{ attachment_url( $query, $gallery['next']['title'] ) }}">Next Image » </a></span>
				 </nav>


        <h2>{{ $gallery['current']['title'] }} Description : </h2>


<div itemprop="description">
	<p>
		<strong>{{ $subquery }}</strong> images that posted in this website was uploaded by {{ sitename() }}. <u>{{ $subquery }}</u> equipped with a <strong>HD resolution</strong> {{ $gallery['current']['size'] }} Pixel.You can save <strong>{{ $subquery }}</strong> for free to your devices.</p>

	<p>If you want to Save <em>{{ $subquery }}</em> with original size you can click the <a target="_blank" href="{{ $gallery['current']['url'] }}" download="{{ $gallery['current']['title'] }}" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}">
		<strong style="color:#12a6ce;">Download</strong>
	</a> link.</p></div>


         {!! ads('responsive') !!}


         <div itemprop="description">
            <p>{{ random_strings($results, 10, ', ', 'ucwords', true ) }}</p>
         </div>

         <div class="master_sosmed">
            <center>
              <a class="twitter" href="http://twitter.com/home?status=Reading: {{ get_permalink() }}" title="Share this post on Twitter!" target="_blank">Twitter</a>
              <a class="facebook" target="_blank" href="http://www.facebook.com/sharer.php?u={{ get_permalink() }}&amp;t={{ $query }}" title="Share this post on Facebook!">Facebook</a>
              <a class="pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url={{ get_permalink() }}&amp;media=&amp;description={{ $query }}" title="Share this post on Pinterest!">Pinterest</a>
              <a class="google" target="_blank" href="https://plus.google.com/share?url={{ get_permalink() }}" title="Share this post on Google!">Google+</a></center>
         </div>

         <div style="display:none;">
            <div itemprop="aggregateRating" itemscope itemtype="schema.org/AggregateRating">
              <span itemprop="ratingValue">{{ rand(3,5) }}</span>
              <span itemprop="ratingCount">({{ rand(1000,9999) }} votes)</span>
              <meta itemprop="author" content="{{ sitename() }}"/>
              <meta itemprop="datePublished" content="{{ date('d/m/Y') }}">
            </div>
         </div>
         <br>
         <div style="clear"></div>
      </div>
   </div>
   <div style="clear"></div>
   <br>
   <div class="master_title">
      <h3>Gallery of {{ $query }}</h3>
   </div>
   <center>
     <br>
     @foreach($results as $key => $item)

     <a itemprop="significantLinks" href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}" rel="bookmark">
       <img width="110" height="110" class="attachment-thumbnail size-thumbnail" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
     </a>

     @endforeach
   </center>

   <div style="clear:both;margin-bottom:20px;"></div>

   <div class="clear"></div>

   <nav>
     <span class="nav-previous1">
       <a href="{{ permalink( $random_terms[0] ) }}" rel="prev"><span class="meta-nav"><span class=" icon-double-angle-left"></span></span>«« {{ $random_terms[0] }}</a>
     </span>
     <span class="nav-next1">
       <a href="{{ permalink( end($random_terms) ) }}" rel="next">
         {{ end($random_terms) }} »» <span class="meta-nav"><span class=" icon-double-angle-right"></span></span>
       </a>
     </span>
   </nav>

   <br>

   <div class="clear"></div>

</div>

@endsection
