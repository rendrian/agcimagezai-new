@extends('layout')

@section('title')
{{ $page_title }}
@endsection

@section('content')
<div class="dhome_page">
  <h1>{{ $page_title }}</h1>
@include('page/' . strtolower($page_title) )
</div>
@endsection
