<div id="master_sidebar">


   <div class="clear"></div>

   <div class="master_sidebarbox">
      <p class="master_sidebarname">Recent Posts</p>
      <div class="master_sidebarpopular">

        @foreach( $random_terms as $term )
           <div class="master_sidebarpop">
              <div class="master_sidebarinfo">
                 <a href="{{ permalink($term) }}" title="{{ $term }}">
                    <h3>{{ $term }}</h3>
                 </a>
              </div>
           </div>
         @endforeach

      </div>
   </div>
</div>
