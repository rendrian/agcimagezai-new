@extends('layout')

@section('content')

<div class="col-md-12">
	<h1>{{ $page_title }}</h1>
</div>

<div class="col-md-12">
@include('page/' . strtolower($page_title) )

</div>

@endsection
