<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Article" itemid="{{ home_url() }}">
	<head>
    @include('header')
<style>
/* RESET */
html,body,body div,span,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,address,cite,code,del,dfn,em,img,ins,kbd,q,samp,small,strong,sub,sup,var,b,i,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,figure,footer,header,hgroup,menu,nav,section,time,mark,audio,video,hr{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent;}article,aside,figure,footer,header,hgroup,nav,section{display:block;}img,object,embed{max-width:100%;}html{overflow-y:scroll;}ul{list-style:none;}blockquote,q{quotes:none;}blockquote:before,blockquote:after,q:before,q:after{content:"";content:none;}a{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent;}del{text-decoration:line-through;}abbr[title],dfn[title]{border-bottom:1px dotted #000;cursor:help;}table{border-collapse:collapse;border-spacing:0;}th{font-weight:bold;vertical-align:bottom;}td{font-weight:normal;vertical-align:top;}input,select{vertical-align:middle;}pre{white-space:pre;white-space:pre-wrap;white-space:pre-line;word-wrap:break-word;}input[type="radio"]{vertical-align:text-bottom;}input[type="checkbox"]{vertical-align:bottom;*vertical-align:baseline;}.ie6 input{vertical-align:text-bottom;}select,input,textarea{font:99% sans-serif;}table{font-size:inherit;font:100%;}a:hover,a:active{outline:none;}small{font-size:85%;}strong,th{font-weight:bold;}td,td img{vertical-align:top;}sub,sup{font-size:75%;line-height:0;position:relative;}sup{top:-0.5em;}sub{bottom:-0.25em;}pre,code,kbd,samp{font-family:monospace,sans-serif;}.clk,label,input[type=button],input[type=submit],button{cursor:pointer;}button,input,select,textarea{margin:0;}button{width:auto;overflow:visible;}.ie7 img{-ms-interpolation-mode:bicubic;}.ie6 html{filter:expression(document.execCommand("BackgroundImageCache",false,true));}.c:before,.c:a  fter{content:"\0020";display:block;height:0;overflow:hidden;}.c:after{clear:both;}.c{zoom:1;}
/* GLOBAL */
body {font-family: 'verdana', sans-serif; font-weight:normal; font-size:14px; line-height:21px; color:#333;}
img {background:#fff center center no-repeat;}
a img a:hover {color:#8d1455;}
/* WHITESPACES */
.c,.btst,hr{clear:both;}
#w,.w {width:835px; margin:0 auto;}
h1,h2,h3,h4,h5,h6,p,hr,.g,.mb15,#w,#s ul,.bc,.di , ol, ul,.btst,.ms{margin-bottom:15px;}
.ri,.di,.mr15, ol,du {margin-right:15px;}
.mi{float:left}
.jh,.ts{overflow: hidden;}
.mi,.ms,.w{text-align: center;}
/*FONT*/
#c .jh{font-size: 14px;line-height: 21px}
.ts{font-size: 12px;line-height: 21px}
#l{text-align: left}
/*HOME*/
.mi,.mi img{width: 185px;}
.mi img{height: 130px;}
.mi{height: 160px;margin:0 15px 15px 0;background:#eee;padding:5px;border-radius:3px}
.jh,.ts{height: 20px}
/*SINGLE*/
.ms,.ms img{width:615px;}
.ms img{height: 450px}
.ms{height: 470px;background: #eee;padding-bottom:10px}
#rsd{width: 645px}
/*ATTACHMENT*/
#rad{width:820px}
#rad .di{width:258px}
/*FOOTER*/
a{text-decoration: none}
/*SIDEBAR*/
#s{margin-right: 15px}
/* LINKS & COLORS */ /* hitam: 222 - 222 */
/*h2 a,.jh{color:#8B1818;}*/
h2 a:hover {color:#8d1455;}
/*a, a img, */#se .gs-title, #se .gs-title b, .di:hover .hd {color:#d3305f; text-decoration:none;}
a:hover,a:focus {color:#390;}
.bc,.bc a {color:#8B1818;}
#s a {color:#222;}
#s a:hover {color:#390;}
#f a:hover {color:#390;}
#f a,#l,#l a {color:#fff;}
.ftt,.ai,.ptg {color:#f2f2f2;}
#s {background:#fff;}
.rd {color:#990}
.gr {color:#ff0000}
#a {background:#fff;}
.bw {background:#fff;}
.bgy {background:#eee;}
.tgh a {color:#222; margin:0 1px 0 0;}
.tghh {font-size:12px; line-height:20px;}
.tghh a {color:#222;}
.tghh a:hover {color:#390;}
.btgh {background:rgba(242, 242, 242, 0.8); padding:15px 15px 5px 15px;}
.futags{background:rgba(242, 242, 242, 0.8); padding:15px 15px 0 15px; float:left; margin-bottom:15px; width:775px;}
.jn a {color:#A22929; margin-right:5px;}
.jn123 {color:#222; margin-right:5px;}
.jn {background:rgba(242,242,242,0.5); padding:5px;}
.fifuhome {color:#222;}
strong,b,em,i,u{font-weight:normal;font-style:normal;text-decoration:none} /*membuat strong italic underline tak terlihat */
/* ALIGNMENT */
.r,#c,#sf ,.ct1{float:right; _display:inline;}
.l,#s,.mi a,.li img, .li a, .g img, .g a , #l, #l a, .di,.ri,.ct1 a {float:left; _display:inline;}
.oh,#h,#sf,.di,.ct1,.g a {overflow:hidden;}
.ct1 {text-indent:-9000px;}
.pa,.reply a,.ep,.di a,.sp,.jh a, #l a{position:absolute;}
.pr,.cl li,#c,.di,#pp .hd,.dsa,.jh, #l{position:relative;}
.dsa {width:728px; height:90px; margin:0 auto;}
.ads160 {height:600px;width:160px:}
.gr {margin-top:15px;}
.w3k, #s li, li {margin-left:15px;}
.nrt {float:left; width:300px;}
.mb10 {margin-bottom:10px;}
/*.mb1 {margin-bottom:1px;}*/
#s .children {margin-bottom:0;}
.p15o {padding:15px 0;}
#s,#cw li,#a {padding:15px;}
#c{width:630px;}
.li img {width:805px;}
.w3 {width:300px; height:250px;}
.ac,.dsa,.btst,.ads160{text-align:center;} 
.p15 {padding:15px;}
.bc {padding:5px;}
/* FONTS */
h1,h2,h3,h4,h5,h6,.st,#se .gs-title, #se .gs-title b,.fda,.glo,.hd {font-family: 'Roboto Slab', serif; font-weight:normal;}
h1,h2,.ep{font-size:24px; line-height:24px;}
h3,h4,h5,h6 {font-size:18px; line-height:22px;}
.fda,.glo {font-size:16px; line-height:20px;}
.st {font-size:16px; line-height:16px; color:#fff;}
#se b {font-weight:normal;}
#se .gsc-adBlock {left:-9000px; z-index:-1;} /* <<---- ini untuk menyembunyikan iklan google hasil search result */
.bc {font-size:11px; line-height:11px;}
/* CSS3 */
a,.di,.hd,img,.athb {transition: all .3s ease-in-out; -moz-transition: all .3s ease-in-out; -webkit-transition: all .3s ease-in-out; -o-transition: all .3s ease-in-out;}
img,.r3,.st,#sf,#w,.futags {border-radius:2px; -moz-border-radius:2px; -webkit-border-radius:2px; -khtml-border-radius:2px;}
/*#c,#s {box-shadow:1px 1px 1px #333;}*/ /*jangan pakai warna hitam*/ /*gunakan warna diatas #999*/
.st {text-shadow:0 1px 1px #222;}
#s a,.bc {text-shadow:0 1px 1px #ffffff;}
.athb{float:left}
.athb a {float: left;}
.btst,.ct1{cursor: pointer;}
.btst {background: none repeat scroll 0 0 #730B0B; border-radius: 2px;color: #FFFFFF;line-height: 12px;padding: 7px;}
.lista {color: #666666;height:auto;overflow: hidden;}
/* HEADER */
#h,.g a{background:#730B0B;;}
#h { border-bottom: 2px solid #; box-shadow: 0 3px 10px rgba(0, 0, 0, 0.2);}
#h,#l,#l a {height:38px;}
#l{margin-top:5px;font-size:25px}
#l,#l a {width:560px;}
#l a:hover,#l a:focus {opacity:1.0;}
.mi:hover img,.g a:hover img{opacity:0.8}
/* SEARCH FORM */
#sf {margin-top:5px; border:1px rgba(255,255,255,0.2) solid;}
#st {background:rgba(0,0,0,0.3);}
#st{width:200px; height:20px; border:0 rgba(0,0,0,0.1) solid;}
#ss {background:rgba(242,242,242,0.2); padding:5px; border:0 none; color:#f2f2f2;}
#st {padding:2px 3px 3px 5px; color:#e5e5e5;}
/* CONTENT */
#w {margin-top:15px; padding-top:15px; box-shadow:0 0 2px rgba(51,51,51,0.2); background:#fff;}
#a {margin:15px 0 0; position:relative;}
.g {width:630px;}
.g img{width:98px; height:75px; }
.gs a,.g a {margin:0 3px 3px 0;}
.li img  {height:auto;}
.ga {width:820px;}
.rg {width:110px;}
.rg,.ry {height:22px;}
.ep {top:15px; right:15px;}
#ras .di {width:253px;}
#rss .di {width:292px;}
#rsd .di {width:300px;}
/* CSE */
#se .gs-visibleUrl-short,.gsc-branding {display:none;}
#se .gs-visibleUrl-long {display:block;}
#se .gsc-branding img,.gcsc-branding img {background-image:none;}
.bc {background:rgba(159,152,152,0.1);}
/* COMMENTS */
hr{height:5px;border:0 none;}
#cw ol {list-style:none;}
.odd,hr {background:#eee;}
.even {background:#ddd;}
.reply a {top:15px; right:15px;}
.di{width:250px;height:70px;background:rgba(238, 238, 238, 0.45);}
.di a,.jh a,#l a{width:100%;height:100%;top:0;left:0;z-index:2;}
#pp, #ras, #rad, #rss, #rsd, .hd {font-size:15px;line-height:18px;z-index:1;}
.sp{width:68px;height:5px;top:-7px;right:1px;}
/* SIDEBAR */
#s {width:160px;background:rgba(0,0,0,0.03);}
#s li {list-style-type:circle; margin-left:15px; color:#d3305f;} /*disc,circle,square*/
.st {background:#8B1818; padding:5px 15px; text-align:center;}
#w .rg{width:98px;height:98px;}
/* FOOTER */
#f {background:#280000;}
.w {padding:15px auto;}
.ax{position:absolute;width:1px;height:1px;overflow:hidden;top:-100px;left:-9999px;}
.left { float:left; }
.right { float:right; }
.popu {width:395px;}
.il {width:185px;}
.h2t {color:#390; border:0 none; text-decoration:none; font-size:14px; line-height:18px;}
.h2t:hover,.h2t:focus {color:#d1452c;}
.h2t a {width:100%; height:100%; top:0; left:0;}
.h2t a {float:left;}
.h2t  {position:relative;}
.h2t a {position:absolute;}
.h2t { transition: all 0.3s ease-out 0s; -moz-transition: all 0.3s ease-out 0s; -webkit-transition: all 0.3s ease-out 0s; -o-transition: all 0.3s ease-out 0s; }
.grs {height:1px; font-size:100%; background:#f2f2f2;}
.h3 {font-size:16px; line-height:20px; font-family: 'Ruda', sans-serif; font-weight:normal;}
h5 {font-size:16px; font-family: 'Ruda', sans-serif; font-weight:normal;}
}
</style>
		</head>
		<body>
		



			<div id="h">
				<div class="w">
					<h1 id="l">
						<a href="{{ home_url() }}" title="{{  sitename() }} : {{ config('site.description') }}">{{  sitename() }}</a>
					</h1>
					<form id="sf" action="{{ home_url() }}" method="post">
						<input id="st" type="text" name="s" placeholder="Search here&#8230;" required="required" value="{{ isset($query) ? $query : "" }}"/>
						<input id="ss" type="submit" name="submit" value=" Search "/>
					</form>
				</div>
			</div>
			<div id="w">
				<div class="ac dsa mb15">
          {!! ads('responsive') !!}
				</div> 
        @yield('content')
			</div>
			<div id="f">
				<div class="w p15o">
					<p class="ptg">
            <a rel="nofollow" href="{{ home_url() }}">Home</a> |
            <a rel="nofollow" href="{{ home_url('page/disclaimer') }}">Disclaimer</a> |
            <a rel="nofollow" href="{{ home_url('page/privacy') }}">Privacy Policy</a> |
            <a rel="nofollow" href="{{ home_url('page/contact') }}">Contact</a> |
            <a rel="nofollow" href="{{ home_url('page/copyright') }}">Copyright</a> |			
            <a href="{{ home_url('sitemap-image.xml') }}">Sitemap</a>
					</p>
					<blockquote>
						<p class="ftt">All graphics and other visual elements as well as any sign reproduced on the display products reproduced on the Website belong to their respective owners and users and is provided AS IS for your personal information only. Copyright &#169; {{ date('Y') }} {{ sitename() }}</p>
					</blockquote>
					<div class="ai">
	Any content, trademark/s, or other material that might be foud on this site that is not this site property remains the copyright of its respective owner/s.
					</div>
				</div>
			</div>

@include('footer')
		</body>
	</html>
