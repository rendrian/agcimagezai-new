@extends('layout')

@section('content')

<div id="a">
	<h1>{{ $subquery }}</h1>

@include('breadcrumb')

	<div class="li mb15">
	<a href="{{ attachment_url( $query, $gallery['current']['title'] ) }}" title="{{ $gallery['current']['title'] }}">
	<img src="{{ $gallery['current']['small'] }}" data-src="{{ $gallery['current']['url'] }}" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}">
		<noscript>&lt;img src="{{ $gallery['current']['url'] }}" alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}"/&gt;</noscript>
	</a>
	<div class="c"></div>
	</div>
	<div class="ac l dsa mb15 mr15">
	{!! ads('responsive') !!}
	</div>


	<a class="bc left" href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}">Prev</a>

	<a class="bc right" href="{{ attachment_url( $query, $gallery['next']['title'] ) }}">Next</a>


<div class="c"></div>

	<h4 class="glo">Gallery of {{ $query }}</h4>
	<div>
	<div class="g ga athb">

  @foreach( $results as $i => $item )

          <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}">

            <img width="103" height="70" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" alt="{{ $item['title'] }}" title="{{ $item['title'] }}"/>

          <noscript>&lt;img src="{{ $item['url'] }}" title="{{ $item['title'] }}" alt="{{ $item['title'] }}"/&gt;&lt;img src="{{ $item['url'] }}" title="{{ $item['title'] }}" alt="{{ $item['title'] }}" width="933" height="784"/&gt;&lt;img src="{{ $item['url'] }}" title="{{ $item['title'] }}" alt="{{ $item['title'] }}"/&gt;</noscript>
        </a>

  @endforeach
		<div class="c"></div>
	</div>
	</div>
	<div class="c mb15"></div>
	<div class=""></div>
	<div class="c"></div>
	<hr>
	<div class="ac dsa mb15">
		{!! ads('responsive') !!}
	</div>
	</div>

@endsection
