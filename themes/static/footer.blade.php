<center><span id="pageload"></span></center>

@if( config('tracking.histats') )
		<div id="histats_counter"></div>
		<script type="text/javascript">var _Hasync= _Hasync|| [];
		_Hasync.push(['Histats.startgif', '1,{{ config('tracking.histats') }},4,10047,"div#histatsC {position: absolute;top:0px;left:0px;}body>div#histatsC {position: fixed;}"']);
		_Hasync.push(['Histats.fasi', '1']);
		_Hasync.push(['Histats.track_hits', '']);
		(function() {
		var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
		hs.src = ('//s10.histats.com/js15_gif_as.js');
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
		})();
		</script>
		<noscript>
			<a href="/" alt="hitcounter" target="_blank" >
				<div id="histatsC">
					<img alt="histats" src="http://s4is.histats.com/stats/i/{{ config('tracking.histats') }}.gif?{{ config('tracking.histats') }}&103">
				</div>
			</a>
		</noscript>
@endif

@if( config('tracking.google_analytic') )
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '{{ config('tracking.google_analytic') }}', 'auto');
		ga('send', 'pageview');
	</script>
@endif

@if( config('tracking.statcounter.sc_project') )
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project={{ config('tracking.statcounter.sc_project') }};
var sc_invisible=1;
var sc_security="{{ config('tracking.statcounter.sc_security') }}";
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript>
	<div class="statcounter">
		<a title="web stats"
	href="http://statcounter.com/" target="_blank">
	<img class="statcounter" src="//c.statcounter.com/{{ config('tracking.statcounter.sc_project') }}/0/{{ config('tracking.statcounter.sc_security') }}/1/" alt="webstats"></a>
	</div>
</noscript>
<!-- End of StatCounter Code for Default Guide -->
@endif

<script type="text/javascript">

	var afterload = (new Date()).getTime();
	seconds = (afterload-beforeload) / 1000;
	document.getElementById("pageload").innerHTML = 'Page load time :  ' + seconds + ' sec(s)';

function init() {
	var imgDefer = document.getElementsByTagName('img');
	for (var i=0; i<imgDefer.length; i++) {
		if(imgDefer[i].getAttribute('data-src')) {
			imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
		}
	}



}
window.onload = init;

</script>

<?php
	if( config('options.pinger.active') )
	{
			if( in_array(date('i'), config('options.pinger.every') ) )
			{
				$sitemap = home_url('sitemap.xml');
				$url = "http://www.google.com/webmasters/sitemaps/ping?sitemap=".$sitemap;
				pinger($url);
				$url = "http://www.bing.com/webmaster/ping.aspx?siteMap=".$sitemap;
				pinger($url);
			}
	}
?>
