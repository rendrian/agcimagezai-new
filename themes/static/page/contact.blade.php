<p>Have any question, comment, suggestion or news tip to pass along to the website ?</p>
<p>We are open to discuss all of the possibilities with you. This page offering the right way to sent any comments to website admin related to your feedback, news coverage and other issues related to this site.</p>
<p>We are happy to hear information from you please write a subject format:</p>
<p>
1. Claim Picture [picture name] [url to real picture] : if you are the real owner to claim your picture and need back links.</br>
2. Submit Wallpapers [wallpaper name] : if you wanna submit your or your wallpaper design to us.</br>
3. Advertise : if you interested to advertising on our site.</br>
4. Support : if you need our support.</br>
5. And send all your inquiries to our official mail at <strong>{{ config('agc.email') }}</strong></br>
</p>
<p>Don’t hesitate to contact us according your concerns and don’t worry, all of your comment are welcome and should be replied by us.</p>
<p>Thank you.</p>
<br><br>
