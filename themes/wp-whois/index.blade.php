@extends('layout')

@section('content')

<div class="ktz-titlepage">
		<h1 class="entry-title clearfix">{{ config('site.title') }} - {{ sitename() }}</h1>
</div>


		
		<div class="entry-content ktz-wrap-content-single clearfix ktz-post">
							
			<div class="row">
			
			<style>
			

			.listku {
				list-style : none;
				margin: 3px auto 0;
				padding: 5px 0 5px 10px;
				color: #aaa;
				font-size: 12px;
				position: relative;
				background: #ecf0f1;
			}
			.truncate {
			  width: 100%;
			  white-space: nowrap;
			  overflow: hidden;
			  text-overflow: ellipsis;
			}
			</style>
			
			@foreach( $random_terms as $term )
				<div class="col-md-4 wrapwidget">
				
					<li class="listku truncate"><a href="{{ permalink( $term ) }}" title="{{ ucwords( $term ) }}" rel="bookmark">{{ ucwords( $term ) }}</a></li>
					
				</div>
			@endforeach
				
				
			</div>
		
		
		
		
</div>		
		
		
		
		
		


@endsection
