	<div class="sbar col-md-3 widget-area wrapwidget" role="complementary">

		<aside id="text-2" class="widget widget_text">
		<h4 class="widget-title"><span class="ktz-blocktitle">Advertisement</span>
		</h4>
		
		<div class="textwidget">
{!! ads('responsive') !!}
</div>
		</aside>
		
@if( $path !== 'index')				
	<aside id="ktz-popular-posts-2" class="widget ktz_popular_post clearfix">
	
	<h4 class="widget-title">
	<span class="ktz-blocktitle">Popular Posts</span>
	</h4>
	
	<ul class="ktz-widgetcolor ktz_widget_default">
	
	@foreach( $random_terms as $term )
		<li><a href="{{ permalink( $term ) }}" title="{{ ucwords( $term ) }}" rel="bookmark">{{ ucwords( $term ) }}</a></li>
	@endforeach
	</ul>
	</aside>
@endif		
		




		</div>