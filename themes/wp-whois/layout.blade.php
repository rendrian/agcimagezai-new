<!DOCTYPE html>
<!--[if IE 7]><html class="ie7 no-js"  lang="en-US" xmlns:og="//opengraphprotocol.org/schema/" xmlns:fb="//www.facebook.com/2008/fbml"<![endif]-->
<!--[if lte IE 8]><html class="ie8 no-js"  lang="en-US" xmlns:og="//opengraphprotocol.org/schema/" xmlns:fb="//www.facebook.com/2008/fbml"<![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="not-ie no-js" lang="en-US" xmlns:og="//opengraphprotocol.org/schema/" xmlns:fb="//www.facebook.com/2008/fbml"><!--<![endif]-->

<head>

	<meta charset="UTF-8" />

	<!-- Meta responsive compatible mode on IE and chrome, and zooming 1 by kentooz themes -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- mobile optimized meta by kentooz themes -->
	<meta name="HandheldFriendly" content="True" />
	<meta name="MobileOptimized" content="320" />

	@include('header')




@include('css')




</head>
<body class="post-template-default single single-post postid-8209 single-format-standard kentooz" id="top">
	<div class="ktz-allwrap">	<header class="ktz-mainheader">
	<div class="header-wrap">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-6">
					<div class="ktz-logo">
						<a href="{{ home_url() }}">
							<span style="font-weight:bold;font-size:120%">{{ strtoupper( sitename() ) }} | {{ config('site.description') }}</span>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
	</header>



	<div class="container">
		<nav class="ktz-mainmenu clearfix">
			<ul id="topmenu" class="sf-menu">

			<li id="menu-item-41" class="menu-item">
				<a href="{{ home_url() }}">Home</a>
			</li>
			<li id="menu-item-41" class="menu-item">
				<a href="{{ home_url('sitemap.xml') }}">Sitemap</a>
			</li>
			@foreach( config('themes.page') as $page )
			<li id="menu-item-41" class="menu-item">
				<a href="{{ permalink($page, 'page')  }}">{{ ucwords($page) }}</a>
			</li>
			@endforeach
</ul>
</nav>
	</div>
	<nav class="ktz-mobilemenu clearfix"></nav>
	<div class="container">

	@include('breadcrumb')

	</div>

	<div class="ktz-inner-content">
		<div class="container">

			<div class="row">
				<section class="col-md-12">
	<div class="row">
			<div role="main" class="main col-md-9">
		<section class="new-content">
					<article id="post-8209" class="ktz-single post-8209 post">
		<div class="ktz-single-box">
	<div class="entry-body">

<div class="ktz-bannersingletop">
{!! ads('responsive') !!}
</div>



@yield('content')


	</div>
	</div>

</article><!-- #post-8209 -->
					</section>

				</div>


@include('sidebar')


	</div>
	</section>
	</div> <!-- .row on head -->
	</div> <!-- .container on head -->
	</div> <!-- .ktz-inner-content head -->
	<footer class="footer">

		<div class="copyright">
	<nav class="ktz-footermenu">
		<div class="container">
					</div>
	</nav>
		<div class="container">
				<div class="footercredits pull-left">&copy; 2017 <a href="{{ home_url() }}" title="{{ config('site.description') }}">{{ sitename() }}</a></div>						</div>
	</div>
	</footer>
	</div> <!-- .all-wrapper on head -->

	@include('footer')

</body>
</html>
