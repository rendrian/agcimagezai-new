@extends('layout')

@section('content')

      <div id="header" class="w pr mb18">
         <div class="w1 m0 pr">
            <h2 class="mb0"><a href="/" style="color:#ff4f00">{{ sitename() }}: {{ $query }}.</a></h2>
         </div>
      </div>
      <div class="w1 m0 cl">
         <div class="tc mb18 h9 ad">
		 {!! ads('responsive') !!}
         </div>



<div class="r w4 pr attachment-15182 " itemprop="articleBody">
   <h1 itemprop="headline">{{ $subquery }} - {{ $query }}</h1>
	<div class="bc mb18" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	   <a href="/" title=" Back to Home " class="l" itemprop="url" ><i itemprop="title"> Home </i></a><i class="l"> &rsaquo; </i><i itemprop="child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	   <a href="{{ permalink($query) }}" itemprop="url" class="l" ><i itemprop="title">{{ $query }}</i></a></i><i class="l"> &rsaquo; </i><i itemprop="child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	   <a href="{{ get_permalink() }}" itemprop="url" class="l curn" ><i itemprop="title">{{ $subquery }}</i></a></i><i class="l"> &rsaquo; </i><i itemprop="child" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	   <a href="#" itemprop="url" class="l curn" ><i itemprop="title">Currrently Reading</i></a></i>
	   <div class="c"></div>
	</div>


   <div class="ai mb18 pr lh0 bw bs" itemid="{{ $gallery['current']['url'] }}" itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">

   <img class="mb0" onerror="this.onerror=null;this.src='{{ $gallery['current']['small'] }}';" data-src="{{ $gallery['current']['url'] }}" src="{{ $gallery['current']['small'] }}" style="width:100%" height="620" alt="{{ $gallery['current']['title'] }}" itemprop="contentURL">
   <a class="imp l pa" href="{{ attachment_url( $query, $gallery['prev']['title'] ) }}"><i class="icon-left-open"></i></a>
   <a class="imn r pa" href="{{ attachment_url( $query, $gallery['next']['title'] ) }}"><i class="icon-right-open"></i></a>

   </div>
   <div class="bw p18 bs info cl mb18">
		{!! ads('responsive') !!}
   </div>
   <div class="bw p18 bs info cl mb18">
      <h2>File Info: {{ $subquery }}</h2>
      <label>Filename</label><b>:</b><strong>{{ clean($subquery, '-') }}.jpg</strong>
      <label>Resolution</label><b>:</b>
      <p>{{ rand(1000,2000) }}x{{ rand(1000,2000) }} pixels.</p>
      <label>Size</label><b>:</b>
      <p>{{ rand(1000,2000) }} kb.</p>
      <label>Download</label><b>:</b>
      <p>
	  <a title="Full" target="_blank" href="{{ $gallery['current']['url'] }}">[Large]</a>&nbsp;
	  <a title="Large" href="{{ $gallery['current']['small'] }}">[Small]</a>
	  </p>
   </div>
</div>


<div class="w3">
	<div class="mb18">
	{!! ads('responsive') !!}
	</div>
<div class="p18 bw bs mb18">
	<h3>Recent Furniture Designs</h3>
	<ul class="ar">
	@foreach($random_terms as $term)
		<li>
			<a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a>
		</li>
	@endforeach
	</ul>
</div>
</div>






         <div class="w2 oh mb18 cl">

            <h2>{{ $subquery }} - {{ $query }}</h2>
            <div class="cl">

	@foreach($results as $key => $item)

	   <div class="post oh l mb18 bw bs pr id-15185" itemid="{{ $item['url'] }}" itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">
	   <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ $item['title'] }}">
			<img onerror="this.onerror=null;this.src='{{ $item['small'] }}';" data-src="{{ $item['url'] }}" src="{{ $item['small'] }}" alt="{{ $item['title'] }}" title="{{ $item['title'] }}" width="188" height="188" itemprop="contentURL">
	   </a>
	   <i class="pa ll">
		   <a target="_blank" title="{{ $item['title'] }}" href="{{ $item['small'] }}">S</a>
		   <a target="_blank" title="{{ $item['title'] }}" href="{{ $item['url'] }}">L</a>
	   </i>
	   </div>

   @endforeach

            </div>
         </div>



      </div>
@endsection
