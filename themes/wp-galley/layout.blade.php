<!DOCTYPE html>
<html>
   <head>

	@include('header')
	<link rel="stylesheet" href="{{ theme_url('css/style.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ theme_url('css/a.css') }}" type="text/css" />

   </head>

   <body>

	@yield('content')

      <div id="f" class="w bs">
         <div class="w1 m0 p36">
            <p class="mb18">
			<a href="#">About</a> /
			<a href="#">Privacy Policy</a> /
			<a href="#">DMCA</a> /
			<a href="#">Terms &amp; Conditions</a> /
			<a href="#">Contact Us</a> /
			<a href="{{ home_url('sitemap.xml') }}">Crawler</a> /
			<a href="{{ home_url('feed') }}">Feed</a> /
			<br>
			</p>
            <p>
			@foreach(range(1,100) as $item)
				<a href="{{ home_url() }}?page={{ $item }}">&bullet;</a>
			@endforeach
			</p>
         </div>
      </div>
	  @include('footer')
   </body>
</html>
