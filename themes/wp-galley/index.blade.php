@extends('layout')

@section('ads')
	{!! ads('responsive') !!}
@endsection

@section('content')

      <div id="header" class="w pr mb18">
         <div class="w1 m0 pr">
            <h2 class="mb0"><a href="{{ home_url() }}" style="color:#ff4f00">{{ sitename() }}: {{ config('site.description') }}.</a></h2>
         </div>
      </div>
      <div class="w1 m0 cl">
         <div class="tc mb18 h9 ad">
			@yield('ads')
         </div>


	<div class="w2 oh cl">
	<h3 class="tc">{{ config('site.description') }}</h3>

	@include('breadcrumb')

	@foreach($results as $key => $item)
		@if($key < 8)
			<div class="thumb oh l mb18 bw bs" itemid="{{ $item['url'] }}" itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">
			<a href="{{ attachment_url( $query, $item['title'] ) }}" class="mb0" title="{{ $item['title'] }}">
					<img width="188" height="188" itemprop="contentURL" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" data-src="{{ $item['url'] }}" src="{{ $item['small'] }}" alt="{{ $item['title'] }}" title="{{ $item['title'] }}">
			</a>
			<h2 class="ti" itemprop="headline">{{ limit_the_words($item['title'], 8) }}</h2>
			</div>
		@endif
	@endforeach

	</div>

<div class="tc mb18 bw h9 ad bs">
<div class="wp-pagenavi">

			@foreach(range(1,25) as $item)
				<a class="page larger" href="{{ home_url() }}?page={{ $item }}">{{ $item }}</a>
			@endforeach
</div>
</div>

	<div class="w2 oh cl">
	<h3 class="tc">{{ $results[0]['title'] }}</h3>

	@foreach($results as $key => $item)

					<div class="thumb oh l mb18 bw bs" itemid="{{ $item['url'] }}" itemtype="http://schema.org/ImageObject" itemscope="" itemprop="associatedMedia">
					<a href="{{ attachment_url( $query, $item['title'] ) }}" class="mb0" title="{{ $item['title'] }}">
							<img width="188" height="188" itemprop="contentURL" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" data-src="{{ $item['url'] }}" src="{{ $item['small'] }}" alt="{{ $item['title'] }}" title="{{ $item['title'] }}">
					</a>
					<h2 class="ti" itemprop="headline">{{ limit_the_words($item['title'], 8) }}</h2>
					</div>

	@endforeach

	</div>


         <div class="tc mb18 h9 ad">
			@yield('ads')
         </div>

		 <div class="cl w2 oh">
		 <h2>{{ config('site.description') }}</h2>


		 @for($i = 0; $i < 20; $i++)
			<a href="{{ permalink( $random_terms[$i] ) }}" title="{{ $random_terms[$i] }}">{{ $random_terms[$i] }} &bullet; </a>
		 @endfor
		 </div>

		 <div class="cl w2 oh" style="position:absolute;left:-999999px;top:-999999px;">
		 <h2>{{ config('site.description') }}</h2>
		 @foreach($random_terms as $term)
			<a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a>
		 @endforeach
		 </div>

      </div>

@endsection
