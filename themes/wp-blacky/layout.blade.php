<!DOCTYPE html>
<html lang="en">
<head>
	@include('header')
<style type="text/css">
<?php
$color = array('#ff69b9','red','blue','green','yellow','orange', 'teal', 'white', '#ff6633','#ff9933','#ffcc33','#ffff33','#ccff33','#99ff33','#66ff33','#33ff33','#33ff66','#33ff99','#33ffcc','#33ffff','#33ccff','#3399ff','#3366ff','#3333ff','#6633ff','#9933ff','#cc33ff','#ff33ff','#ff33cc','#ff3399','#ff3366','#ff3333');
shuffle($color);
$color = $color[0];
?>    *{margin:0;padding:0}body{font-size:12px;background-color:#222;font-family:arial,verdana,georgia;color:<?= $color; ?>}a{color:<?= $color; ?>;text-decoration:none}h1,h2,h3,h4,h5,p{margin:10px 0}h1{font-size:22px}.class_cl,.class_clear{clear:both}img{border:none}#class_kp{width:100%;max-width:950px;margin:0 auto;padding:10px;box-sizing:border-box}.class_kr{font-size:20px;font-family:arial;font-weight:700;letter-spacing:1px;float:left}#class_kr a:hover{color:#faaa00}#class_cl{width:100%;max-width:750px;float:left;text-align:center;box-sizing:border-box}.class_ld{font-size:12px;text-transform:uppercase;font-family:arial;color:#616161;letter-spacing:1px;margin:2px 0;text-align:left}figure{margin:5px auto;background:#000;padding:5px;min-height:200px}figure h2{text-align:center;font-size:10px;text-transform:uppercase;font-family:arial;color:#333;letter-spacing:1px;margin:2px 0}figure img{width:100%;height:auto;cursor:pointer}.class_current{color:#DDD;background:#111;margin:0;padding:6px 8px}#class_sb{float:right;width:100%;max-width:170px}.class_hc{background:#1B1B1B;font-size:12px;text-transform:uppercase;font-family:arial;color:#616161;margin:0;padding:5px}.class_image-meta a,figure p{color:#ccc}#class_breadchumb{margin-bottom:10px}#class_breadchumb a:hover,.class_image-meta a:hover{color:#EB8D02;text-decoration:underline}.class_soc{margin-top:-105px;height:19px;position:absolute;margin-left:9px;width:620px;text-align:left}#class_cc{width:100%;max-width:970px;background:#222;margin:0 auto;border:10px solid #2B2B2B;padding:10px;box-sizing:border-box}.class_prevnext{font-weight:700;letter-spacing:1px;font-size:10px;text-transform:uppercase;width:99%;margin:0 auto;padding:5px 0}.class_prev{width:50%;float:left;text-align:left}.class_next{width:50%;float:right;text-align:right}#class_rel{text-align:center}#class_rel img{background:#000;margin:2px;padding:4px;width:90px;height:60px}.class_content h3{letter-spacing:1px;text-transform:uppercase;font-size:12px}#class_rel img:hover{background:#616161}.class_content{background-color:#222;padding:4px 8px}.class_crumbs{margin-top:-5px;margin-bottom:5px;margin-left:8px;text-transform:uppercase;letter-spacing:1px;font-size:9px;font-weight:700}.class_content h4{background-color:#1B1B1B;border-bottom:2px solid #111;text-align:left;letter-spacing:1px;margin:10px 0 0;padding:10px}.class_content p{line-height:20px;margin:5px 0;padding:0 3px;text-align:justify}.class_kn,.class_right{float:right}.class_crumbs a{color:#999}.class_content a:hover,.class_crent,.class_crumbs a:hover,p{color:#ccc}
.class_content li {background:#2B2B2B;float: left;height: 10px;list-style-type: none;margin-bottom: 9px;margin-right: 6px;overflow: hidden;padding: 6px 9px;text-transform: capitalize;width: 30%;} .class_content li a{color:#fff;}
.class_box{width:32%;display:block;float:left;text-align:center;background:#252525;height:150px;overflow:hidden;border-bottom:2px solid #111;margin:5px 2px;padding-top:5px}.class_box:hover{cursor:pointer;background:#000}.class_box h2,.class_box h3{font-size:11px;line-height:20px;margin:0;color:#666;font-weight:400}a.class_th{display:block;padding:5px;height:130px;overflow:hidden;margin:0 auto;box-sizing:border-box}a.class_th img{width:100%;height:auto;position:relative;top:50%;transform:translateY(-50%);-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%)}.class_fb_iframe_widget,.class_fb_iframe_widget span,.class_fb_iframe_widget span iframe[style]{min-width:100%!important;width:100%!important}.class_share-box{width:90%;max-width:718px;background:#f9f9f9;border-radius:3px;margin:5px auto;border:1px solid #ddd;padding:10px 5px}.class_share-box span{color:#666;width:85px;display:block;float:left;text-align:right}input.class_share-input{margin-left:10px;width:80%;max-width:650px}.class_ads-bottom,.class_ads-top{width:100%;max-width:728px;height:auto;margin:0 auto;overflow:hidden;box-sizing:border-box}.class_ads-right{width:100%;height:auto;padding:10px 0;overflow:hidden;box-sizing:border-box}ul.class_rand-text li{width:100%;box-sizing:border-box;list-style-type:none}ul.class_rand-text h3{text-transform:uppercase;font-size:10px;margin-bottom:0;height:12px;overflow:hidden}ul.class_rand-text p{color:#999;margin-top:0}.class_insearch{background:#111;border:none;font-weight:700;color:#fff;letter-spacing:1px;font-size:12px;float:right;margin:0 0 5px 10px;width:100%;padding:6px;box-sizing:border-box}.class_menu li{list-style:none;display:inline-block;margin-left:15px;text-transform:uppercase;font-weight:700;letter-spacing:1px;font-size:10px}.class_menu{margin-top:5px}.class_menu li a{color:#BBB}.class_menu li a:hover{color:#fff}.class_content ul{text-align:justify;color:#ccc;margin-left:14px}.class_bf{color:#CCC;text-align:center;background:rgb(51,51,51);font-size:11px;margin-top:20px;letter-spacing:2px;text-transform:uppercase;padding:15px}.class_bf a{color:rgb(199,199,199)}.class_bf a:hover{color: rgb(0, 211, 219)}img.class_wallpaper{min-height:300px}#class_pagination{width:100%;margin-top:15px;clear:both;text-align:center}#class_pagination a,#class_pagination span{padding:6px 8px;margin:1px;line-height:30px}#class_pagination a:hover{text-decoration:none;background:#000;color:#bbb}#class_pagination b{background:#000;color:#bbb;padding:6px 8px;margin:2px}
</style>

</head>
<body>

<div id="class_kp">
    <div class="class_kr">
        <a class="class_lo" href="/">@yield('title')</a>
    </div>
    <div class="class_kn">
        <ul class="class_menu">
					<li><a href="{{ home_url() }}" title="Home">Home</a> </li>
		@foreach( config('themes.page') as $page )
			<li><a href="{{ permalink( $page, 'page' ) }}" title="{{ $page }}">{{ ucwords($page) }}</a> </li>
		@endforeach
            <li><a target="_blank" href="{{ home_url('sitemap.xml') }}">crawler</a></li>
        </ul>
    </div>
    <div class="class_cl"></div>
</div>

<div id="class_cc">

	@include('breadcrumb')

    <div id="class_cl">
        <div class="class_content">

		@yield('content')

          </div>
              </div>
              <div id="class_sb">
                  <div class="class_ads-right">

				  {!! ads('responsive') !!}

                  </div>
                  <h3 class="class_hc">Random post :</h3>
                  <ul class="class_rand-text">

											@foreach ($random_terms as $index => $term)

												@if($index < 30)
													<li><h3><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></h3></li>
												@endif

											@endforeach

									</ul>

									<div class="class_ads-right">
                  </div>
              </div>
              <div class="class_cl"></div>
          </div>

<div class="class_bf">Copyright &copy; {{ date('Y') }}. Some Rights Reserved.</div>
@include('footer')
</body>
</html>
