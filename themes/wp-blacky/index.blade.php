@extends('layout')

@section('content')

<h1 class="class_ld">Welcome : {{ sitename() }} - {{ config('site.description') }}</h1>

@foreach( array_slice($results, 0, 12) as $key => $item)
      <div class="class_box">
        <a href="{{ attachment_url( $query, $item['title'] ) }}" class="th" title="{{ strtolower($query) }} {{ $item['title'] }}">
          <img class="class_th" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
        </a>
        <h2>{{ strtolower($query) }} - {{ $item['title'] }}</h2>
      </div>
@endforeach

<ul>
@foreach ($random_terms as $term)
<li><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></li>
@endforeach
</ul>

@endsection
