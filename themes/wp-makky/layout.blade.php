<!DOCTYPE html>
<html>
<head>
@include('header')
<link rel="stylesheet" type="text/css" href="{{ assets_url('css/bootstrap.css') }}">
<style>
body{
background-color: #353535;
}
.list-group li{
	list-style: none;
}
p {
    margin: 0 0 10px;
    color: white;
    text-align: justify;
}
a {
    color: white;
    text-decoration: none;
}
.list-group{
	
width: 750px;
	
float: left;
	
background: #353537;
}
.sidebar li {
    color: white;
}
.list-group ul{
	margin:0px;
}
.list-group h2{
	font-size: 10px;
	margin: 0px;
	color: white;
}
.list-group-item {
	padding: 8px;
	float: left;
	width: 245px;
	margin: 2px;
	background: #252525;
	border: 1px solid #353535;
}
.konten{
	width: 742px;
	float: left;
	padding: 6px;
	background: #353537;
}
.images{
	
width: 237px;
	
float:left;
	
margin: 3px;
	
height: 225px;
	
background: #252525;
}
.well{
	display: inline-block;
	width: 100%;
	background-color: #252525;
	border: 1px solid #252525;
}
.images img{
	width: 100%;
	padding: 12px;
	height: 170px;
}
.images h2{
	font-size: 10px;
	padding: 12px;
	padding-top: 0px;
	color: white;
	text-align: center;
}
.container{
	width: 1024px;
}
.breadcrumbs a {
    color: white;
}
.well h1{
	font-size: 18px;
	text-transform: uppercase;
	margin-top: 0px;
	font-weight: 700;
	margin-bottom: 0px;
	color: white;
}
.related{
	float:left;
	width: 100%;
}
.related h3{
	font-size: 15px;
	font-weight: 700;
	text-transform: uppercase;
	color: white;
	margin-top: 20px;
	border-bottom: 1px solid #dadada;
}
.breadcrumbs{
    text-align: center;
    background: #353535;
    color: white;
    margin-bottom: 10px;
}
.iklan{
	float:left;
	width: 100%;
	margin-top: 5px;
	margin-bottom: 5px;
}
.sidebar{
	float:right;
	width: 200px;
	padding: 5px;
	background-color: #353537;
}
.sidebar ul{

margin-left: -20px;
}
#sidebar{
	float:right;
	width: 200px;
	padding: 5px;
}
.sticky {
	position: fixed;
	width: 200px;
	padding: 5px;
}
.footer{
	
width: 100%;
	
background: #252525;
	
margin: auto;
	
text-align: center;
	
padding: 10px;
}
.list-group-item a{
	
}
.big_images img{
	width: 100%;
	height: 500px;
	background-color: #252525;
}
.big_images h2{
	font-size: 18px;
	margin-top: 5px;
	text-align: center;
	text-transform: uppercase;
	color: white;
	margin-bottom: 5px;
}
.big_images h1{
	margin-bottom: 5px;
}
.images_att {
    width: 237px;
    float: left;
    margin: 3px;
    height: 260px;
    background: #252525;
}
.images_att img{
	width: 100%;
	padding: 12px;
	height: 150px;
}
.images_att h2{
	font-size: 10px;
	padding: 12px;
	padding-top: 0px;
	color: white;
	text-align: center;
}
.images_att p{
	text-align: center;
}
.images_att a{
	
}
.search	{
	width: 100%;
	margin-bottom: 5px;
}
.iklan_tengah{
	
float: left;
	
width: 100%;
}

@media only screen and (max-width:500px){
	.container {
	    width: 100%;
	}
	.row {
	    margin-right: -15px;
	     margin-left: 0px; 
	}
	.images_att img {
	    width: 100%;
	    padding: 12px;
	    height: 195px;
	}
	.container {
	    padding-right: 0px; 
	    padding-left: 0px; 
	    margin-right: auto;
	    margin-left: auto;
	}
	.images_att {
	    width: 339px;
	    float: left;
	    margin: 3px;
	    height: 290px;
	    background: #252525;
	}
	
	.list-group {
	    width: 100%;
	    float: left;
	}
	.sidebar{
		float:right;
		width: 100%;
		padding: 5px;
	}
	.list-group-item {
	    padding: 8px;
	    float: left;
	    width: 100%;
	    margin: 2px;
	}
	.footer {
	    width: 100%;
	    background: #252525;
	    margin: auto;
	    text-align: center;
	    padding: 10px;
	}
	.well {
		display: block;
		width: 100%;
		float: left;
		padding: 0px;
		background-color: #252525;
		border: 1px solid #252525;
	}
	.konten {
	    width: 100%;
	    background: #353537;
	    float: left;
	    margin-top: 34px;
	}

	.images {
	    width: 339px;
	    float: left;
	    margin: 3px;
	    height: 250px;
	    background: #252525;
	}
}
</style>
</head>
<body>
<div class="container">
<div class="row">
<div class="well">
	
@yield('content')

<div class="sidebar">
<noscript><span>ads/responsive.txt</span></noscript>
	<p>Recent Post</p>
	<ul>
	
		@foreach ($random_terms as $index => $term)

			@if($index < 10)
				<li><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></li>
			@endif

		@endforeach
		
	</ul>
<noscript><span>ads/link.txt</span></noscript>
</div>
</div>
</div>
</div>
<div class="footer">
<p>Copyright &copy; {{ date('Y') }}</p>
<p>Any content, trademark/s, or other material that might be found on the {{ config('site.title') }}  website that is not Latest Map Us  property remains the copyright of its respective owner/s.
In no way does {{ config('site.title') }} claim ownership or responsibility for such items, and you should seek legal consent for any use of such materials from its owner.</p>
<p>
			<a href="{{ home_url() }}" title="Home">Home</a> | 
		@foreach( config('themes.page') as $page )
			<a href="{{ permalink( $page, 'page' ) }}" title="{{ $page }}">{{ ucwords($page) }}</a> | 
		@endforeach
            <a target="_blank" href="{{ home_url('sitemap.xml') }}">crawler</a>
</p>
</div>
<script type="text/javascript">
jQuery( document ).ready(function($) {
  $( 'img.image-item' ).click(function(){
    $( '.main-image' ).attr( 'src', $(this).prop('src') );
  });

  $( 'a.image-item' ).click(function(){
    $( '.main-image' ).attr( 'src', $(this).data('src') );
  });

});
</script>

@include('footer')

</body>
</html>