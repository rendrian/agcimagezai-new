<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
@include('header')

<link href="{{ theme_url('style.css') }}" rel="stylesheet">

</head>
<body>

<ul class="palale">
<li><a href="{{ home_url() }}">Home</a></li>

@foreach( config('themes.page') as $page )
	<li><a rel="nofollow"href="{{ permalink($page, 'page') }}">{{ ucwords($page) }}</a></li>
@endforeach

<li><a rel="nofollow" href="{{ home_url('rss') }}">Atom Rss</a></li>
<li><a rel="nofollow" href="{{ home_url('sitemap.xml') }}">Sitemap</a></li>
</ul>
<div id="pala">
<div class="palala">
<a class="lo"><h2>{{ sitename() }} </br> {{ config('site.description') }}</h2></a>

</div>
<div class="palalu">
</div>
<div class="cl"></div>
</div><div class="cl"></div>

<div id="usb">
@include('breadcrumb')

@yield('content')
</div>

<div class="down">Copyright &copy; {{ date('Y') }} {{ sitename() }}. All Rights Reserved.</div>

@include('footer')

</body>
</html>
