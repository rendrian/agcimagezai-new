@extends('layout')

@section('content')
    <div id="usb2">
    <h1 class="usb3">{{ $page_title }}</h1>
    <br>
    <div class="brr">
    @include('page/' . strtolower($page_title) )
    </div>
    </div>
    <div class="cl"></div>
@endsection
