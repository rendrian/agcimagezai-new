@extends('layout')

@section('content')
<div class="breadchumb">
  <div class="crumb">
    <a href="{{ home_url() }}">Home</a> &raquo;
    <a href="{{ get_permalink() }}">{{ $query }}</a> &raquo; <span class="current1">{{ $query }}</span></div>
</div><!-- breadchumb -->
<div class="clear"></div>
<div class="magc-post">
  <div class="magc-postsingle">
              <h1>{{ $query }}</h1>

{!! ads('responsive') !!}


    <a href="#" rel="bookmark" title="{{ $results[0]['title'] }}">

      <img style="width:100%" src="{{ $results[0]['small'] }}" data-src="{{ $results[0]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" class="attachment-large" alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}" />

    </a>
    <h2><center>{{ $results[0]['title'] }}</center></h2>


{!! ads('responsive') !!}


    <span class="adagambar">
      <a href="#" rel="bookmark" title="{{ $results[1]['title'] }}">

        <img style="width:100%" src="{{ $results[1]['small'] }}" data-src="{{ $results[1]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[1]['small'] }}';" class="attachment-large" alt="{{ $results[1]['title'] }}" title="{{ $results[1]['title'] }}" />

      </a>
    </span>
<h2><center>{{ $results[1]['title'] }}</center></h2>

            </div><!-- postsingle -->
</div><!-- post -->
<div class="clear"></div>
<div class="magc-postgallery">
  <h3 class="magc-gallerytitle">Photo Gallery Of The Granite Dining Room Tables And Chairs</h3>


  @foreach($results as $key => $item)

    <a href="#" title="{{ strtolower($query) }} {{ $item['title'] }}">
      <img class="attachment-full" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
    </a>

  @endforeach


</div><!-- postgallery -->
<div class="clear"></div>
<nav class="navsingle">
  <span class="navprevious">
    <a href="{{ permalink( $random_terms[0] ) }}" rel="prev"><span class="meta-nav"><span class=" icon-double-angle-left"></span></span>  << {{ ucwords($random_terms[0]) }} </a>
  </span>
  <span class="navnext">
    <a href="{{ permalink( $random_terms[1] ) }}" rel="next">{{ ucwords($random_terms[1]) }}  <span class="meta-nav"><span class=" icon-double-angle-right"></span> </span>  >> </a>
  </span>
</nav>
<div class="clear"></div>

<h3 class="magc-titlerel">Related Posts to {{ $query }}</h3>
<div class="magc-postboxrel">
<div class="magc-postinforel">
<h3><a href="http://pjamteen.com/granite-dining-room-tables-and-chairs.html">{{ $query }}</a></h3>
<p>{{ $query }} was posted {{ date('d/m/Y') }} by {{ sitename() }} . More over <em>{{ $query }}</em> has viewed by {{ rand(100,9999) }} visitor. </p>
</div>
<div class="magc-postboximgrel">
@for($i=0; $i<=2; $i++)
    <a href="#" title="{{ strtolower($query) }} {{ $results[$i]['title'] }}">
    <img class="attachment-full" src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';" width="110" height="90" title="{{ strtolower($query) }} {{ $results[$i]['title'] }}" alt="{{ strtolower($query) }} {{ $results[$i]['title'] }}">
    </a>
@endfor
</div>
</div>
<div class="clear"></div>

@endsection
