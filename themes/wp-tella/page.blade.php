@extends('layout')

@section('content')
<div class="magc-postpage">
  <div class="magc-titlepage">
    <h1>{{ $page_title }}</h1>
  </div>

@include('page/' . strtolower($page_title) )
</div>

@endsection
