@extends('layout')

@section('content')

@php
	$cols   = array_group( $results, 5 );
@endphp

<div class="title">
				<h1>{{ config('site.title') }} - {{ sitename() }}</h1>
</div>

@foreach ($cols as $item)

@php
  $lastitem = end($item);
@endphp

<div class="magc-post">
<h2><a href="{{ attachment_url( $query, $lastitem['title'] ) }}">{{ ucwords( $lastitem['title'] ) }}</a></h2>
<div class="magc-postimg">
  <a href="{{ attachment_url( $query, $lastitem['title'] ) }}" rel="bookmark" title="{{ ucwords($query) }} {{ ucwords( $lastitem['title'] ) }}">
    <img src="{{ $lastitem['small'] }}" data-src="{{ $lastitem['url'] }}" onerror="this.onerror=null;this.src='{{ $lastitem['small'] }}';" class="attachment-featured-home" alt="{{ ucwords($query) }} {{ ucwords( $lastitem['title'] ) }}" title="{{ ucwords($query) }} {{ ucwords( $lastitem['title'] ) }}" width="280" height="150" />
  </a>

</div>
<div class="magc-postlittleimg">

  @for( $i=0; $i<=count($item)-2; $i++ )

    <a href="{{ attachment_url( $query, $item[$i]['title'] ) }}" rel="bookmark" title="{{ ucwords($query) }} {{ ucwords( $item[$i]['title'] ) }}">
    <img width="70" height="55"  src="{{ $item[$i]['small'] }}" data-src="{{ $item[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $item[$i]['small'] }}';"  class="attachment-featured-thumb" alt="{{ ucwords($query) }} {{ ucwords( $item[$i]['title'] ) }}" title="{{ ucwords($query) }} {{ ucwords( $item[$i]['title'] ) }}"/>
    </a>

  @endfor

</div>

<div class="magc-postinfo">
	<blockquote>
    @for( $i=0; $i<=count($item)-2; $i++ )

<li>{{ ucwords( $item[$i]['title'] ) }}</li>

    @endfor

 					</blockquote>
<p><strong style="color:#FF1493;"> Category : </strong><a href="{{ permalink($query) }}" rel="category tag">{{ $query }}</a></p>
</div><!--   end post-info   -->
				<div class="clear"></div>
</div>
@endforeach

<div class="clear"></div>

<style>
.magc-boxed{
  width:45%;
  float:left;
  margin:5px;
  padding:5px;
  border: solid 1px #a0a0a0;
}
</style>


@php
  $pagination = range(1,20)
@endphp

    <div id="post-navigator">
       <div class="wp-pagenavi">
     <span class="pages">Page 1 of {{ end($pagination) }}:</span>

       @foreach($pagination as $n => $page)
          @if( input()->get('page') == $n+1 )
              <strong class='current'>{{ $page }}</strong>
          @elseif( $page == end($pagination) )
              <a href="{{ home_url() }}?page={{ $page }}">Last &raquo;</a>
          @else
              <a href="{{ home_url() }}?page={{ $page }}">{{ $page }}</a>
          @endif
       @endforeach

  </div>
     <div class="clear"></div>
    </div>

<div class="clear"></div>

    @foreach( $random_terms as $term )
    <div class="magc-boxed">
    				<a href="{{ permalink($term) }}" rel="bookmark">{{ $term }}</a>
    			</div>
    @endforeach
@endsection
