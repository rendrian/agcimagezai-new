@extends('layout')


@section('content')

<header class="main-header">
<div id="header">					
		<h1 class="title single-title">{{ $page_title }}</h1>
</div>
</header>	
@include('breadcrumb')	

@include('page/' . strtolower($page_title) )

@endsection
