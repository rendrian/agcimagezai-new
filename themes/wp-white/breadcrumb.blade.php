<div class="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
	<span typeof="v:Breadcrumb"><a href="{{ home_url() }}" rel="v:url" property="v:title">Home</a></span> &raquo;
	<span typeof="v:Breadcrumb"><a href="{{ get_permalink() }}" rel="v:url" property="v:title">{{ ( isset($query) ) ? $query : config('site.description') }}</a></span> &raquo; 
	<span class="crent">{{ ( isset($query) ) ? $query : config('site.description') }}</span>
</div>
