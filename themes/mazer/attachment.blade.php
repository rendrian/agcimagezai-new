@extends('layout')




@section('content')

  <h1>{{ $query }} - {{ $subquery }}</h1>
  <ul class="c listsingle">

    <div class="ads">
      {!! ads('responsive') !!}
    </div>


    <img style="width:100%" src="{{ $gallery['current']['url'] }}" data-src="{{ $gallery['current']['url'] }}"
      onerror="this.onerror=null;this.src='{{ $gallery['current']['thumbnail'] }}';" class="attachment-full size-full"
      alt="{{ $gallery['current']['title'] }}" title="{{ $gallery['current']['title'] }}" />
    <h2>
      <center>{{ $subquery }}</center>
    </h2>

    <div class="ads">
      {!! ads('responsive') !!}
    </div>

    @for ($i = 0; $i <= 7; $i++)
      <div class="listimg">
        <a href="{{ attachment_url($query, $results[$i]['title']) }}" class="th"
          title="{{ strtolower($query) }} {{ $results[$i]['title'] }}">
          <img width="294" height="230" title="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}"
            alt="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}"
            src="{{ $results[$i]['thumbnail'] }}" data-src="{{ $results[$i]['url'] }}"
            onerror="this.onerror=null;this.src='{{ $results[$i]['thumbnail'] }}';" />
        </a>
        <p>{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}
          <a target="_blank" href="{{ $results[$i]['url'] }}">.</a>
        </p>
      </div>


    @endfor



    @for ($i = 8; $i <= count($results) - 1; $i++)

      <div class="listimg">
        <a href="{{ attachment_url($query, $results[$i]['title']) }}" class="th"
          title="{{ strtolower($query) }} {{ $results[$i]['title'] }}">
          <img width="294" height="230" title="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}"
            alt="{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}"
            src="{{ $results[$i]['thumbnail'] }}" data-src="{{ $results[$i]['url'] }}"
            onerror="this.onerror=null;this.src='{{ $results[$i]['thumbnail'] }}';" />
        </a>
        <p>{{ ucwords($query) }} {{ ucwords($results[$i]['title']) }}
          <a target="_blank" href="{{ $results[$i]['url'] }}">.</a>
        </p>
      </div>
    @endfor

  </ul>

  <ul class="c listhome">
    @foreach ($random_terms as $term)
      <li><a href="{{ permalink($term) }}">{{ $term }}</a></li>
    @endforeach
  </ul>

@endsection
