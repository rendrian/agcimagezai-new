@extends('layout')
@php
// shuffle($results);
@endphp

@section('ads')
  {!! ads('responsive') !!}
@endsection


@section('breadcumb')
  @includeIf('breadcrumb')
@endsection

@section('h1')
  {{ $results[0]['title'] }}
@endsection
@section('content')


  <section class="section-0 px-4 w-full mb-8">
    <a href="{{ attachment_url($query, $results[0]['title']) }}" class="d-block">
      <div class="h-full md:flex">
        <div class="md:w-2/5 bg-cover overflow-hidden">
          <figure class="relative">
            <div class="relative block w-full mx-auto my-0">
              <div class="block" style="padding-bottom:53%"></div>
              <div class="bg-gray-100 absolute inset-0 w-full h-full m-auto overflow-hidden">
                <img class="absolute inset-0 w-full h-full m-auto ezlazyloaded" src="{{ $item['url'] }}"
                  onerror="this.onerror=null;this.src='{{ $results[0]['thumbnail'] }}';"
                  alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}">
              </div>
            </div>
          </figure>
        </div>

        <div class="md:w-3/5 pt-4 md:pt-0 md:pl-4 flex flex-col justify-between leading-normal">
          <div class="md:mb-4">
            <h2 class="mb-2 font-medium text-xl lg:text-3xl">{{ $results[0]['title'] }}</h2>
            <p class="text-base">
              {{-- @include('spintax/search-2') --}}
            </p>
          </div>

        </div>
      </div>
    </a>
  </section>



  @foreach ($results as $i => $item)
    @if ($i !== 0)
      <section class="section-1 w-full md:w-1/3 px-4 mb-8">
        <a href="{{ attachment_url($query, $item['title']) }}" class="flex overflow-hidden"
          title="{{ $item['title'] }}" rel="bookmark" itemprop="associatedMedia" itemscope=""
          itemtype="http://schema.org/ImageObject">
          <div class="flex flex-col h-full w-full">
            <div class="overflow-hidden">
              <figure class="relative">
                <div class="relative block w-full mx-auto my-0">
                  <div class="block" style="padding-bottom:53%"></div>
                  <div class="bg-gray-100 absolute inset-0 w-full h-full m-auto overflow-hidden">
                    <img class="absolute inset-0 w-full h-full m-auto ezlazyloaded"
                      src="{{ filterImage($item['url']) }}"
                      onerror="this.onerror=null;this.src='{{ $item['thumbnail'] }}';" alt="{{ $item['title'] }}"
                      title="{{ $item['title'] }}">
                  </div>
                </div>
              </figure>
            </div>
            <div class="w-full pt-4 flex flex-col justify-between leading-normal">
              <h2 class="mb-2 font-medium text-md sm:text-xl">{{ $item['title'] }}</h2>
              {{-- <p class="text-base">
               
              </p> --}}
            </div>
          </div>
        </a>
      </section>
    @endif

    {{-- @if ($i===6)
    <div class="mx-auto">
      @include('spintax/search-1')
    </div>
    @endif --}}
    {{-- <a href="{{ attachment_url($query, $results[$i]['title']) }}" title="{{ $results[$i]['title'] }}" rel="bookmark"
      itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject">
      <img src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}"
        onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"
        class="attachment-featured-sidebar size-featured-sidebar" alt="{{ $results[$i]['title'] }}"
        title="{{ $results[$i]['title'] }}" />
    </a> --}}

  @endforeach








  {{-- <div id="adr_contentfront">
   <div id="leftxhouse">
      <article class="article" itemscope="" itemtype="http://schema.org/BlogPosting">
         <h1 itemprop="headline" class="headxhouse">{{ $query }}</h1>
         <div class="xhouseimg" itemprop="articleBody">
            <div class="sdkonten" style="background: #3E3B3A;">
			@yield('ads')
               <figure>
                  <a href="#" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject" rel="bookmark">
					  <img width="1187" height="780" src="{{ $results[0]['small'] }}" data-src="{{ $results[0]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[0]['small'] }}';" class="attachment-full size-full" alt="{{ $results[0]['title'] }}" title="{{ $results[0]['title'] }}">
				  </a>
                  <figcaption>
                     <h2>{{ $results[0]['title'] }}</h2>
                     <figcaption> </figcaption>
                  </figcaption>
               </figure>
            </div>
         </div>
         <div class="cl"></div>
         <div class="xhousecontent">
            <div itemprop="articleBody">
                @include('spintax/search-2')
            </div>
			@yield('ads')
            <div class="prevnext">
               <div class="prev">&laquo; <a href="{{ permalink( $random_terms[0] ) }}" rel="prev">{{ ucwords($random_terms[0]) }}</a></div>
               <div class="next">&raquo; <a href="{{ permalink( end($random_terms) ) }}" rel="next">{{ ucwords( end($random_terms) ) }}</a></div>
               <div class="cl"></div>
            </div>
            <div class="cl"></div>
            <div class="xhousecl">
               <h3 class="xhouse_up" itemprop="headline">Gallery of {{ $query }}</h3>
					@foreach ($results as $i => $item)
					<a href="{{ attachment_url( $query, $results[$i]['title'] ) }}" title="{{ $results[$i]['title'] }}" rel="bookmark" itemprop="associatedMedia" itemscope="" itemtype="http://schema.org/ImageObject" >
					  <img src="{{ $results[$i]['small'] }}" data-src="{{ $results[$i]['url'] }}" onerror="this.onerror=null;this.src='{{ $results[$i]['small'] }}';"  class="attachment-featured-sidebar size-featured-sidebar" alt="{{ $results[$i]['title'] }}" title="{{ $results[$i]['title'] }}"/>
					</a>
					@endforeach
            </div>
            <div class="cl"></div>
         </div>
      </article>
   </div>
   <aside id="xhouseside" role="complementary" itemscope="" itemtype="http://schema.org/WPSidebar">
      <div class="sidebarbox">
         <div class="ads_sidebar">
            <center>@yield('ads')</center>
         </div>
         <h3 class="xhouse_up">Popular Post</h3>
         <div class="postlist">

			 @foreach ($random_terms as $term)
				<div class="popularlink">
					<h4><a title="{{ ucwords($term) }}" href="{{ permalink($term) }}" rel="bookmark">{{ ucwords($term) }}</a></h4>
				</div>
			 @endforeach

         </div>
      </div>
   </aside>
   <div class="cl"></div>
</div> --}}

@endsection
