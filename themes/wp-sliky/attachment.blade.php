@extends('layout')

@section('content')


<h1>{{ $subquery }} - {{ $query }}</h1>

<div class="imagegallery">

  <div class="singlet">
<span class="adagambar">
<a href="{{ attachment_url( $query, $gallery['current']['title'] ) }}" title="{{ ucwords($query) }} {{ ucwords($gallery['current']['title']) }}">
  <img style="width:100%" class="attachment-large size-large" data-src="{{ $gallery['current']['url'] }}" src="{{ $gallery['current']['small'] }}" alt="{{ ucwords($gallery['current']['title']) }}" title="{{ ucwords($gallery['current']['title']) }}"></a>
</span>
      <center>{{ ucwords($gallery['current']['title']) }}</center>
{!! ads('responsive') !!}
</div>

<h3>Gallery of {{ $subquery }}</h3>

@foreach($results as $key => $item)
      <div class="ithumb">
        <a href="{{ attachment_url( $query, $item['title'] ) }}" title="{{ strtolower($query) }} {{ $item['title'] }}">
          <img class="class_th" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ strtolower($query) }} {{ $item['title'] }}" alt="{{ strtolower($query) }} {{ $item['title'] }}">
        </a>
        <p>{{ strtolower($query) }} - {{ $item['title'] }} <a target="_blank" href="{{ $item['url'] }}">.</a></p>
      </div>
@endforeach

</div>

@endsection
