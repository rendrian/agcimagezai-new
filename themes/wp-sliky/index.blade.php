@extends('layout')

@section('title')
{{ sitename() }} - {{ config('site.description') }}
@endsection

@section('content')

<div class="post list">
	<h1>Welcome : @yield('title')</h1>

	@foreach ($random_terms as $term)
	<li><a href="{{ permalink($term) }}" title="{{ $term }}">{{ $term }}</a></li>
	@endforeach
	
</div>

@endsection
