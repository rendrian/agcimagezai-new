<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

@include('header')

<style>
html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,br,hr,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,font,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend {margin:0; padding:0; border:0; outline:0; font-size:100%; vertical-align: baseline; background:transparent; }

/* Body */
body {color:#404040;font-size:12px; font-family:arial; line-height:18px;}

/* Main wrap */
#wrap {color:#404040; margin:0 auto; width:1064px; padding:18px 0 0;}

/* Headers */
#header{margin:0 0 36px;}
.logo {margin-left:36px;}
.logo,.logo a{width:206px;height:90px;float:right;}
h3 {border-bottom:1px solid #dadada; font-size:14px; font-weight:bold; margin:10px 0 8px; padding:1px 0 3px;}

/* Sidebar */
#sidebar {float:left; margin-left:30px; padding:0; width:300px;}
#sidebar ul{margin:0 0 15px;}
#sidebar li {margin-left:12px;}
#sidebar li a {font-size:12px; padding:2px; color:#404040; font-weight:normal;}
#sidebar  .label{border-bottom:1px solid #dadada; padding:0 0 6px; margin:0 0 10px; font-size:14px; font-weight:bold;}

/* Content */
#content {float:left; line-height:1.5em; text-align:left; width:728px;}
#content ul,#content ol {margin:0 0 18px 30px;}
#content ul li,#content ol li {margin:0 0 3px;}
#content ul ul,#content ol ol {margin:5px 0 5px 15px;}
.timestamp,.timestamp a {font-size:11px; color:#888; font-weight:normal;}
.postmeta {font-size:11px;border-bottom:2px solid #f0f0f0;line-height:18px;padding:0 0 6px;margin-bottom:0;}
.postmeta a{font-weight:normal;color:#404040;}
#content #postnav {padding:5px; margin:5px 0 18px;}
#content #postnav p {margin:0; padding-left:5px;}
#content #postnav p.right {text-align:right; padding-right:5px; margin-top:-18px;}
#content .introtext p {margin:-5px 0 15px 10px;}
#content h1{border-bottom:4px solid #dadada; font-weight:bold; font-size:1.4em; margin:0 0 8px; padding-bottom:5px;}
.imagegallery{width:734px;margin-bottom:12px;float:left;}
.imed{line-height:0;}
.imedium,.imedium img,.imed img,.attachimage img{width:728px;}
.imedium{height:306px;overflow:hidden;margin-bottom:6px;}
.imedium img,.imed img,.attachimage img{height:auto;max-height:auto;}
.ithumb{width:177.5px;height:200px;margin:0 6px 18px 0;float:left;overflow:hidden;}
.ithumb h2 {
  font: 400 13px/1.3 "Roboto Condensed",sans-serif;text-decoration: none;
}
.ads {border-top:1px solid #dadada;margin-top: 18px;padding-top:18px;}
/* Footer */
#footer {border-top:4px solid #dadada; clear:both; color:#808080; font-size:14px; line-height:18px; margin:0 auto; padding:18px 0 0; text-align:center; font-weight:bold;}
#footer p {
  margin-bottom: 0px;
}
/* Comments */
#content div.comment {margin-bottom:20px;}
#content div.comment p {margin:0 0 4px 10px; padding:3px 0 0 0;}
#respond p {margin:0 0 5px 10px;}
#respond input,#respond textarea {padding:5px; width:330px;}
#respond textarea#comment {width:700px;}
#respond input#submit {width:75px;color:#333; background-color:#fff;}
#respond input,#respond textarea,form div.searchbox input {font:94% Verdana,Tahoma,Arial,sans-serif; border:1px solid #ccc;}
.gravatarside {width:48px; height:48px; float:right; margin:0 5px 3px 5px;}

/* Tags */
blockquote {border:1px solid #dadada; font-size:0.9em; margin:20px 10px; padding:8px;}
blockquote p {padding:2px 0; margin:0; font-weight:bold;}
table#wp-calendar {width:180px; margin:0 0 18px 10px;}
p {margin-bottom:18px;}
ul,ol,dl {font-size:0.9em; margin:2px 0 16px 35px;}
ul ul,ol ol {margin:4px 0 4px 35px;}
code{font-size:1.1em; background-color:#f4f4f4; color:#555; display:block; margin:5px 0 15px 0; padding:5px 5px 5px 7px; border:1px solid #ccc;}

/* Links */
a {color:#4088b8; font-weight:bold; text-decoration:none;}
a:hover {text-decoration:underline;}
a img {border:0;}

/* Searchbox */
#searchform{margin-bottom:18px;}
.searchbox input {font-size:0.9em; padding:5px; width:194px; border:1px solid #ccc;}

/* Various classes */
.post {margin:0 0 15px;}
.textright {text-align:right;}
.textcenter {text-align:center;}
.hide {display:none;}

/* WP image align classes */
.aligncenter {display:block; margin-left:auto; margin-right:auto;}
.alignleft {float:left;}
.alignright {float:right;}
.wp-caption {border:1px solid #ddd; text-align:center; background-color:#f3f3f3; padding-top:4px; margin:10px; border-radius:3px;}
.wp-caption img {margin:0; padding:0; border:0 none;}
.wp-caption-dd {font-size: 11px; line-height: 17px; padding:0 4px 5px; margin:0;}
#pages{width:970px;}
table{border-collapse:collapse;}
table{border: 1px solid #aaa}
tr{border-bottom: 1px solid #aaa;}
td,th{padding:5px;width:182px;border-right: 1px solid #aaa;float:left;overflow:hidden;}
td:nth-child(1),td:nth-child(5),th:nth-child(5){border-right:none;}
td:nth-child(2){border-left: 1px solid #aaa;}
th{text-align:center;font-weight:bold;}
tr:nth-child(odd){background:#e1e1e1;}
.clear {clear:both;}
.ads7{width:728px;height:90px;}
.floatleft{float:left;}
.floatright{float:right;}
.galpage{margin-right:30px;width:220px;position:relative;margin-bottom:30px}
.galpage img{min-width:220px}
.galpage p{background:rgba(10,10,10,0.6);color:#fff;width:210px;padding:5px;height:15px;overflow:hidden;margin-bottom:0}
.galpage:nth-of-type(4n){margin-right:0;}
.galpage a{line-height:0;}
.list li {
  float: left;
  height: 16px;
  list-style-type: none;
  margin-bottom: 9px;
  margin-right: 6px;
  overflow: hidden;
  padding: 6px 9px;
  text-transform: capitalize;
  width: 30%;
}

.list li:nth-child(2n+1) {
    background-color: #ececec;
}
</style>

</head>
<body>
<div id="wrap">
	<div id="header">
		<h2 class="logo"><a href="{{ home_url() }}" title="{{ config('site.title') }}">{{ config('site.title') }} - {{ config('site.description') }}</a></h2>
		<div class="ads7 floatleft">
		{!! ads('responsive') !!}	
		</div>
		<div class="clear"></div>
	</div>
	<div id="content">
	
		@yield('content')
	

	</div>

	@include('sidebar')
	
	<div id="footer">
		<p>
		@foreach( config('themes.page') as $page )
			<a href="{{ permalink( $page, 'page' ) }}" title="{{ $page }}">{{ ucwords($page) }}</a> | 
		@endforeach		
            <a target="_blank" href="{{ home_url('sitemap.xml') }}">crawler</a>
		</p>
		<p>
			© {{ date('Y') }} {{ sitename() }} - All rights reserved.
		</p>
	</div>
</div>

@include('footer')

</body>
</html>