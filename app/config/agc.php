<?php

/*
 *	Agc Configuration
 */

return [

  /*
   *  General AGC Configuration
   */

  'email'          => 'akuorangertijenengesampeanmas@gmail.com',
  'folder_keyword' => ['campur'],                                   // Input single folder name or array ex : [ 'homedesign', 'kitchen', 'bedroom' ]
  'default_search' => 'google',                                    // google, bing, yahoo, yandex
  'sitemap_xsl'    => 'jetpack',                                   // yoast, jetpack
  'meta_index'     => ['index', 'search'],                         // allowed indexed page

  /*
   *  Filter Badwords Configuration
   */
  'filter_badwords_uri'   => false,        // filter badwords in query
  'action_badwords_uri'   => 'newquery',   // redirect, newquery
  'filter_badwords_grab'  => true,         // filter badwords in grab title
  'filter_badwords_terms' => false,        // filter badwords in random_terms
  'filter_common_words'   => false,
  'filter_badwords_ads'   => false,

];
