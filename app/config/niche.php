<?php

/*
 *	Absolute Niche Configuration
 */

return [

  // only grab keyword with minimum count 2
  
  'min_keyword' => 2,

  // take caution to use this, this feature is for advanced use only

  'stritch'    => false,

  // the script will only make request to search query that contains the following word below

  'keyword'    => 'nuptse,baseball,flipper,demarini,2011,2012,2013,2014,2015,2016,2017,2018,boost,350,40s,hoodie,catchers,abmattierung,education,grade,chemistry,book,pdf,floor,sofa,bathroom,kitchen,room,worksheet,cover letter,house,style,adidas,adieu,aesop,ahp,aire,air force,airfryer,air jordan,air max,air,all,shoes,american,anessa,heater,hair,atomy,casas,closeout,colchas,colchones,size,colchon,custom,dalstrong,blush,mask,decoracion,shampoo,lotion,demarini,dls,diy,sweater,hampton,kjobenhavn,wallpaper,hielera,home,horno,how to,kitchen,institut,jersey,jordan,jacket,resistor,capacitor,kapasitor,inductor,induktor,diode,dioda,transistor,ic,integrated circuit,switch,saklar',

  // if the search query doesn't have any keyword above, return to this action

  'action'     => 'noindex', // OPSI : 'redirect', 'noindex'

];
