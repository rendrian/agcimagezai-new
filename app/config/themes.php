<?php

/*
 *	Themes Configuration
 */

return [

	'active_theme'  	=> 'linuxize',

	'attachment'		=> true,

	'page'				=> [ "contact", "disclaimer", "privacy", "copyright", "terms" ], // list name pages

];
