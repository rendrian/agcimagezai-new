<?php

/*
 *	Initial Loader
 */

function init_loader()
{
	// redirect bad domain
	if( in_array( $_SERVER['HTTP_HOST'], config('options.bad_domain') ) ){
			redirection( "http://google.com/" );
	}

	// filter badwords $_SERVER['REQUEST_URI']
	if( config('agc.filter_badwords_uri') )
	{
      if( config('agc.action_badwords_uri') === 'redirect' ) {
            is_badwords( $_SERVER['REQUEST_URI'], true );
      }
      if( is_search() ) {
           if( is_badwords( search_query() ) ){
             redirection( permalink( filter_badstrings( search_query() ) ) );
           }
      } elseif( is_attachment() ) {
        if( is_badwords( attachment_query() ) ){
            redirection( attachment_url( search_query(), filter_badstrings( attachment_query() ) ) );
        }
      }
	}

	// redirect http://www.domain.com to http://domain.com
	$checkwww = strpos( get_permalink( true ), "www.{$_SERVER['HTTP_HOST']}" );

	if( $checkwww !== false && config('options.redirect.www_to_nonwww') )
	{
		$Redir = str_replace( "www.{$_SERVER['HTTP_HOST']}", $_SERVER['HTTP_HOST'], get_permalink( true ) );
		redirection( $Redir );
	}
	return true;
}
