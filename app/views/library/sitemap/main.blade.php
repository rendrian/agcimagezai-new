<?php echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<?php echo '<?xml-stylesheet type="text/xsl" href="'. str_replace('http://', 'https://', sitemap_xsl('main')) .'"?>'; ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url>
  <loc>{{ home_url() }}</loc>
</url>
@if( $sitemap )
  @foreach($sitemap as $item)
  <url>
  	<loc>{{ permalink($item) }}</loc>
  	<lastmod>{{ sitemap_date() }}</lastmod>
  </url>
  @endforeach
@endif
</urlset>
