<?xml version="1.0" encoding="UTF-8" ?>
<?php echo '<?xml-stylesheet type="text/xsl" href="'. str_replace('http://', 'https://', sitemap_xsl('index')) .'"?>'; ?>


<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
		<loc>{{ home_url('feed') }}</loc>
		<lastmod>{{ sitemap_date() }}</lastmod>
	</sitemap>
	<sitemap>
		<loc>{{ home_url('rss') }}</loc>
		<lastmod>{{ sitemap_date() }}</lastmod>
	</sitemap>	
	<sitemap>
		<loc>{{ home_url('atom') }}</loc>
		<lastmod>{{ sitemap_date() }}</lastmod>
	</sitemap>	
	<sitemap>
		<loc>{{ home_url('feed/image') }}</loc>
		<lastmod>{{ sitemap_date() }}</lastmod>
	</sitemap>	
	<sitemap>
		<loc>{{ home_url('sitemap-image.xml') }}</loc>
		<lastmod>{{ sitemap_date() }}</lastmod>
	</sitemap>
	@foreach($sitemap as $item)
	<sitemap>
		<loc>{{ home_url('sitemap-' . $item . '.xml') }}</loc>
		<lastmod>{{ sitemap_date() }}</lastmod>
	</sitemap>
	@endforeach
</sitemapindex>